USE [test5]
GO
/****** Object:  Table [dbo].[BANHANG]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BANHANG](
	[ID] [uniqueidentifier] NOT NULL,
	[RefID] [nvarchar](20) NULL,
	[RefDate] [datetime] NULL,
	[RefNo] [nvarchar](20) NULL,
	[RefOrgNo] [nvarchar](20) NULL,
	[RefType] [smallint] NULL,
	[RefStatus] [smallint] NULL,
	[PhuongThucThanhToan] [nvarchar](20) NULL,
	[MaVach] [nvarchar](20) NULL,
	[IdHangTon] [nvarchar](20) NULL,
	[IdTienTe] [nvarchar](3) NULL,
	[TyGiaNgoaiTe] [money] NULL,
	[IdChiNhanh] [nvarchar](20) NULL,
	[IdHopDong] [nvarchar](20) NULL,
	[IdKhachHang] [nvarchar](20) NULL,
	[TenKhachHang] [nvarchar](255) NULL,
	[DiaChiKhachHang] [nvarchar](255) NULL,
	[TaxKhachHang] [nvarchar](20) NULL,
	[TenNguoiLienHe] [nvarchar](100) NULL,
	[ExpectedDate] [datetime] NULL,
	[NgayGiamGia] [datetime] NULL,
	[PhanTramGiamGia] [money] NULL,
	[DiscountTaken] [money] NULL,
	[FOB] [money] NULL,
	[Amount] [money] NULL,
	[ThanhToan] [money] NULL,
	[VatAmount] [money] NULL,
	[GiamGia] [money] NULL,
	[CacGiamGiaKhac] [money] NULL,
	[PhiGiaoHang] [money] NULL,
	[CacLoaiPhiKhac] [money] NULL,
	[FAmount] [money] NULL,
	[FPayment] [money] NULL,
	[FVatAmount] [money] NULL,
	[FDiscount] [money] NULL,
	[FOtherDiscount] [money] NULL,
	[FShipCharge] [money] NULL,
	[FOtherCharge] [money] NULL,
	[IdNguoiBanHang] [nvarchar](20) NULL,
	[IdNhaCungCap] [nvarchar](20) NULL,
	[NgayGiaoHang] [datetime] NULL,
	[IdTaiXe] [nvarchar](20) NULL,
	[SoXe] [nvarchar](20) NULL,
	[CarryingMeans] [nvarchar](255) NULL,
	[IsPublic] [bit] NULL,
	[NguoiLapPhieu] [nvarchar](20) NULL,
	[NgaylapPhieu] [datetime] NULL,
	[NguoiSuadoi] [nvarchar](20) NULL,
	[NgaySuaDoi] [datetime] NULL,
	[MoTa] [nvarchar](255) NULL,
	[SapXep] [bigint] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_SALE_ORDER_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BAOCAO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BAOCAO](
	[IdBaoCao] [varchar](20) NOT NULL,
	[TenBaoCao] [nvarchar](255) NULL,
	[TieuDe] [nvarchar](255) NULL,
	[BinhLuan] [nvarchar](255) NULL,
	[TenFile] [nvarchar](255) NULL,
	[Mota] [nvarchar](255) NULL,
	[DataSet] [nvarchar](50) NULL,
	[Class] [nvarchar](50) NULL,
	[Parent_ID] [varchar](20) NULL,
	[Order] [int] NULL,
	[Avtive] [bit] NULL,
 CONSTRAINT [PK_REPORT] PRIMARY KEY CLUSTERED 
(
	[IdBaoCao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BienLai]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BienLai](
	[IdBienLai] [varchar](20) NOT NULL,
	[Receipt_Date] [datetime] NULL,
	[Receipt_No] [varchar](20) NULL,
	[NguoiGiaoHang] [nvarchar](100) NULL,
	[IdTienTe] [varchar](3) NULL,
	[GiaoDich] [money] NULL,
	[IdKhachHang] [varchar](20) NULL,
	[TenKhachHang] [nvarchar](255) NULL,
	[DiaChiKhachHang] [nvarchar](255) NULL,
	[MoTa] [nvarchar](255) NULL,
	[Receipt_Group_ID] [varchar](20) NULL,
	[Amount] [money] NULL,
 CONSTRAINT [PK_RECEIPT] PRIMARY KEY CLUSTERED 
(
	[IdBienLai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BOPHAN]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BOPHAN](
	[BoPhan_ID] [varchar](20) NOT NULL,
	[BoPhan_Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
 CONSTRAINT [PK_DEPARTMENT] PRIMARY KEY CLUSTERED 
(
	[BoPhan_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHITIET_CHUYENKHO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHITIET_CHUYENKHO](
	[ID] [uniqueidentifier] NOT NULL,
	[ChuyenKho_ID] [varchar](20) NOT NULL,
	[RefType] [int] NULL,
	[SanPham_ID] [varchar](20) NOT NULL,
	[TenSanPham] [nvarchar](250) NULL,
	[MaKhoXuat] [varchar](20) NOT NULL,
	[TenKhoXuat] [nvarchar](250) NULL,
	[MaKhoNhap] [varchar](20) NOT NULL,
	[TenKhoNHap] [nvarchar](250) NULL,
	[DonVi] [nvarchar](20) NULL,
	[DonViChuyen] [money] NULL,
	[DonGia] [money] NULL,
	[SoLuong] [money] NULL,
	[MoTa] [nvarchar](255) NULL,
 CONSTRAINT [PK_STOCK_TRANSFER_DETAIL_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHITIET_NHAPKHO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHITIET_NHAPKHO](
	[ID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[NhapKho_ID] [varchar](20) NOT NULL,
	[SanPham_ID] [varchar](20) NOT NULL,
	[TenSanPham] [nvarchar](255) NULL,
	[RefType] [int] NULL,
	[Kho_ID] [varchar](20) NULL,
	[DonVi] [nvarchar](20) NULL,
	[ChuyenDoiDonVi] [money] NULL,
	[Vat] [int] NULL,
	[SoTien_Vat] [money] NULL,
	[ThanhTien] [money] NULL,
	[SoLuong] [money] NULL,
	[DonGia] [money] NULL,
	[TyLeChieuKhau] [money] NULL,
	[ChietKhau] [money] NULL,
	[Phi] [money] NULL,
	[GioiHan] [datetime] NULL,
	[StoreID] [bigint] NULL,
	[MoTa] [nvarchar](255) NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_STOCK_INWARD_DETAIL] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHITIET_XUATKHO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHITIET_XUATKHO](
	[ID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[XuatKho_ID] [varchar](20) NOT NULL,
	[Kho_ID] [varchar](20) NULL,
	[RefType] [int] NULL,
	[SanPham_ID] [varchar](20) NOT NULL,
	[TenSanPham] [nvarchar](255) NULL,
	[Vat] [int] NULL,
	[SoTien_Vat] [money] NULL,
	[DonVi] [nvarchar](20) NULL,
	[ChuyenDonVi] [money] NULL,
	[SoLuong] [money] NULL,
	[DonGia] [money] NULL,
	[TyLeChietKhau] [money] NULL,
	[ChietKhau] [money] NULL,
	[Phi] [money] NULL,
	[ChiPhi] [money] NULL,
	[StoreID] [bigint] NULL,
	[MoTa] [nvarchar](250) NULL,
	[KichHoat] [bit] NOT NULL,
 CONSTRAINT [PK_STOCK_OUTWARD_DETAIL] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHITIETBANHANG]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CHITIETBANHANG](
	[ID] [uniqueidentifier] NOT NULL,
	[RefID] [nvarchar](20) NOT NULL,
	[IdChungKhoan] [nvarchar](20) NULL,
	[IdKieuHangHoa] [smallint] NULL,
	[IdSanPham] [nvarchar](20) NOT NULL,
	[TenSanPham] [nvarchar](255) NULL,
	[DonVi] [nvarchar](20) NULL,
	[UnitConvert] [money] NULL,
	[IdThue] [nvarchar](20) NULL,
	[Thue] [money] NULL,
	[TaxAmount] [money] NULL,
	[Vat] [nvarchar](20) NULL,
	[Quantity] [money] NULL,
	[PhiGiaoHang] [money] NULL,
	[FUnitPrice] [money] NULL,
	[DonGia] [money] NULL,
	[LaiXuatChietKhau] [money] NULL,
	[Discount] [money] NULL,
	[VatAmount] [money] NULL,
	[Phi] [money] NULL,
	[Amount] [money] NULL,
	[Batch] [nvarchar](20) NULL,
	[SerialBegin] [nvarchar](20) NULL,
	[SerialEnd] [nvarchar](20) NULL,
	[ChassyNo] [nvarchar](20) NULL,
	[SapXep] [int] NULL,
	[IdCuaHang] [uniqueidentifier] NULL,
	[MoTa] [nvarchar](255) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_SALE_ORDER_DETAIL_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CHITIETKHACHHANGNO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHITIETKHACHHANGNO](
	[ID] [uniqueidentifier] NOT NULL,
	[ID_Phieu] [nvarchar](20) NULL,
	[NgayTao] [datetime] NULL,
	[HinhThucThanhToan] [uniqueidentifier] NULL,
	[TenHangHoa] [nvarchar](255) NULL,
	[TiGiaQuyDoi] [money] NULL,
	[HanThanhToan] [datetime] NULL,
	[SoLuong] [money] NULL,
	[ThanhTien] [money] NULL,
	[ChietKhau] [money] NULL,
	[NguoiTao] [nvarchar](20) NULL,
	[NguoiChinhSua] [nvarchar](20) NULL,
	[NgayChinhSua] [datetime] NULL,
	[MoTa] [nvarchar](255) NULL,
	[ViTri] [bigint] NULL,
	[KichHoat] [bit] NULL,
	[ID_KhachHang] [varchar](50) NULL,
	[ID_HangHoa] [varchar](50) NULL,
	[ID_TienTe] [varchar](50) NULL,
 CONSTRAINT [PK_CHITIETKHACHHANGNO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHITIETPHIEUHANGTON]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHITIETPHIEUHANGTON](
	[ID] [uniqueidentifier] NOT NULL,
	[Transfer_ID] [varchar](20) NOT NULL,
	[RefType] [int] NULL,
	[IdSanPham] [varchar](20) NOT NULL,
	[TenSanPham] [nvarchar](250) NULL,
	[OutStock] [varchar](20) NULL,
	[TonKho] [varchar](20) NULL,
	[DonVi] [nvarchar](20) NULL,
	[ChuyenDonVi] [money] NULL,
	[GiaDonVi] [money] NULL,
	[Quantity] [money] NULL,
	[Amount] [money] NULL,
	[QtyConvert] [money] NULL,
	[GioiHan] [datetime] NULL,
	[IdCuaHang] [bigint] NULL,
	[MoTa] [nvarchar](255) NULL,
	[SapXep] [bigint] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHITIETPHIEUTHUNOKHACHHANG]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CHITIETPHIEUTHUNOKHACHHANG](
	[ID] [uniqueidentifier] NOT NULL,
	[ID_Phieu] [uniqueidentifier] NOT NULL,
	[ID_TienTe] [nvarchar](3) NULL,
	[TiGiaQuyDoi] [money] NULL,
	[SoLuong] [money] NULL,
	[ThanhTien] [money] NULL,
	[TongNo] [money] NULL,
	[TienTra] [money] NULL,
	[PhanTramChietKhau] [money] NULL,
	[ChietKhau] [money] NULL,
	[MoTa] [nvarchar](255) NULL,
	[ViTri] [bigint] NULL,
 CONSTRAINT [PK_CHITIETPHIEUTHUNOKHACHHANG] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CHITIETTHANHTOANNHACUNGCAP]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CHITIETTHANHTOANNHACUNGCAP](
	[ID] [uniqueidentifier] NOT NULL,
	[IdPhieuThanhToan] [uniqueidentifier] NOT NULL,
	[RefOrgNo] [uniqueidentifier] NULL,
	[IdTienTe] [nvarchar](3) NULL,
	[TiGiaNgoaiTe] [money] NULL,
	[Quantity] [money] NULL,
	[Amount] [money] NULL,
	[SoNo] [money] NULL,
	[DaTra] [money] NULL,
	[PhanTramCHietKhau] [money] NULL,
	[ChietKhau] [money] NULL,
	[FAmount] [money] NULL,
	[FDebit] [money] NULL,
	[FPayment] [money] NULL,
	[FDiscount] [money] NULL,
	[MoTa] [nvarchar](255) NULL,
	[SapXep] [bigint] NULL,
 CONSTRAINT [PK_PROVIDER_PAYMENT_DETAIL_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CHUYENDOIDONVI]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHUYENDOIDONVI](
	[ID] [nvarchar](255) NOT NULL,
	[SanPhamID] [varchar](20) NOT NULL,
	[DonViChinh] [nvarchar](20) NOT NULL,
	[DonViChuyenDoi] [nvarchar](20) NOT NULL,
	[TyLeChuyenDoi] [money] NULL,
 CONSTRAINT [PK_UNITCONVERT_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHUYENKHO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHUYENKHO](
	[ID] [varchar](20) NOT NULL,
	[RefDate] [datetime] NULL,
	[Ref_OrgNo] [nvarchar](20) NULL,
	[RefType] [int] NULL,
	[NguoiGui_ID] [varchar](20) NULL,
	[KhoTu_ID] [varchar](20) NULL,
	[KhoDen_ID] [varchar](20) NULL,
	[NguoiNhan_ID] [varchar](20) NULL,
	[TyGiaNgoaiTe] [money] NULL,
	[SoLuong] [money] NULL,
	[MoTa] [nvarchar](255) NULL,
	[KichHoat] [bit] NOT NULL,
 CONSTRAINT [PK_STOCK_TRANSFER] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CONGNO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONGNO](
	[ID] [uniqueidentifier] NOT NULL,
	[RefID] [nvarchar](20) NULL,
	[RefDate] [datetime] NULL,
	[RefType] [smallint] NULL,
	[RefStatus] [smallint] NULL,
	[HinhThucThanhToan] [uniqueidentifier] NULL,
	[KhachHangID] [nvarchar](20) NULL,
	[SanPhamID] [nvarchar](20) NULL,
	[SanPhamName] [nvarchar](255) NULL,
	[TienTeID] [nvarchar](3) NULL,
	[TyGia] [money] NULL,
	[TermID] [nvarchar](20) NULL,
	[NgayDaoHan] [datetime] NULL,
	[SoLuong] [money] NULL,
	[ReSoLuong] [money] NULL,
	[GiaBan] [money] NULL,
	[Tong] [money] NULL,
	[ThanhToan] [money] NULL,
	[CanDoi] [money] NULL,
	[FTong] [money] NULL,
	[IsChanged] [bit] NULL,
	[Description] [nvarchar](255) NULL,
 CONSTRAINT [PK_CONGNO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DIEUKHOANTHANHTOAN]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DIEUKHOANTHANHTOAN](
	[ID] [nvarchar](20) NOT NULL,
	[ID_DK] [nvarchar](20) NULL,
	[DienGiai] [nvarchar](255) NULL,
	[NguoiTao] [nvarchar](20) NULL,
	[NgayTao] [datetime] NULL,
	[NguoiChinhSua] [nvarchar](20) NULL,
	[NgayChinhSua] [datetime] NULL,
	[ViTri] [bigint] NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_DIEUKHOANTHANHTOAN] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[donvi]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[donvi](
	[DonVi_ID] [nvarchar](20) NOT NULL,
	[TenDonVi] [nvarchar](250) NULL,
	[MoTa] [nvarchar](250) NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_UNIT] PRIMARY KEY CLUSTERED 
(
	[DonVi_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DONVI_HANGHOA]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DONVI_HANGHOA](
	[IdSanPham] [nvarchar](20) NOT NULL,
	[IdDonVi] [nvarchar](20) NOT NULL,
	[UnitConvert_ID] [nvarchar](20) NOT NULL,
	[UnitConvert] [money] NOT NULL,
	[SapXep] [bigint] NOT NULL,
 CONSTRAINT [PK_PRODUCT_UNIT] PRIMARY KEY CLUSTERED 
(
	[IdSanPham] ASC,
	[IdDonVi] ASC,
	[UnitConvert_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FORM]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FORM](
	[Form_Id] [nvarchar](50) NOT NULL,
	[Form_Caption] [nvarchar](255) NULL,
	[ENCaption] [nvarchar](250) NULL,
	[Width] [int] NULL,
	[Height] [int] NULL,
	[Description] [nvarchar](255) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_FORM] PRIMARY KEY CLUSTERED 
(
	[Form_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GIASANPHAM]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GIASANPHAM](
	[ID] [uniqueidentifier] NOT NULL,
	[RefDate] [datetime] NULL,
	[RefStatus] [smallint] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[IdSanPham] [uniqueidentifier] NULL,
	[IdKhachHang] [uniqueidentifier] NULL,
	[Gia] [money] NULL,
	[ChietKhau] [money] NULL,
	[HoaHong] [money] NULL,
	[SoLuong] [money] NULL,
	[SoLuongBatDau] [money] NULL,
	[SoLuongKetThuc] [money] NULL,
	[CongKhai] [bit] NULL,
	[NguoiLapPhieu] [nvarchar](20) NULL,
	[NgayLapPhieu] [datetime] NULL,
	[NguoiThayDoi] [nvarchar](20) NULL,
	[NgayThayDoi] [datetime] NULL,
	[IdNhaCungCap] [nvarchar](20) NULL,
	[MoTa] [nvarchar](255) NULL,
	[SapXep] [bigint] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_PRODUCT_PRICE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HANGTON]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HANGTON](
	[IdHangTon] [varchar](20) NOT NULL,
	[TenHangTon] [nvarchar](255) NULL,
	[LienLac] [nvarchar](255) NULL,
	[DiaChi] [nvarchar](255) NULL,
	[Email] [nvarchar](50) NULL,
	[SDT] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[Mobi] [varchar](20) NULL,
	[QuanLy] [nvarchar](255) NULL,
	[MoTa] [nvarchar](255) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_tbStock] PRIMARY KEY CLUSTERED 
(
	[IdHangTon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HANGTONKHO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HANGTONKHO](
	[ID] [bigint] NOT NULL,
	[RefID] [varchar](20) NULL,
	[RefDate] [datetime] NULL,
	[RefType] [int] NULL,
	[Kho_ID] [varchar](20) NULL,
	[SanPham_ID] [varchar](20) NULL,
	[KhachHang_ID] [varchar](20) NULL,
	[TienTe_ID] [varchar](3) NULL,
	[GioiHan] [datetime] NULL,
	[Quantity] [money] NULL,
	[Amount] [money] NULL,
	[Lô] [nvarchar](50) NULL,
	[Serial] [nvarchar](50) NULL,
	[Width] [money] NULL,
	[Height] [money] NULL,
	[Size] [nvarchar](255) NULL,
	[Descritopn] [nvarchar](255) NULL,
 CONSTRAINT [PK_INVENTORY_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HANGTONKHO_ACTION]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HANGTONKHO_ACTION](
	[Action_ID] [int] NOT NULL,
	[Action_Name] [nvarchar](255) NULL,
 CONSTRAINT [PK_INVENTORY_ACTION] PRIMARY KEY CLUSTERED 
(
	[Action_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HANGTONKHO_BOOK]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HANGTONKHO_BOOK](
	[ID] [uniqueidentifier] NOT NULL,
	[Book_ID] [varchar](20) NULL,
	[NgayTao] [datetime] NULL,
	[NgayVaoKho] [datetime] NULL,
	[GioiHan] [datetime] NULL,
	[TonKHo_ID] [varchar](20) NULL,
	[SanPham_ID] [varchar](20) NULL,
	[SanPhamName] [nvarchar](255) NULL,
	[KhachHang_ID] [varchar](20) NULL,
	[TienTe_ID] [varchar](3) NULL,
	[Quantity] [money] NULL,
	[TyGia] [money] NULL,
	[DonGia] [money] NULL,
	[Amount] [money] NULL,
	[Lô] [nvarchar](50) NULL,
	[Serial] [nvarchar](50) NULL,
	[Frame] [nvarchar](50) NULL,
	[Width] [money] NULL,
	[Height] [money] NULL,
	[Size] [nvarchar](255) NULL,
	[Lock] [int] NULL,
 CONSTRAINT [PK_HANGTONKHO_BOOK] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HANGTONKHO_DATE]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HANGTONKHO_DATE](
	[ID] [bigint] NOT NULL,
	[RefDate] [datetime] NULL,
	[TonKho_ID] [varchar](20) NULL,
	[SanPham_ID] [varchar](20) NULL,
	[E_Qty] [money] NULL,
	[E_Unt] [money] NULL,
	[E_Amt] [money] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HANGTONKHO_DETAIL]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HANGTONKHO_DETAIL](
	[ID] [bigint] NOT NULL,
	[RefNo] [varchar](20) NULL,
	[RefDate] [datetime] NULL,
	[RefDetailNo] [uniqueidentifier] NULL,
	[RefType] [int] NULL,
	[RefStatus] [int] NULL,
	[CuaHangID] [bigint] NULL,
	[TonKHo_ID] [varchar](20) NULL,
	[SanPham_ID] [varchar](20) NULL,
	[SanPham_Name] [nvarchar](255) NULL,
	[KhachHang_ID] [varchar](20) NULL,
	[NhanVien_ID] [varchar](20) NULL,
	[Lô] [nvarchar](50) NULL,
	[Serial] [varchar](50) NULL,
	[DonVi] [nvarchar](50) NULL,
	[Price] [money] NULL,
	[Quantity] [money] NULL,
	[UnitPrice] [money] NULL,
	[Amount] [money] NULL,
	[E_Qty] [money] NULL,
	[E_Amt] [money] NULL,
	[Description] [nvarchar](255) NULL,
	[Book_ID] [varchar](20) NULL,
 CONSTRAINT [PK_HANGTONKHO_DETAIL] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HANGTONKHO_SOSACH]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HANGTONKHO_SOSACH](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RefDate] [datetime] NULL,
	[SanPham_ID] [varchar](20) NOT NULL,
	[SanPham_Name] [nvarchar](255) NULL,
	[DonVi_Name] [nvarchar](250) NULL,
	[TonKho_ID] [varchar](20) NULL,
	[TonKho_Name] [nvarchar](255) NULL,
	[NhomSanPham_ID] [varchar](20) NULL,
	[NhomSanPham_Name] [nvarchar](255) NULL,
	[SoLuongMo] [money] NULL,
	[TongLuongMo] [money] NULL,
	[SoLuongNhap] [money] NULL,
	[TongSLNhap] [money] NULL,
	[SoLuongXuat] [money] NULL,
	[TongSLXuat] [money] NULL,
	[SoLuongSanCo] [money] NULL,
	[TongKetTong] [money] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HINHTHUCTHANHTOAN]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HINHTHUCTHANHTOAN](
	[ID] [uniqueidentifier] NOT NULL,
	[ID_HTThanhToan] [nvarchar](20) NULL,
	[DienGiai] [nvarchar](255) NULL,
	[NguoiTao] [nvarchar](20) NULL,
	[NgayTao] [datetime] NULL,
	[NguoiChinhSua] [nvarchar](20) NULL,
	[NgayChinhSua] [datetime] NULL,
	[ViTri] [int] NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_HINHTHUCTHANHTOAN] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KEYCODE]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KEYCODE](
	[Id_Key] [int] NOT NULL,
	[KeyCode] [varchar](500) NOT NULL,
	[Id_Contact] [int] NULL,
	[DateCreate] [datetime] NOT NULL,
	[Property] [nvarchar](500) NULL,
	[NumActive] [int] NOT NULL,
	[Activated] [int] NOT NULL,
	[Blacklist] [bit] NULL,
	[TypeActive] [int] NULL,
	[TypeSoft] [int] NULL,
	[DateLimit] [datetime] NULL,
	[Register] [bit] NULL,
 CONSTRAINT [PK_KEYCODE] PRIMARY KEY CLUSTERED 
(
	[Id_Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KHACHHANG]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KHACHHANG](
	[Id_KhachHang] [varchar](20) NOT NULL,
	[ThuTu] [bigint] NULL,
	[TenKhachHang] [nvarchar](255) NOT NULL,
	[Id_LoaiKhachHang] [varchar](20) NOT NULL,
	[Id_NhomKhachHang] [varchar](20) NOT NULL,
	[DiaChiKhachHang] [nvarchar](255) NULL,
	[NgaySinh] [varchar](10) NULL,
	[MST] [varchar](20) NULL,
	[DienThoai] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[Email] [varchar](100) NULL,
	[SoDiDong] [varchar](20) NULL,
	[Website] [varchar](100) NULL,
	[LienHe] [nvarchar](100) NULL,
	[ChucVu] [nvarchar](100) NULL,
	[NickYM] [nvarchar](20) NULL,
	[NickSky] [nvarchar](20) NULL,
	[SoTKNganHang] [varchar](20) NULL,
	[TenNganHang] [nvarchar](100) NULL,
	[HanMucNo] [money] NULL,
	[ChietKhau] [money] NULL,
	[CoNo] [bit] NULL,
	[LaNhaCungCap] [bit] NULL,
	[MoTa] [nvarchar](255) NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_KHACHHANG] PRIMARY KEY CLUSTERED 
(
	[Id_KhachHang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LAPPHIEUHANGTON]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LAPPHIEUHANGTON](
	[ID] [varchar](20) NOT NULL,
	[RefDate] [datetime] NULL,
	[Ref_OrgNo] [nvarchar](20) NULL,
	[RefType] [int] NULL,
	[MaVach] [varchar](20) NULL,
	[IdTinh] [varchar](20) NULL,
	[IdNhanVien] [varchar](20) NULL,
	[FromStock_ID] [varchar](20) NULL,
	[ToStock_ID] [varchar](20) NULL,
	[IdChiNhanh] [varchar](20) NULL,
	[IdHopDong] [varchar](20) NULL,
	[IdSanPham] [varchar](20) NULL,
	[IdTienTe] [varchar](3) NULL,
	[TenSanPham] [nvarchar](255) NULL,
	[TyGiaTienTe] [money] NULL,
	[Quantity] [money] NULL,
	[Amount] [money] NULL,
	[IsClose] [bit] NULL,
	[MoTa] [nvarchar](255) NULL,
	[IdNguoiDung] [varchar](20) NULL,
	[SapXep] [bigint] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_STOCK_BUILD] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LICENSE]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LICENSE](
	[Id_License] [int] NOT NULL,
	[Id_Soft] [int] NOT NULL,
	[KeyCode] [varchar](500) NOT NULL,
	[MachineCode] [varchar](500) NOT NULL,
	[Active] [bit] NULL,
	[Version] [varchar](50) NULL,
	[DateActive] [datetime] NULL,
	[ReActive] [int] NULL,
 CONSTRAINT [PK_LICENSE_1] PRIMARY KEY CLUSTERED 
(
	[Id_License] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOAIKHACHHANG]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOAIKHACHHANG](
	[ID_Loai] [varchar](20) NOT NULL,
	[Ten] [nvarchar](255) NULL,
	[MoTa] [nvarchar](255) NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_LOAIKHACHHANG] PRIMARY KEY CLUSTERED 
(
	[ID_Loai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOAISANPHAM]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LOAISANPHAM](
	[IdLoaiSanPham] [int] NOT NULL,
	[TenSanPham] [nvarchar](255) NULL,
 CONSTRAINT [PK_PRODUCT_TYPE] PRIMARY KEY CLUSTERED 
(
	[IdLoaiSanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MESSAGE]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MESSAGE](
	[Mess_Id] [nvarchar](50) NOT NULL,
	[Caption] [nvarchar](200) NULL,
	[ENCaption] [nvarchar](200) NULL,
	[Description] [nvarchar](250) NULL,
	[Active] [bit] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MODULE]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MODULE](
	[ID] [int] NOT NULL,
	[Ref_ID] [nvarchar](20) NULL,
	[Ref_Name] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL,
	[Version] [nvarchar](20) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_MODULE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NHACUNGCAP]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHACUNGCAP](
	[IdKhachHang] [varchar](20) NOT NULL,
	[IdDatHang] [bigint] NULL,
	[TenKhachHang] [nvarchar](255) NOT NULL,
	[IdLoaiKhachHang] [varchar](20) NOT NULL,
	[IdNhomKhachHang] [varchar](20) NOT NULL,
	[DiaChiKhachHang] [nvarchar](255) NULL,
	[NgaySinh] [varchar](10) NULL,
	[MaVach] [varchar](20) NULL,
	[Tax] [varchar](20) NULL,
	[Tel] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[Email] [varchar](100) NULL,
	[Mobile] [varchar](20) NULL,
	[Website] [varchar](100) NULL,
	[NguoiLienHe] [nvarchar](100) NULL,
	[DiaChi] [nvarchar](100) NULL,
	[NickYM] [nvarchar](20) NULL,
	[NickSky] [nvarchar](20) NULL,
	[KhuVuc] [nvarchar](100) NULL,
	[Quan] [nvarchar](100) NULL,
	[QuocGia] [nvarchar](100) NULL,
	[ThanhPho] [nvarchar](100) NULL,
	[TaiKhoanNganHang] [varchar](20) NULL,
	[TenNganHang] [nvarchar](100) NULL,
	[GioiHanNo] [money] NULL,
	[ChietKhau] [money] NULL,
	[IsCongNo] [bit] NULL,
	[ChiTietCongNo] [bit] NULL,
	[LaNhaCungCap] [bit] NULL,
	[MoTa] [nvarchar](255) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_SUPPLIER_1] PRIMARY KEY CLUSTERED 
(
	[IdKhachHang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHANVIEN]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHANVIEN](
	[NhanVien_ID] [varchar](20) NOT NULL,
	[FirtName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[NhanVien_Name] [nvarchar](100) NULL,
	[BietDanh] [nvarchar](100) NULL,
	[GioiTinh] [bit] NULL,
	[DiaChi] [nvarchar](255) NULL,
	[QueQuan_ID] [varchar](20) NULL,
	[H_Tel] [varchar](20) NULL,
	[O_Tel] [varchar](20) NULL,
	[Mobile] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[Email] [nvarchar](255) NULL,
	[Birthday] [datetime] NULL,
	[HonNhan] [int] NULL,
	[ChucVu_ID] [varchar](20) NULL,
	[ChucDanh_ID] [varchar](20) NULL,
	[Branch_ID] [varchar](20) NULL,
	[BoPhan_ID] [varchar](20) NULL,
	[Nhom_ID] [varchar](20) NULL,
	[ThueCaNhan_ID] [varchar](50) NULL,
	[ThanhPho_ID] [varchar](20) NULL,
	[DiaChi_ID] [varchar](20) NULL,
	[QuanLy_ID] [varchar](20) NULL,
	[LoaiNhanVien] [int] NULL,
	[LuongCoBan] [money] NULL,
	[HoaHong] [money] NULL,
	[Discount] [money] NULL,
	[LoiNhuan] [money] NULL,
	[OwnerID] [nvarchar](20) NULL,
	[Description] [nvarchar](255) NULL,
 CONSTRAINT [PK_EMPLOYEE] PRIMARY KEY CLUSTERED 
(
	[NhanVien_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHAPKHO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHAPKHO](
	[ID] [varchar](20) NOT NULL,
	[RefDate] [datetime] NULL,
	[Ref_OrgNo] [nvarchar](20) NULL,
	[RefType] [int] NULL,
	[RefStatus] [int] NULL,
	[PhuongThucThanhToan] [uniqueidentifier] NULL,
	[TermID] [nvarchar](20) NULL,
	[NgayThanhToan] [datetime] NULL,
	[DeliveryDate] [datetime] NULL,
	[MaVach] [varchar](20) NULL,
	[ID_branch] [nvarchar](20) NULL,
	[IdNhanVien] [varchar](20) NULL,
	[IdHangTon] [varchar](20) NULL,
	[IdChiNhanh] [varchar](20) NULL,
	[Contract_ID] [varchar](20) NULL,
	[IdKhachHang] [varchar](20) NULL,
	[TenKhachHang] [nvarchar](255) NULL,
	[DiaChiKhachHang] [nvarchar](255) NULL,
	[LienLac] [nvarchar](100) NULL,
	[Reason] [nvarchar](255) NULL,
	[ThanhToan] [smallint] NULL,
	[IdTienTe] [varchar](3) NULL,
	[TyGia] [money] NULL,
	[Vat] [int] NULL,
	[VatAmount] [money] NULL,
	[Amount] [money] NULL,
	[FAmount] [money] NULL,
	[NgayGiamGia] [datetime] NULL,
	[LaiXuatChietKhau] [money] NULL,
	[ChietKhau] [money] NULL,
	[CacLoaiGiamGiaKhac] [money] NULL,
	[Charge] [money] NULL,
	[IsClose] [bit] NULL,
	[MoTa] [nvarchar](255) NULL,
	[SapXep] [bigint] NULL,
	[User_ID] [varchar](20) NULL,
	[Active] [bit] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_STOCK_INWARD] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHOM_BAOCAO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHOM_BAOCAO](
	[IdNhom_BaoCao] [varchar](20) NOT NULL,
	[TenNhomBaoCao] [nvarchar](50) NULL,
	[MoTa] [nvarchar](255) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_REPORT_GROUP] PRIMARY KEY CLUSTERED 
(
	[IdNhom_BaoCao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHOMKHACHHANG]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHOMKHACHHANG](
	[ID_NhomKH] [varchar](20) NOT NULL,
	[Ten] [nvarchar](255) NOT NULL,
	[MoTa] [nvarchar](255) NULL,
	[KichHoat] [bit] NOT NULL,
 CONSTRAINT [PK_NHOMKHACHHANG] PRIMARY KEY CLUSTERED 
(
	[ID_NhomKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHOMSANPHAM]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHOMSANPHAM](
	[NhomSanPham_ID] [varchar](20) NOT NULL,
	[NhomSanPham_Name] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[IsMain] [bit] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_PRODUCTGROUP] PRIMARY KEY CLUSTERED 
(
	[NhomSanPham_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NO_NHACUNGCAP]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_NHACUNGCAP](
	[ID] [uniqueidentifier] NOT NULL,
	[RefID] [nvarchar](20) NOT NULL,
	[RefDate] [datetime] NOT NULL,
	[RefOrgNo] [nvarchar](20) NOT NULL,
	[RefType] [smallint] NOT NULL,
	[RefStatus] [smallint] NOT NULL,
	[PhuongThucThanhToan] [uniqueidentifier] NOT NULL,
	[IdKhachHang] [nvarchar](20) NOT NULL,
	[IdSanPham] [nvarchar](20) NOT NULL,
	[TenSanPham] [nvarchar](255) NULL,
	[IdTienTe] [nvarchar](20) NULL,
	[TyGiaNgoaiTe] [money] NULL,
	[Quantity] [money] NULL,
	[Amount] [money] NULL,
	[ThanhToan] [money] NULL,
	[Chietkhau] [money] NULL,
	[IsChanged] [bit] NULL,
	[IsPublic] [bit] NULL,
	[NguoiLapPhieu] [nvarchar](20) NULL,
	[NgayLapPhieu] [datetime] NULL,
	[NguoiThayDoi] [nvarchar](20) NULL,
	[NgayThayDoi] [datetime] NULL,
	[IdDaiLyCungCap] [nvarchar](20) NULL,
	[MoTa] [nvarchar](255) NULL,
	[SapXep] [bigint] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_DEBT_TRANSACTION] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PHANMEM]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHANMEM](
	[IdPhanMem] [int] NOT NULL,
	[Ten] [nvarchar](255) NOT NULL,
	[PhienBanMoi] [varchar](50) NULL,
	[Ngay] [datetime] NULL,
	[VerHistory] [nvarchar](500) NULL,
 CONSTRAINT [PK_SOFT] PRIMARY KEY CLUSTERED 
(
	[IdPhanMem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHIEUCHITIEN]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHIEUCHITIEN](
	[ID] [nvarchar](20) NOT NULL,
	[ID_Phieu] [nvarchar](20) NULL,
	[NgayTao] [datetime] NULL,
	[SoPhieuNhapTay] [nvarchar](20) NULL,
	[HinhThucThanhToan] [uniqueidentifier] NULL,
	[LyDoChi] [nvarchar](255) NULL,
	[SoHopDong] [nvarchar](20) NULL,
	[TenKhachHang] [nvarchar](255) NULL,
	[DiaChi] [nvarchar](255) NULL,
	[MST] [nvarchar](20) NULL,
	[NguoiLienHe] [nvarchar](100) NULL,
	[TiGiaQuyDoi] [money] NULL,
	[TongTien] [money] NULL,
	[ChietKhau] [money] NULL,
	[DaNhanTien] [bit] NULL,
	[NguoiTao] [nvarchar](20) NULL,
	[NguoiChinhSua] [nvarchar](20) NULL,
	[NgayChinhSua] [datetime] NULL,
	[MoTa] [nvarchar](255) NULL,
	[ViTri] [bigint] NULL,
	[KichHoat] [bit] NULL,
	[ID_TienTe] [varchar](3) NULL,
	[ID_KhachHang] [varchar](20) NULL,
 CONSTRAINT [PK_PHIEUCHITIEN] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHIEUTHUNOKHACHHANG]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PHIEUTHUNOKHACHHANG](
	[ID] [uniqueidentifier] NOT NULL,
	[ID_Phieu] [nvarchar](20) NULL,
	[NgayLapPhieu] [datetime] NULL,
	[SoPhieuNhapTay] [nvarchar](20) NULL,
	[HinhThucThanhToan] [uniqueidentifier] NULL,
	[ID_TienTe] [nvarchar](3) NULL,
	[TiGiaQuyDoi] [money] NULL,
	[SoHopDong] [nvarchar](20) NULL,
	[ID_KhachHang] [nvarchar](20) NULL,
	[TenKhachHang] [nvarchar](255) NULL,
	[DiaChi] [nvarchar](255) NULL,
	[MST] [nvarchar](20) NULL,
	[TenLienHe] [nvarchar](100) NULL,
	[TongTien] [money] NULL,
	[NguoiTao] [nvarchar](20) NULL,
	[NgayTao] [datetime] NULL,
	[NguoiChinhSua] [nvarchar](20) NULL,
	[NgayChinhSua] [datetime] NULL,
	[MoTa] [nvarchar](255) NULL,
	[ViTri] [bigint] NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_PHIEUTHUNOKHACHHANG] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PHIEUTHUTIEN]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHIEUTHUTIEN](
	[ID] [uniqueidentifier] NOT NULL,
	[ID_Phieu] [nvarchar](20) NULL,
	[NgayTao] [datetime] NULL,
	[SoPhieuNhapTay] [nvarchar](20) NULL,
	[HinhThucThanhToan] [uniqueidentifier] NULL,
	[LyDoThu] [nvarchar](255) NULL,
	[SoHopDong] [nvarchar](20) NULL,
	[TenKhachHang] [nvarchar](255) NULL,
	[DiaChi] [nvarchar](255) NULL,
	[MST] [nvarchar](20) NULL,
	[TenNguoiLienHe] [nvarchar](100) NULL,
	[TiGiaQuyDoi] [money] NULL,
	[ThanhTien] [money] NULL,
	[ChietKhau] [money] NULL,
	[IsReceived] [bit] NULL,
	[IsClosed] [bit] NULL,
	[IsPublic] [bit] NULL,
	[NguoiTao] [nvarchar](20) NULL,
	[NguoiChinhSua] [nvarchar](20) NULL,
	[NgayChinhSua] [datetime] NULL,
	[MoTa] [nvarchar](255) NULL,
	[ViTri] [bigint] NULL,
	[KichHoat] [bit] NULL,
	[ID_KhachHang] [varchar](20) NULL,
	[ID_TienTe] [varchar](3) NULL,
 CONSTRAINT [PK_PHIEUTHUTIEN] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PRODUCT_MODEL]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUCT_MODEL](
	[ID] [uniqueidentifier] NULL,
	[Code] [nvarchar](20) NULL,
	[Name] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RECEIPT_GROUP]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RECEIPT_GROUP](
	[Receipt_Group_ID] [varchar](20) NOT NULL,
	[Receipt_Group_Name] [nvarchar](100) NULL,
	[MoTa] [nvarchar](255) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_RECEIPT_GROUP] PRIMARY KEY CLUSTERED 
(
	[Receipt_Group_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[REFTYPE]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REFTYPE](
	[ID] [smallint] NOT NULL,
	[Ten] [nvarchar](255) NULL,
	[NameEN] [nvarchar](255) NULL,
	[IdDanhMuc] [smallint] NULL,
	[SapXep] [bigint] NULL,
	[IsSearch] [bit] NULL,
 CONSTRAINT [PK_REFTYPE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SANPHAM]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SANPHAM](
	[SanPham_ID] [varchar](20) NOT NULL,
	[SanPham_Name] [nvarchar](255) NULL,
	[SanPham_NameEN] [nvarchar](255) NULL,
	[SanPham_Type_ID] [int] NULL,
	[NhaSX_ID] [int] NULL,
	[Loai_ID] [nvarchar](20) NULL,
	[SanPham_Group_ID] [varchar](20) NULL,
	[NhaCungCap_ID] [varchar](20) NULL,
	[NguonGoc] [nvarchar](100) NULL,
	[MaVach] [varchar](20) NULL,
	[DonVi] [nvarchar](20) NULL,
	[DonViChuyenDoi] [nvarchar](20) NULL,
	[UnitRate] [money] NULL,
	[GiaBan] [money] NULL,
	[GiaBanLe] [money] NULL,
	[SoLuong] [money] NULL,
	[ChiPhiHienTai] [money] NULL,
	[ChiPhiTrungBinh] [money] NULL,
	[SuDamBao] [int] NULL,
	[VAT_ID] [money] NULL,
	[ImportTax_ID] [money] NULL,
	[ExportTax_ID] [money] NULL,
	[LuxuryTax_ID] [money] NULL,
	[KhachHang_ID] [varchar](20) NULL,
	[KhachHang_Name] [nvarchar](255) NULL,
	[HinhThucThanhToan] [smallint] NULL,
	[TonKho_Min] [money] NULL,
	[TonKho_Max] [money] NULL,
	[ChietKhau] [money] NULL,
	[UyQuyen] [money] NULL,
	[Description] [nvarchar](255) NULL,
	[Color] [nvarchar](50) NULL,
	[HanSuDung] [bit] NULL,
	[GioiHanDatHAng] [money] NULL,
	[GiaTriHanSD] [money] NULL,
	[Lô] [bit] NULL,
	[Depth] [money] NULL,
	[Height] [money] NULL,
	[Width] [money] NULL,
	[Thickness] [money] NULL,
	[Size] [nvarchar](50) NULL,
	[Photo] [image] NULL,
	[TienTe_ID] [varchar](3) NULL,
	[TyGia] [money] NULL,
	[TonKho_ID] [varchar](20) NULL,
	[UserID] [varchar](20) NULL,
	[Serial] [bit] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_SanPham] PRIMARY KEY CLUSTERED 
(
	[SanPham_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_CONGTY]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYS_CONGTY](
	[CongTy_Id] [varchar](20) NOT NULL,
	[TenCongTy] [nvarchar](250) NULL,
	[DiaChi] [nvarchar](250) NULL,
	[DT] [varchar](50) NULL,
	[Fax] [varchar](50) NULL,
	[WebSite] [varchar](250) NULL,
	[Email] [varchar](50) NULL,
	[Tax] [varchar](50) NULL,
	[Licence] [nvarchar](50) NULL,
	[Photo] [image] NULL,
 CONSTRAINT [PK_SYS_COMPANY] PRIMARY KEY CLUSTERED 
(
	[CongTy_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_LOG]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYS_LOG](
	[SYS_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](20) NULL,
	[ThoiGian] [datetime] NULL,
	[ChucNang] [nvarchar](200) NULL,
	[MaHanhDong] [int] NULL,
	[TenHanhDong] [nvarchar](50) NULL,
	[MoTa] [nvarchar](255) NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_SYS_LOG] PRIMARY KEY CLUSTERED 
(
	[SYS_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_NGUOIDUNG]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYS_NGUOIDUNG](
	[UserID] [varchar](20) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [varchar](50) NULL,
	[Nhom_ID] [varchar](20) NOT NULL,
	[MoTa] [nvarchar](255) NULL,
	[PartID] [varchar](20) NULL,
	[KichHoat] [bit] NOT NULL CONSTRAINT [DF_Table_1_System]  DEFAULT ((0)),
 CONSTRAINT [PK_SYS_USER] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_NGUOIDUNG_QUYTAC]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC](
	[Nhom_ID] [varchar](20) NOT NULL,
	[DoiTuong_ID] [varchar](50) NOT NULL,
	[QuyTac_ID] [varchar](20) NOT NULL,
	[Them_All] [bit] NULL,
	[Xoa_All] [bit] NULL,
	[Sua_All] [bit] NULL,
	[TruyCap_All] [bit] NULL,
	[In_All] [bit] NULL,
	[Nhap_All] [bit] NULL,
	[Xuat_All] [bit] NULL,
	[KichHoat] [bit] NOT NULL,
 CONSTRAINT [PK_SYS_USER_RULE_1] PRIMARY KEY CLUSTERED 
(
	[Nhom_ID] ASC,
	[DoiTuong_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_NGUOIQUANLY_KHO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYS_NGUOIQUANLY_KHO](
	[NguoiQLID] [varchar](50) NOT NULL,
	[KhoID] [varchar](50) NOT NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_SYS_USER_STOCK] PRIMARY KEY CLUSTERED 
(
	[NguoiQLID] ASC,
	[KhoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_NHOM]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYS_NHOM](
	[Nhom_ID] [varchar](20) NOT NULL,
	[TenNhom] [nvarchar](100) NULL,
	[MoTa] [nvarchar](255) NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_SYS_GROUP] PRIMARY KEY CLUSTERED 
(
	[Nhom_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_NHOM_DOITUONG]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYS_NHOM_DOITUONG](
	[DoiTuong_ID] [varchar](50) NOT NULL,
	[Nhom_ID] [varchar](20) NOT NULL,
	[KichHoat] [nchar](10) NULL,
 CONSTRAINT [PK_SYS_GROUP_OBJECT] PRIMARY KEY CLUSTERED 
(
	[DoiTuong_ID] ASC,
	[Nhom_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_NHOM_KHO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYS_NHOM_KHO](
	[NhomID] [varchar](50) NOT NULL,
	[KhoID] [varchar](50) NOT NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_SYS_GROUP_STOCK] PRIMARY KEY CLUSTERED 
(
	[NhomID] ASC,
	[KhoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_QUYTAC]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYS_QUYTAC](
	[QuyTac_ID] [varchar](20) NOT NULL,
	[TenQuyTac] [nvarchar](100) NULL,
	[MoTa] [nvarchar](255) NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_SYS_RULE] PRIMARY KEY CLUSTERED 
(
	[QuyTac_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_TUYCHON]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYS_TUYCHON](
	[TuyChon_ID] [varchar](50) NOT NULL,
	[OptionValue] [nvarchar](100) NULL,
	[ValueType] [int] NULL,
	[System] [bit] NULL,
	[MoTa] [nvarchar](255) NULL,
 CONSTRAINT [PK_SYS_OPTION] PRIMARY KEY CLUSTERED 
(
	[TuyChon_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[THANHTOAN]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[THANHTOAN](
	[ThanhToan_ID] [varchar](20) NULL,
	[ThanhToan_Date] [datetime] NULL,
	[ThanhToan_No] [varchar](20) NULL,
	[NguoiNhanName] [nvarchar](100) NULL,
	[TienTe_ID] [varchar](3) NULL,
	[GiaoDich] [money] NULL,
	[MaKhacHang_ID] [nchar](10) NULL,
	[KhachHang_Name] [nvarchar](255) NULL,
	[KhachHang_Address] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[NhomThanhToan_ID] [varchar](20) NULL,
	[Tong] [money] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[THANHTOAN_GROUP]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[THANHTOAN_GROUP](
	[ThanhToan_Group_ID] [varchar](20) NOT NULL,
	[ThanhToan_Group_Name] [nvarchar](100) NULL,
	[Description] [nvarchar](255) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_PAYMENT_GROUP] PRIMARY KEY CLUSTERED 
(
	[ThanhToan_Group_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[THANHTOAN_NHACUNGCAP]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[THANHTOAN_NHACUNGCAP](
	[ID] [uniqueidentifier] NOT NULL,
	[RefID] [nvarchar](20) NULL,
	[RefDate] [datetime] NULL,
	[RefOrgNo] [nvarchar](20) NULL,
	[RefType] [smallint] NULL,
	[RefStatus] [smallint] NULL,
	[PhuongThucThanhToan] [uniqueidentifier] NULL,
	[MaVach] [nvarchar](20) NULL,
	[IdTienTe] [nvarchar](3) NULL,
	[TyGiaNgoaiTe] [money] NULL,
	[IdChiNhanh] [nvarchar](20) NULL,
	[ContractID] [nvarchar](20) NULL,
	[IdKhachHang] [nvarchar](20) NULL,
	[TenKhachHang] [nvarchar](255) NULL,
	[DiaChiKhachHang] [nvarchar](255) NULL,
	[FaxCuaKhachHang] [nvarchar](20) NULL,
	[NguoiLienHe] [nvarchar](100) NULL,
	[Amount] [money] NULL,
	[Discount] [money] NULL,
	[IsPublic] [bit] NOT NULL,
	[NguoiLapPhieu] [nvarchar](20) NULL,
	[NgayLapPhieu] [datetime] NULL,
	[NguoiSuaDoi] [nvarchar](20) NULL,
	[NgaySuaDoi] [datetime] NULL,
	[IdDaiLyCungCap] [nvarchar](20) NULL,
	[MoTa] [nvarchar](255) NULL,
	[SapXep] [bigint] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_PROVIDER_PAYMENT_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[THONGTINCONGTY]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[THONGTINCONGTY](
	[Id] [varchar](50) NOT NULL,
	[Ten] [nvarchar](255) NULL,
	[DiaChi] [nvarchar](255) NULL,
	[MST] [varchar](50) NULL,
	[DienThoai] [varchar](30) NULL,
	[Fax] [varchar](50) NULL,
	[WebSite] [varchar](30) NULL,
	[Email] [varchar](30) NULL,
	[Logo] [image] NULL,
 CONSTRAINT [PK_THONGTINCONGTY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TIENTE]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TIENTE](
	[ID_TienTe] [varchar](3) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[TiGiaQuyDoi] [money] NULL,
	[KichHoat] [bit] NULL,
 CONSTRAINT [PK_TIENTE] PRIMARY KEY CLUSTERED 
(
	[ID_TienTe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[XAYDUNGSANPHAM]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[XAYDUNGSANPHAM](
	[SanPhamID] [varchar](20) NOT NULL,
	[BuildID] [varchar](20) NOT NULL,
	[SoLuong] [money] NULL,
	[Gia] [money] NULL,
	[Tong] [money] NULL,
 CONSTRAINT [PK_SanPham_BUILD] PRIMARY KEY CLUSTERED 
(
	[SanPhamID] ASC,
	[BuildID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[XUAT_KHO]    Script Date: 8/24/2016 2:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[XUAT_KHO](
	[ID] [varchar](20) NOT NULL,
	[RefDate] [datetime] NULL,
	[Ref_OrgNo] [nvarchar](20) NULL,
	[RefType] [int] NULL,
	[RefStatus] [int] NULL,
	[HinhThucThanhToan] [uniqueidentifier] NULL,
	[TermID] [nvarchar](20) NULL,
	[NgayThanhToan] [datetime] NULL,
	[NgayGiaoHang] [datetime] NULL,
	[Barcode] [varchar](20) NULL,
	[Department_ID] [nvarchar](20) NULL,
	[NhanVien_ID] [varchar](20) NULL,
	[Kho_ID] [varchar](20) NULL,
	[ChiNhanh_ID] [varchar](20) NULL,
	[HopDong_ID] [varchar](20) NULL,
	[KhachHnag_ID] [varchar](20) NULL,
	[TenKhachHang] [nvarchar](255) NULL,
	[DiaChiKhachHang] [nvarchar](255) NULL,
	[LienHe] [nvarchar](100) NULL,
	[ThanhToan] [smallint] NULL,
	[TienTe_ID] [varchar](3) NULL,
	[TyGia] [money] NULL,
	[Vat] [int] NULL,
	[SoTien_Vat] [money] NULL,
	[SoLuong] [money] NULL,
	[NgayChietKhau] [datetime] NULL,
	[TyLeChietKhau] [money] NULL,
	[ChietKhau] [money] NULL,
	[phiVanChuyen] [money] NULL,
	[ChiPhi] [money] NULL,
	[NguoiDung_ID] [varchar](20) NULL,
	[MoTa] [nvarchar](255) NULL,
	[KichHoat] [bit] NOT NULL,
 CONSTRAINT [PK_INWARD] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[HANGTON] ([IdHangTon], [TenHangTon], [LienLac], [DiaChi], [Email], [SDT], [Fax], [Mobi], [QuanLy], [MoTa], [Active]) VALUES (N'ANS0064', N'Tem từ giấy 4x4 không mã vạch', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[HANGTON] ([IdHangTon], [TenHangTon], [LienLac], [DiaChi], [Email], [SDT], [Fax], [Mobi], [QuanLy], [MoTa], [Active]) VALUES (N'ANS0090', N'Tem từ giấy  3x4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[HANGTON] ([IdHangTon], [TenHangTon], [LienLac], [DiaChi], [Email], [SDT], [Fax], [Mobi], [QuanLy], [MoTa], [Active]) VALUES (N'CAM0287', N'Camera NC-131HD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[HANGTON] ([IdHangTon], [TenHangTon], [LienLac], [DiaChi], [Email], [SDT], [Fax], [Mobi], [QuanLy], [MoTa], [Active]) VALUES (N'DAC0005', N'Cáp đồng trục RG6 (Unisat)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[HANGTON] ([IdHangTon], [TenHangTon], [LienLac], [DiaChi], [Email], [SDT], [Fax], [Mobi], [QuanLy], [MoTa], [Active]) VALUES (N'DAG0040', N'Dầu ghi QTX6408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[HANGTON] ([IdHangTon], [TenHangTon], [LienLac], [DiaChi], [Email], [SDT], [Fax], [Mobi], [QuanLy], [MoTa], [Active]) VALUES (N'OCU0013', N'Ổ cứng seagate 3Tb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[HANGTON] ([IdHangTon], [TenHangTon], [LienLac], [DiaChi], [Email], [SDT], [Fax], [Mobi], [QuanLy], [MoTa], [Active]) VALUES (N'TBK0005', N'Cục nguồn 12V-2A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[HANGTON] ([IdHangTon], [TenHangTon], [LienLac], [DiaChi], [Email], [SDT], [Fax], [Mobi], [QuanLy], [MoTa], [Active]) VALUES (N'TBK0105', N'Chân đế cố định J703', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[HANGTON] ([IdHangTon], [TenHangTon], [LienLac], [DiaChi], [Email], [SDT], [Fax], [Mobi], [QuanLy], [MoTa], [Active]) VALUES (N'TBK0148', N'Chân đế NC-224', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[HANGTON] ([IdHangTon], [TenHangTon], [LienLac], [DiaChi], [Email], [SDT], [Fax], [Mobi], [QuanLy], [MoTa], [Active]) VALUES (N'TBV0006', N'Thiết bị định vị 1000TL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)

INSERT [dbo].[BAOCAO] ([IdBaoCao], [TenBaoCao], [TieuDe], [BinhLuan], [TenFile], [Mota], [DataSet], [Class], [Parent_ID], [Order], [Avtive]) VALUES (N'BC01', N'Tồn Kho Tháng 1', N'TỒN KHO', NULL, N'TonKho01', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BAOCAO] ([IdBaoCao], [TenBaoCao], [TieuDe], [BinhLuan], [TenFile], [Mota], [DataSet], [Class], [Parent_ID], [Order], [Avtive]) VALUES (N'BC02', N'Tồn Kho Tháng 2', N'TỒN KHO', NULL, N'TonKho02', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BAOCAO] ([IdBaoCao], [TenBaoCao], [TieuDe], [BinhLuan], [TenFile], [Mota], [DataSet], [Class], [Parent_ID], [Order], [Avtive]) VALUES (N'BC03', N'Tồn Kho Tháng 3', N'TỒN KHO', NULL, N'TonKho03', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BAOCAO] ([IdBaoCao], [TenBaoCao], [TieuDe], [BinhLuan], [TenFile], [Mota], [DataSet], [Class], [Parent_ID], [Order], [Avtive]) VALUES (N'BC04', N'Tồn Kho Tháng 4', N'TỒN KHO', NULL, N'TonKho04', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BAOCAO] ([IdBaoCao], [TenBaoCao], [TieuDe], [BinhLuan], [TenFile], [Mota], [DataSet], [Class], [Parent_ID], [Order], [Avtive]) VALUES (N'BC05', N'Tồn Kho Tháng 5', N'TỒN KHO', NULL, N'TonKho05', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BAOCAO] ([IdBaoCao], [TenBaoCao], [TieuDe], [BinhLuan], [TenFile], [Mota], [DataSet], [Class], [Parent_ID], [Order], [Avtive]) VALUES (N'BC06', N'Tồn Kho Tháng 6', N'TỒN KHO', NULL, N'TonKho06', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BAOCAO] ([IdBaoCao], [TenBaoCao], [TieuDe], [BinhLuan], [TenFile], [Mota], [DataSet], [Class], [Parent_ID], [Order], [Avtive]) VALUES (N'BC07', N'Tồn Kho Tháng 7', N'TỒN KHO', NULL, N'TonKho07', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BAOCAO] ([IdBaoCao], [TenBaoCao], [TieuDe], [BinhLuan], [TenFile], [Mota], [DataSet], [Class], [Parent_ID], [Order], [Avtive]) VALUES (N'BC08', N'Tồn Kho Tháng 8', N'TỒN KHO', NULL, N'TonKho08', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BAOCAO] ([IdBaoCao], [TenBaoCao], [TieuDe], [BinhLuan], [TenFile], [Mota], [DataSet], [Class], [Parent_ID], [Order], [Avtive]) VALUES (N'BC09', N'Tồn Kho Tháng 9', N'TỒN KHO', NULL, N'TonKho09', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BAOCAO] ([IdBaoCao], [TenBaoCao], [TieuDe], [BinhLuan], [TenFile], [Mota], [DataSet], [Class], [Parent_ID], [Order], [Avtive]) VALUES (N'BC10', N'Tồn Kho Tháng 10', N'TỒN KHO', NULL, N'TonKho10', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BienLai] ([IdBienLai], [Receipt_Date], [Receipt_No], [NguoiGiaoHang], [IdTienTe], [GiaoDich], [IdKhachHang], [TenKhachHang], [DiaChiKhachHang], [MoTa], [Receipt_Group_ID], [Amount]) VALUES (N'BL01', CAST(N'2016-01-01 00:00:00.000' AS DateTime), N'01', N'Luật', N'T01', 1000000.0000, N'MAKH01', N'Anh Chánh', N'Q7', NULL, NULL, 1.0000)
INSERT [dbo].[BienLai] ([IdBienLai], [Receipt_Date], [Receipt_No], [NguoiGiaoHang], [IdTienTe], [GiaoDich], [IdKhachHang], [TenKhachHang], [DiaChiKhachHang], [MoTa], [Receipt_Group_ID], [Amount]) VALUES (N'BL02', CAST(N'2016-01-02 00:00:00.000' AS DateTime), N'02', N'Luật', N'T01', 2500000.0000, N'MAKH02', N'Chị Như', N'Q9', NULL, NULL, NULL)
INSERT [dbo].[BienLai] ([IdBienLai], [Receipt_Date], [Receipt_No], [NguoiGiaoHang], [IdTienTe], [GiaoDich], [IdKhachHang], [TenKhachHang], [DiaChiKhachHang], [MoTa], [Receipt_Group_ID], [Amount]) VALUES (N'BL03', CAST(N'2015-04-09 00:00:00.000' AS DateTime), N'03', N'Luật', N'T01', 300000.0000, N'MAKH03', N'Chị Hoa', N'Q1', NULL, NULL, NULL)
INSERT [dbo].[BienLai] ([IdBienLai], [Receipt_Date], [Receipt_No], [NguoiGiaoHang], [IdTienTe], [GiaoDich], [IdKhachHang], [TenKhachHang], [DiaChiKhachHang], [MoTa], [Receipt_Group_ID], [Amount]) VALUES (N'BL04', CAST(N'2014-01-09 00:00:00.000' AS DateTime), N'04', N'Hữu', N'T02', 500000.0000, N'MAKH04', N'Anh Thọ', N'Vũng Tàu', NULL, NULL, NULL)
INSERT [dbo].[BienLai] ([IdBienLai], [Receipt_Date], [Receipt_No], [NguoiGiaoHang], [IdTienTe], [GiaoDich], [IdKhachHang], [TenKhachHang], [DiaChiKhachHang], [MoTa], [Receipt_Group_ID], [Amount]) VALUES (N'BL05', CAST(N'2016-01-11 00:00:00.000' AS DateTime), N'05', N'Luật', N'T01', 300000.0000, N'MAKH05', N'Chị Lệ', N'Q1', NULL, NULL, NULL)
INSERT [dbo].[BienLai] ([IdBienLai], [Receipt_Date], [Receipt_No], [NguoiGiaoHang], [IdTienTe], [GiaoDich], [IdKhachHang], [TenKhachHang], [DiaChiKhachHang], [MoTa], [Receipt_Group_ID], [Amount]) VALUES (N'BL06', CAST(N'2015-09-22 00:00:00.000' AS DateTime), N'06', N'Luật', N'T01', 26660000.0000, N'MAKH05', N'Chị Lệ', N'Q1', NULL, NULL, NULL)
INSERT [dbo].[BienLai] ([IdBienLai], [Receipt_Date], [Receipt_No], [NguoiGiaoHang], [IdTienTe], [GiaoDich], [IdKhachHang], [TenKhachHang], [DiaChiKhachHang], [MoTa], [Receipt_Group_ID], [Amount]) VALUES (N'BL07', CAST(N'2014-11-30 00:00:00.000' AS DateTime), N'07', N'Luật', N'T01', 3620000.0000, N'MAKH05', N'Chị Lệ', N'Q1', NULL, NULL, NULL)
INSERT [dbo].[BienLai] ([IdBienLai], [Receipt_Date], [Receipt_No], [NguoiGiaoHang], [IdTienTe], [GiaoDich], [IdKhachHang], [TenKhachHang], [DiaChiKhachHang], [MoTa], [Receipt_Group_ID], [Amount]) VALUES (N'BL08', CAST(N'2014-11-30 00:00:00.000' AS DateTime), N'08', N'An', N'T01', 950000.0000, N'MAKH06', N'Chị Hường', N'Tiền Giang', NULL, NULL, NULL)
INSERT [dbo].[BienLai] ([IdBienLai], [Receipt_Date], [Receipt_No], [NguoiGiaoHang], [IdTienTe], [GiaoDich], [IdKhachHang], [TenKhachHang], [DiaChiKhachHang], [MoTa], [Receipt_Group_ID], [Amount]) VALUES (N'BL09', CAST(N'2014-11-30 00:00:00.000' AS DateTime), N'09', N'Luật', N'T01', 125400000.0000, N'MAKH07', N'Chị Thu', N'Vĩnh long', NULL, NULL, NULL)
INSERT [dbo].[BienLai] ([IdBienLai], [Receipt_Date], [Receipt_No], [NguoiGiaoHang], [IdTienTe], [GiaoDich], [IdKhachHang], [TenKhachHang], [DiaChiKhachHang], [MoTa], [Receipt_Group_ID], [Amount]) VALUES (N'BL10', CAST(N'2014-11-30 00:00:00.000' AS DateTime), N'10', N'Thái', N'T02', 1326666.0000, N'MAKH08', N'Bác Tuấn', N'Cà Mau', NULL, NULL, NULL)
INSERT [dbo].[LAPPHIEUHANGTON] ([ID], [RefDate], [Ref_OrgNo], [RefType], [MaVach], [IdTinh], [IdNhanVien], [FromStock_ID], [ToStock_ID], [IdChiNhanh], [IdHopDong], [IdSanPham], [IdTienTe], [TenSanPham], [TyGiaTienTe], [Quantity], [Amount], [IsClose], [MoTa], [IdNguoiDung], [SapXep], [Active]) VALUES (N'01', NULL, NULL, NULL, NULL, NULL, N'NV01', NULL, NULL, N'CN01', N'HD01', N'TBK0005', NULL, N'Cục nguồn 12V-2A', 1.0000, 1.0000, 1.0000, NULL, NULL, N'ND01', NULL, 1)
INSERT [dbo].[LAPPHIEUHANGTON] ([ID], [RefDate], [Ref_OrgNo], [RefType], [MaVach], [IdTinh], [IdNhanVien], [FromStock_ID], [ToStock_ID], [IdChiNhanh], [IdHopDong], [IdSanPham], [IdTienTe], [TenSanPham], [TyGiaTienTe], [Quantity], [Amount], [IsClose], [MoTa], [IdNguoiDung], [SapXep], [Active]) VALUES (N'02', NULL, NULL, NULL, NULL, NULL, N'NV02', NULL, NULL, N'CN02', N'HD02', N'TBV0006', NULL, N'Thiết bị định vị 1000TL', 1.0000, 2.0000, 1.0000, NULL, NULL, N'ND01', NULL, 1)
INSERT [dbo].[LAPPHIEUHANGTON] ([ID], [RefDate], [Ref_OrgNo], [RefType], [MaVach], [IdTinh], [IdNhanVien], [FromStock_ID], [ToStock_ID], [IdChiNhanh], [IdHopDong], [IdSanPham], [IdTienTe], [TenSanPham], [TyGiaTienTe], [Quantity], [Amount], [IsClose], [MoTa], [IdNguoiDung], [SapXep], [Active]) VALUES (N'03', NULL, NULL, NULL, NULL, NULL, N'NV03', NULL, NULL, N'CN03', N'HD03', N'TBK0148', NULL, N'Chân đế NC-224', 1.0000, 3.0000, 1.0000, NULL, NULL, N'ND01', NULL, 1)
INSERT [dbo].[LAPPHIEUHANGTON] ([ID], [RefDate], [Ref_OrgNo], [RefType], [MaVach], [IdTinh], [IdNhanVien], [FromStock_ID], [ToStock_ID], [IdChiNhanh], [IdHopDong], [IdSanPham], [IdTienTe], [TenSanPham], [TyGiaTienTe], [Quantity], [Amount], [IsClose], [MoTa], [IdNguoiDung], [SapXep], [Active]) VALUES (N'04', NULL, NULL, NULL, NULL, NULL, N'NV04', NULL, NULL, N'CN04', N'HD04', N'CAM0287', NULL, N'Camera NC-131HD', 1.0000, 4.0000, 1.0000, NULL, NULL, N'ND01', NULL, 1)
INSERT [dbo].[LAPPHIEUHANGTON] ([ID], [RefDate], [Ref_OrgNo], [RefType], [MaVach], [IdTinh], [IdNhanVien], [FromStock_ID], [ToStock_ID], [IdChiNhanh], [IdHopDong], [IdSanPham], [IdTienTe], [TenSanPham], [TyGiaTienTe], [Quantity], [Amount], [IsClose], [MoTa], [IdNguoiDung], [SapXep], [Active]) VALUES (N'05', NULL, NULL, NULL, NULL, NULL, N'NV05', NULL, NULL, N'CN05', N'HD05', N'TBK0105', NULL, N'Chân đế cố định J703', 1.0000, 5.0000, 1.0000, NULL, NULL, N'ND01', NULL, 1)
INSERT [dbo].[LAPPHIEUHANGTON] ([ID], [RefDate], [Ref_OrgNo], [RefType], [MaVach], [IdTinh], [IdNhanVien], [FromStock_ID], [ToStock_ID], [IdChiNhanh], [IdHopDong], [IdSanPham], [IdTienTe], [TenSanPham], [TyGiaTienTe], [Quantity], [Amount], [IsClose], [MoTa], [IdNguoiDung], [SapXep], [Active]) VALUES (N'06', NULL, NULL, NULL, NULL, NULL, N'NV06', NULL, NULL, N'CN06', N'HD06', N'DAG0040', NULL, N'Dầu ghi QTX6408', 1.0000, 6.0000, 1.0000, NULL, NULL, N'ND01', NULL, 1)
INSERT [dbo].[LAPPHIEUHANGTON] ([ID], [RefDate], [Ref_OrgNo], [RefType], [MaVach], [IdTinh], [IdNhanVien], [FromStock_ID], [ToStock_ID], [IdChiNhanh], [IdHopDong], [IdSanPham], [IdTienTe], [TenSanPham], [TyGiaTienTe], [Quantity], [Amount], [IsClose], [MoTa], [IdNguoiDung], [SapXep], [Active]) VALUES (N'07', NULL, NULL, NULL, NULL, NULL, N'NV07', NULL, NULL, N'CN07', N'HD07', N'DAC0005', NULL, N'Cáp đồng trục RG6 (Unisat)', 1.0000, 7.0000, 1.0000, NULL, NULL, N'ND01', NULL, 1)
INSERT [dbo].[LAPPHIEUHANGTON] ([ID], [RefDate], [Ref_OrgNo], [RefType], [MaVach], [IdTinh], [IdNhanVien], [FromStock_ID], [ToStock_ID], [IdChiNhanh], [IdHopDong], [IdSanPham], [IdTienTe], [TenSanPham], [TyGiaTienTe], [Quantity], [Amount], [IsClose], [MoTa], [IdNguoiDung], [SapXep], [Active]) VALUES (N'08', NULL, NULL, NULL, NULL, NULL, N'NV08', NULL, NULL, N'CN08', N'HD08', N'OCU0013', NULL, N'Ổ cứng seagate 3Tb', 1.0000, 8.0000, 1.0000, NULL, NULL, N'ND01', NULL, 1)
INSERT [dbo].[LAPPHIEUHANGTON] ([ID], [RefDate], [Ref_OrgNo], [RefType], [MaVach], [IdTinh], [IdNhanVien], [FromStock_ID], [ToStock_ID], [IdChiNhanh], [IdHopDong], [IdSanPham], [IdTienTe], [TenSanPham], [TyGiaTienTe], [Quantity], [Amount], [IsClose], [MoTa], [IdNguoiDung], [SapXep], [Active]) VALUES (N'09', NULL, NULL, NULL, NULL, NULL, N'NV09', NULL, NULL, N'CN09', N'HD09', N'ANS0090', NULL, N'Tem từ giấy  3x4', 1.0000, 9.0000, 1.0000, NULL, NULL, N'ND01', NULL, 1)
INSERT [dbo].[LAPPHIEUHANGTON] ([ID], [RefDate], [Ref_OrgNo], [RefType], [MaVach], [IdTinh], [IdNhanVien], [FromStock_ID], [ToStock_ID], [IdChiNhanh], [IdHopDong], [IdSanPham], [IdTienTe], [TenSanPham], [TyGiaTienTe], [Quantity], [Amount], [IsClose], [MoTa], [IdNguoiDung], [SapXep], [Active]) VALUES (N'10', NULL, NULL, NULL, NULL, NULL, N'NV10', NULL, NULL, N'CN10', N'HD10', N'ANS0064', NULL, N'Tem từ giấy 4x4 không mã vạch', 1.0000, 10.0000, 1.0000, NULL, NULL, N'ND01', NULL, 1)
INSERT [dbo].[LOAISANPHAM] ([IdLoaiSanPham], [TenSanPham]) VALUES (1, N'Hàng Hóa')
INSERT [dbo].[LOAISANPHAM] ([IdLoaiSanPham], [TenSanPham]) VALUES (2, N'Dịch Vụ')
INSERT [dbo].[NHACUNGCAP] ([IdKhachHang], [IdDatHang], [TenKhachHang], [IdLoaiKhachHang], [IdNhomKhachHang], [DiaChiKhachHang], [NgaySinh], [MaVach], [Tax], [Tel], [Fax], [Email], [Mobile], [Website], [NguoiLienHe], [DiaChi], [NickYM], [NickSky], [KhuVuc], [Quan], [QuocGia], [ThanhPho], [TaiKhoanNganHang], [TenNganHang], [GioiHanNo], [ChietKhau], [IsCongNo], [ChiTietCongNo], [LaNhaCungCap], [MoTa], [Active]) VALUES (N'KH01', 1, N'Nguyễn Thị Hoa', N'LKH01', N'NKH01', N'Ấp 2 xã Mỹ An', N'1/1/1989', NULL, NULL, N'09090990909', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Việt nam', N'Tân An', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[NHACUNGCAP] ([IdKhachHang], [IdDatHang], [TenKhachHang], [IdLoaiKhachHang], [IdNhomKhachHang], [DiaChiKhachHang], [NgaySinh], [MaVach], [Tax], [Tel], [Fax], [Email], [Mobile], [Website], [NguoiLienHe], [DiaChi], [NickYM], [NickSky], [KhuVuc], [Quan], [QuocGia], [ThanhPho], [TaiKhoanNganHang], [TenNganHang], [GioiHanNo], [ChietKhau], [IsCongNo], [ChiTietCongNo], [LaNhaCungCap], [MoTa], [Active]) VALUES (N'KH02', 2, N'Nguyễn Trọng Hòa', N'LHK02', N'NHK02', N'Số 2 Đường 3/2 Quận 2', N'1/2/1987', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Việt Nam', N'Hồ Chí Minh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[NHACUNGCAP] ([IdKhachHang], [IdDatHang], [TenKhachHang], [IdLoaiKhachHang], [IdNhomKhachHang], [DiaChiKhachHang], [NgaySinh], [MaVach], [Tax], [Tel], [Fax], [Email], [Mobile], [Website], [NguoiLienHe], [DiaChi], [NickYM], [NickSky], [KhuVuc], [Quan], [QuocGia], [ThanhPho], [TaiKhoanNganHang], [TenNganHang], [GioiHanNo], [ChietKhau], [IsCongNo], [ChiTietCongNo], [LaNhaCungCap], [MoTa], [Active]) VALUES (N'KH03', 3, N'Nguyễn Hoàng Bách', N'LHK03', N'NHK03', N'Phú Mỹ Hưng', N'1/3/1993', NULL, NULL, N'0909109199', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Việt Nam', N'Hồ Chí Minh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[NHACUNGCAP] ([IdKhachHang], [IdDatHang], [TenKhachHang], [IdLoaiKhachHang], [IdNhomKhachHang], [DiaChiKhachHang], [NgaySinh], [MaVach], [Tax], [Tel], [Fax], [Email], [Mobile], [Website], [NguoiLienHe], [DiaChi], [NickYM], [NickSky], [KhuVuc], [Quan], [QuocGia], [ThanhPho], [TaiKhoanNganHang], [TenNganHang], [GioiHanNo], [ChietKhau], [IsCongNo], [ChiTietCongNo], [LaNhaCungCap], [MoTa], [Active]) VALUES (N'KH04', 4, N'Đông Nhi', N'LHK04', N'NHK04', N'1/6 Hồng Bàng', N'28/12/1986', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Việt Nam', N'Hồ Chí Minh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[NHACUNGCAP] ([IdKhachHang], [IdDatHang], [TenKhachHang], [IdLoaiKhachHang], [IdNhomKhachHang], [DiaChiKhachHang], [NgaySinh], [MaVach], [Tax], [Tel], [Fax], [Email], [Mobile], [Website], [NguoiLienHe], [DiaChi], [NickYM], [NickSky], [KhuVuc], [Quan], [QuocGia], [ThanhPho], [TaiKhoanNganHang], [TenNganHang], [GioiHanNo], [ChietKhau], [IsCongNo], [ChiTietCongNo], [LaNhaCungCap], [MoTa], [Active]) VALUES (N'KH05', 5, N'Ông Cao Thắng', N'LHK05', N'NHK05', N'Ấp 4', N'21/3/2000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Việt Nam', N'Cần Thơ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[NHACUNGCAP] ([IdKhachHang], [IdDatHang], [TenKhachHang], [IdLoaiKhachHang], [IdNhomKhachHang], [DiaChiKhachHang], [NgaySinh], [MaVach], [Tax], [Tel], [Fax], [Email], [Mobile], [Website], [NguoiLienHe], [DiaChi], [NickYM], [NickSky], [KhuVuc], [Quan], [QuocGia], [ThanhPho], [TaiKhoanNganHang], [TenNganHang], [GioiHanNo], [ChietKhau], [IsCongNo], [ChiTietCongNo], [LaNhaCungCap], [MoTa], [Active]) VALUES (N'KH06', 6, N'Minh Hằng', N'LHK06', N'NHK06', N'Hẻm Số 5', N'21/12/1991', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Việt Nam', N'Đà Lạt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[NHACUNGCAP] ([IdKhachHang], [IdDatHang], [TenKhachHang], [IdLoaiKhachHang], [IdNhomKhachHang], [DiaChiKhachHang], [NgaySinh], [MaVach], [Tax], [Tel], [Fax], [Email], [Mobile], [Website], [NguoiLienHe], [DiaChi], [NickYM], [NickSky], [KhuVuc], [Quan], [QuocGia], [ThanhPho], [TaiKhoanNganHang], [TenNganHang], [GioiHanNo], [ChietKhau], [IsCongNo], [ChiTietCongNo], [LaNhaCungCap], [MoTa], [Active]) VALUES (N'KH07', 7, N'Tuấn Hưng', N'LHK07', N'NHK07', N'Hẻm Số 9', N'7/8/1997', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Việt Nam', N'Đà Lạt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[NHACUNGCAP] ([IdKhachHang], [IdDatHang], [TenKhachHang], [IdLoaiKhachHang], [IdNhomKhachHang], [DiaChiKhachHang], [NgaySinh], [MaVach], [Tax], [Tel], [Fax], [Email], [Mobile], [Website], [NguoiLienHe], [DiaChi], [NickYM], [NickSky], [KhuVuc], [Quan], [QuocGia], [ThanhPho], [TaiKhoanNganHang], [TenNganHang], [GioiHanNo], [ChietKhau], [IsCongNo], [ChiTietCongNo], [LaNhaCungCap], [MoTa], [Active]) VALUES (N'KH08', 8, N'Đan Trường', N'LHK08', N'NHK08', N'Lầu 6 Tòa Nhà Trung Tâm Q1', N'13/4/2001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Việt Nam', N'Đà Lạt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[NHACUNGCAP] ([IdKhachHang], [IdDatHang], [TenKhachHang], [IdLoaiKhachHang], [IdNhomKhachHang], [DiaChiKhachHang], [NgaySinh], [MaVach], [Tax], [Tel], [Fax], [Email], [Mobile], [Website], [NguoiLienHe], [DiaChi], [NickYM], [NickSky], [KhuVuc], [Quan], [QuocGia], [ThanhPho], [TaiKhoanNganHang], [TenNganHang], [GioiHanNo], [ChietKhau], [IsCongNo], [ChiTietCongNo], [LaNhaCungCap], [MoTa], [Active]) VALUES (N'KH09', 9, N'Khởi Mi', N'LHK09', N'NHK09', N'số 2 Hồng Bàng', N'9/7/1997', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Việt Nam', N'Hồ Chí Minh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[NHACUNGCAP] ([IdKhachHang], [IdDatHang], [TenKhachHang], [IdLoaiKhachHang], [IdNhomKhachHang], [DiaChiKhachHang], [NgaySinh], [MaVach], [Tax], [Tel], [Fax], [Email], [Mobile], [Website], [NguoiLienHe], [DiaChi], [NickYM], [NickSky], [KhuVuc], [Quan], [QuocGia], [ThanhPho], [TaiKhoanNganHang], [TenNganHang], [GioiHanNo], [ChietKhau], [IsCongNo], [ChiTietCongNo], [LaNhaCungCap], [MoTa], [Active]) VALUES (N'KH10', 10, N'Nam Cường', N'LHK10', N'NHK10', NULL, N'2/6/1996', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Việt Nam', N'Lâm Hà', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[NHOM_BAOCAO] ([IdNhom_BaoCao], [TenNhomBaoCao], [MoTa], [Active]) VALUES (N'NBC01', N'TỒN KHO TỔNG HỢP', NULL, NULL)
INSERT [dbo].[NHOM_BAOCAO] ([IdNhom_BaoCao], [TenNhomBaoCao], [MoTa], [Active]) VALUES (N'NBC02', N'NHẬP XUẤT TỒN', NULL, NULL)
INSERT [dbo].[NHOM_BAOCAO] ([IdNhom_BaoCao], [TenNhomBaoCao], [MoTa], [Active]) VALUES (N'NBC03', N'THẺ KHO', NULL, NULL)
INSERT [dbo].[NHOM_BAOCAO] ([IdNhom_BaoCao], [TenNhomBaoCao], [MoTa], [Active]) VALUES (N'NBC04', N'SỔ CHI TIẾT HÀNG HÓA', NULL, NULL)
INSERT [dbo].[NHOM_BAOCAO] ([IdNhom_BaoCao], [TenNhomBaoCao], [MoTa], [Active]) VALUES (N'NBC05', N'LỊCH SỬ HÀNG HÓA', NULL, NULL)
INSERT [dbo].[REFTYPE] ([ID], [Ten], [NameEN], [IdDanhMuc], [SapXep], [IsSearch]) VALUES (1, NULL, NULL, NULL, NULL, NULL)

--============
INSERT [dbo].[donvi] ([DonVi_ID], [TenDonVi], [MoTa], [KichHoat]) VALUES (N'1', N'Cái', N'cái', 1)
INSERT [dbo].[donvi] ([DonVi_ID], [TenDonVi], [MoTa], [KichHoat]) VALUES (N'10', N'Lon', N'Lon', 1)
INSERT [dbo].[donvi] ([DonVi_ID], [TenDonVi], [MoTa], [KichHoat]) VALUES (N'2', N'Bộ', N'Bộ', 1)
INSERT [dbo].[donvi] ([DonVi_ID], [TenDonVi], [MoTa], [KichHoat]) VALUES (N'3', N'Túi', N'Túi', 1)
INSERT [dbo].[donvi] ([DonVi_ID], [TenDonVi], [MoTa], [KichHoat]) VALUES (N'4', N'Bình', N'Bình', 1)
INSERT [dbo].[donvi] ([DonVi_ID], [TenDonVi], [MoTa], [KichHoat]) VALUES (N'5', N'Đôi', N'Đôi', 1)
INSERT [dbo].[donvi] ([DonVi_ID], [TenDonVi], [MoTa], [KichHoat]) VALUES (N'6', N'Cặp', N'Cặp', 1)
INSERT [dbo].[donvi] ([DonVi_ID], [TenDonVi], [MoTa], [KichHoat]) VALUES (N'7', N'Gói', N'Gói', 1)
INSERT [dbo].[donvi] ([DonVi_ID], [TenDonVi], [MoTa], [KichHoat]) VALUES (N'8', N'Chai', N'Chai', 1)
INSERT [dbo].[donvi] ([DonVi_ID], [TenDonVi], [MoTa], [KichHoat]) VALUES (N'9', N'Hộp', N'Hộp', 1)
INSERT [dbo].[SYS_CONGTY] ([CongTy_Id], [TenCongTy], [DiaChi], [DT], [Fax], [WebSite], [Email], [Tax], [Licence], [Photo]) VALUES (N'Q', N'Xuan Thai', N'quan 12, tp hcm', N'1234567890', N'1234567890', NULL, N'noname@yahoo.com', NULL, NULL, NULL)
INSERT [dbo].[SYS_NGUOIDUNG] ([UserID], [UserName], [Password], [Nhom_ID], [MoTa], [PartID], [KichHoat]) VALUES (N'1', N'admin', N'123', N'1', N'amin', NULL, 1)
INSERT [dbo].[SYS_NGUOIDUNG] ([UserID], [UserName], [Password], [Nhom_ID], [MoTa], [PartID], [KichHoat]) VALUES (N'10', N'user9', N'123', N'2', NULL, NULL, 0)
INSERT [dbo].[SYS_NGUOIDUNG] ([UserID], [UserName], [Password], [Nhom_ID], [MoTa], [PartID], [KichHoat]) VALUES (N'2', N'user1', N'123', N'2', NULL, NULL, 0)
INSERT [dbo].[SYS_NGUOIDUNG] ([UserID], [UserName], [Password], [Nhom_ID], [MoTa], [PartID], [KichHoat]) VALUES (N'3', N'user2', N'123', N'3', NULL, NULL, 0)
INSERT [dbo].[SYS_NGUOIDUNG] ([UserID], [UserName], [Password], [Nhom_ID], [MoTa], [PartID], [KichHoat]) VALUES (N'4', N'user3', N'123', N'4', NULL, NULL, 0)
INSERT [dbo].[SYS_NGUOIDUNG] ([UserID], [UserName], [Password], [Nhom_ID], [MoTa], [PartID], [KichHoat]) VALUES (N'5', N'user4', N'123', N'5', NULL, NULL, 0)
INSERT [dbo].[SYS_NGUOIDUNG] ([UserID], [UserName], [Password], [Nhom_ID], [MoTa], [PartID], [KichHoat]) VALUES (N'6', N'user5', N'123', N'4', NULL, NULL, 0)
INSERT [dbo].[SYS_NGUOIDUNG] ([UserID], [UserName], [Password], [Nhom_ID], [MoTa], [PartID], [KichHoat]) VALUES (N'7', N'user6', N'123', N'3', NULL, NULL, 0)
INSERT [dbo].[SYS_NGUOIDUNG] ([UserID], [UserName], [Password], [Nhom_ID], [MoTa], [PartID], [KichHoat]) VALUES (N'8', N'user7', N'123', N'2', NULL, NULL, 0)
INSERT [dbo].[SYS_NGUOIDUNG] ([UserID], [UserName], [Password], [Nhom_ID], [MoTa], [PartID], [KichHoat]) VALUES (N'9', N'user8', N'123', N'2', NULL, NULL, 0)
INSERT [dbo].[SYS_NHOM] ([Nhom_ID], [TenNhom], [MoTa], [KichHoat]) VALUES (N'1', N'Admin', N'Quan Tri He Thong', 1)
INSERT [dbo].[SYS_NHOM] ([Nhom_ID], [TenNhom], [MoTa], [KichHoat]) VALUES (N'2', N'Ke Toan', N'Quan ly ngan sach', 1)
INSERT [dbo].[SYS_NHOM] ([Nhom_ID], [TenNhom], [MoTa], [KichHoat]) VALUES (N'3', N'Ban Hang', N'Nguoi Ban hang', 1)
INSERT [dbo].[SYS_NHOM] ([Nhom_ID], [TenNhom], [MoTa], [KichHoat]) VALUES (N'4', N'Nhan Su', N'Quan Ly Nhan Su', 1)
INSERT [dbo].[SYS_NHOM] ([Nhom_ID], [TenNhom], [MoTa], [KichHoat]) VALUES (N'5', N'Giam Doc', N'Giam Doc', 1)
INSERT [dbo].[SYS_QUYTAC] ([QuyTac_ID], [TenQuyTac], [MoTa], [KichHoat]) VALUES (N'1', N'Them', NULL, 1)
INSERT [dbo].[SYS_QUYTAC] ([QuyTac_ID], [TenQuyTac], [MoTa], [KichHoat]) VALUES (N'10', N'Bao Cao', NULL, 1)
INSERT [dbo].[SYS_QUYTAC] ([QuyTac_ID], [TenQuyTac], [MoTa], [KichHoat]) VALUES (N'2', N'Xoa', NULL, 1)
INSERT [dbo].[SYS_QUYTAC] ([QuyTac_ID], [TenQuyTac], [MoTa], [KichHoat]) VALUES (N'3', N'Sua', NULL, 1)
INSERT [dbo].[SYS_QUYTAC] ([QuyTac_ID], [TenQuyTac], [MoTa], [KichHoat]) VALUES (N'4', N'Gui', NULL, 1)
INSERT [dbo].[SYS_QUYTAC] ([QuyTac_ID], [TenQuyTac], [MoTa], [KichHoat]) VALUES (N'5', N'Xuat', NULL, 1)
INSERT [dbo].[SYS_QUYTAC] ([QuyTac_ID], [TenQuyTac], [MoTa], [KichHoat]) VALUES (N'6', N'Nhap', NULL, 1)
INSERT [dbo].[SYS_QUYTAC] ([QuyTac_ID], [TenQuyTac], [MoTa], [KichHoat]) VALUES (N'7', N'In', NULL, 1)
INSERT [dbo].[SYS_QUYTAC] ([QuyTac_ID], [TenQuyTac], [MoTa], [KichHoat]) VALUES (N'8', N'All', NULL, 1)
INSERT [dbo].[SYS_QUYTAC] ([QuyTac_ID], [TenQuyTac], [MoTa], [KichHoat]) VALUES (N'9', N'Xem', NULL, 1)
ALTER TABLE [dbo].[FORM] ADD  CONSTRAINT [DF_FORM_Width]  DEFAULT ((0)) FOR [Width]
GO
ALTER TABLE [dbo].[FORM] ADD  CONSTRAINT [DF_FORM_Height]  DEFAULT ((0)) FOR [Height]
GO
ALTER TABLE [dbo].[FORM] ADD  CONSTRAINT [DF_FORM_Active]  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC] ADD  CONSTRAINT [DF_SYS_USER_RULE_AllowAdd]  DEFAULT ((1)) FOR [Them_All]
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC] ADD  CONSTRAINT [DF_SYS_USER_RULE_AllowDelete]  DEFAULT ((1)) FOR [Xoa_All]
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC] ADD  CONSTRAINT [DF_SYS_USER_RULE_AllowEdit]  DEFAULT ((1)) FOR [Sua_All]
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC] ADD  CONSTRAINT [DF_SYS_USER_RULE_AllowAccess]  DEFAULT ((1)) FOR [TruyCap_All]
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC] ADD  CONSTRAINT [DF_SYS_USER_RULE_AllowPrint]  DEFAULT ((1)) FOR [In_All]
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC] ADD  CONSTRAINT [DF_SYS_USER_RULE_AllowExport]  DEFAULT ((1)) FOR [Nhap_All]
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC] ADD  CONSTRAINT [DF_SYS_USER_RULE_AllowImport]  DEFAULT ((1)) FOR [Xuat_All]
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC] ADD  CONSTRAINT [DF_SYS_USER_RULE_Active]  DEFAULT ((1)) FOR [KichHoat]
GO
ALTER TABLE [dbo].[CHITIET_CHUYENKHO]  WITH CHECK ADD  CONSTRAINT [FK_CHITIET_CHUYENKHO_CHUYENKHO] FOREIGN KEY([ChuyenKho_ID])
REFERENCES [dbo].[CHUYENKHO] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CHITIET_CHUYENKHO] CHECK CONSTRAINT [FK_CHITIET_CHUYENKHO_CHUYENKHO]
GO
ALTER TABLE [dbo].[CHITIET_CHUYENKHO]  WITH CHECK ADD  CONSTRAINT [FK_CHITIET_CHUYENKHO_PRODUCT] FOREIGN KEY([SanPham_ID])
REFERENCES [dbo].[SANPHAM] ([SanPham_ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CHITIET_CHUYENKHO] CHECK CONSTRAINT [FK_CHITIET_CHUYENKHO_PRODUCT]
GO
ALTER TABLE [dbo].[CHITIET_NHAPKHO]  WITH CHECK ADD  CONSTRAINT [FK_CHITIET_NHAPKHO_NHAPKHO] FOREIGN KEY([NhapKho_ID])
REFERENCES [dbo].[NHAPKHO] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CHITIET_NHAPKHO] CHECK CONSTRAINT [FK_CHITIET_NHAPKHO_NHAPKHO]
GO
ALTER TABLE [dbo].[CHITIET_NHAPKHO]  WITH CHECK ADD  CONSTRAINT [FK_CHITIET_NHAPKHO_SANPHAM] FOREIGN KEY([SanPham_ID])
REFERENCES [dbo].[SANPHAM] ([SanPham_ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CHITIET_NHAPKHO] CHECK CONSTRAINT [FK_CHITIET_NHAPKHO_SANPHAM]
GO
ALTER TABLE [dbo].[CHITIET_XUATKHO]  WITH CHECK ADD  CONSTRAINT [FK_CHITIET_XUATKHO_SANPHAM] FOREIGN KEY([SanPham_ID])
REFERENCES [dbo].[SANPHAM] ([SanPham_ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CHITIET_XUATKHO] CHECK CONSTRAINT [FK_CHITIET_XUATKHO_SANPHAM]
GO
ALTER TABLE [dbo].[CHITIET_XUATKHO]  WITH CHECK ADD  CONSTRAINT [FK_CHITIET_XUATKHO_XUAT_KHO] FOREIGN KEY([XuatKho_ID])
REFERENCES [dbo].[XUAT_KHO] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CHITIET_XUATKHO] CHECK CONSTRAINT [FK_CHITIET_XUATKHO_XUAT_KHO]
GO
ALTER TABLE [dbo].[CHITIETTHANHTOANNHACUNGCAP]  WITH CHECK ADD  CONSTRAINT [FK_PROVIDER_PAYMENT_DETAIL_PROVIDER_PAYMENT] FOREIGN KEY([IdPhieuThanhToan])
REFERENCES [dbo].[THANHTOAN_NHACUNGCAP] ([ID])
GO
ALTER TABLE [dbo].[CHITIETTHANHTOANNHACUNGCAP] CHECK CONSTRAINT [FK_PROVIDER_PAYMENT_DETAIL_PROVIDER_PAYMENT]
GO
ALTER TABLE [dbo].[DONVI_HANGHOA]  WITH CHECK ADD  CONSTRAINT [fk_donvi_hanghoa] FOREIGN KEY([IdDonVi])
REFERENCES [dbo].[donvi] ([DonVi_ID])
GO
ALTER TABLE [dbo].[DONVI_HANGHOA] CHECK CONSTRAINT [fk_donvi_hanghoa]
GO
ALTER TABLE [dbo].[HANGTONKHO_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_INVENTORY_DETAIL_INVENTORY] FOREIGN KEY([CuaHangID])
REFERENCES [dbo].[HANGTONKHO] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[HANGTONKHO_DETAIL] CHECK CONSTRAINT [FK_INVENTORY_DETAIL_INVENTORY]
GO
ALTER TABLE [dbo].[HANGTONKHO_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_INVENTORY_DETAIL_PRODUCT] FOREIGN KEY([SanPham_ID])
REFERENCES [dbo].[SANPHAM] ([SanPham_ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[HANGTONKHO_DETAIL] CHECK CONSTRAINT [FK_INVENTORY_DETAIL_PRODUCT]
GO
ALTER TABLE [dbo].[HANGTONKHO_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_INVENTORY_DETAIL_STOCK] FOREIGN KEY([TonKHo_ID])
REFERENCES [dbo].[HANGTON] ([IdHangTon])
GO
ALTER TABLE [dbo].[HANGTONKHO_DETAIL] CHECK CONSTRAINT [FK_INVENTORY_DETAIL_STOCK]
GO
ALTER TABLE [dbo].[PHIEUCHITIEN]  WITH CHECK ADD  CONSTRAINT [FK_PHIEUCHITIEN_KHACHHANG] FOREIGN KEY([ID_KhachHang])
REFERENCES [dbo].[KHACHHANG] ([Id_KhachHang])
GO
ALTER TABLE [dbo].[PHIEUCHITIEN] CHECK CONSTRAINT [FK_PHIEUCHITIEN_KHACHHANG]
GO
ALTER TABLE [dbo].[PHIEUCHITIEN]  WITH CHECK ADD  CONSTRAINT [FK_PHIEUCHITIEN_TIENTE] FOREIGN KEY([ID_TienTe])
REFERENCES [dbo].[TIENTE] ([ID_TienTe])
GO
ALTER TABLE [dbo].[PHIEUCHITIEN] CHECK CONSTRAINT [FK_PHIEUCHITIEN_TIENTE]
GO
ALTER TABLE [dbo].[PHIEUTHUTIEN]  WITH CHECK ADD  CONSTRAINT [FK_PHIEUTHUTIEN_KHACHHANG] FOREIGN KEY([ID_KhachHang])
REFERENCES [dbo].[KHACHHANG] ([Id_KhachHang])
GO
ALTER TABLE [dbo].[PHIEUTHUTIEN] CHECK CONSTRAINT [FK_PHIEUTHUTIEN_KHACHHANG]
GO
ALTER TABLE [dbo].[PHIEUTHUTIEN]  WITH CHECK ADD  CONSTRAINT [FK_PHIEUTHUTIEN_TIENTE] FOREIGN KEY([ID_TienTe])
REFERENCES [dbo].[TIENTE] ([ID_TienTe])
GO
ALTER TABLE [dbo].[PHIEUTHUTIEN] CHECK CONSTRAINT [FK_PHIEUTHUTIEN_TIENTE]
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG]  WITH CHECK ADD  CONSTRAINT [FK_SYS_NGUOIDUNG_SYS_NHOM] FOREIGN KEY([Nhom_ID])
REFERENCES [dbo].[SYS_NHOM] ([Nhom_ID])
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG] CHECK CONSTRAINT [FK_SYS_NGUOIDUNG_SYS_NHOM]
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC]  WITH CHECK ADD  CONSTRAINT [FK_SYS_NGUOIDUNG_QUYTAC_SYS_QUYTAC] FOREIGN KEY([QuyTac_ID])
REFERENCES [dbo].[SYS_QUYTAC] ([QuyTac_ID])
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC] CHECK CONSTRAINT [FK_SYS_NGUOIDUNG_QUYTAC_SYS_QUYTAC]
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC]  WITH CHECK ADD  CONSTRAINT [SYS_NGUOIDUNG_QUYTAC_SYS_NHOM] FOREIGN KEY([Nhom_ID])
REFERENCES [dbo].[SYS_NHOM] ([Nhom_ID])
GO
ALTER TABLE [dbo].[SYS_NGUOIDUNG_QUYTAC] CHECK CONSTRAINT [SYS_NGUOIDUNG_QUYTAC_SYS_NHOM]
GO
ALTER TABLE [dbo].[SYS_NHOM_DOITUONG]  WITH CHECK ADD  CONSTRAINT [FK_SYS_NHOM_DOITUONG_SYS_NHOM] FOREIGN KEY([Nhom_ID])
REFERENCES [dbo].[SYS_NHOM] ([Nhom_ID])
GO
ALTER TABLE [dbo].[SYS_NHOM_DOITUONG] CHECK CONSTRAINT [FK_SYS_NHOM_DOITUONG_SYS_NHOM]
GO
