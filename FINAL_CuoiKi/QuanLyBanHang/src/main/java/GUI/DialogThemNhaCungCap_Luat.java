package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import controller.NhaCungCap_Controller_Luat;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class DialogThemNhaCungCap_Luat extends JDialog {

	private final JPanel contentPanel = new JPanel();
	public static JTextField txtNguoiLienHe;
	public static JTextField txtChietKhau;
	public static JTextField txtGioiHanNo;
	public static JTextField txtTaiKhoan;
	public static JTextField txtEmail;
	public static JTextField txtDienThoai;
	public static JTextField txtMaSoThue;
	public static JTextField txtDiaChi;
	public static JTextField txtTen;
	public static JTextField txtMa;
	public static JTextField txtNoHienTai;
	public static JTextField txtChucVu;
	public static JTextField txtNganHang;
	public static JTextField txtWebsite;
	public static JTextField txtMobile;
	public static JTextField txtFax;
	public static JComboBox cbbKhuVuc;
	private JButton okButton;
	private JButton cancelButton;
	
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DialogThemNhaCungCap_Luat dialog = new DialogThemNhaCungCap_Luat();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public DialogThemNhaCungCap_Luat() {
		setTitle("Thêm Nhà Cung Cấp");
		setBounds(100, 100, 525, 378);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBackground(Color.LIGHT_GRAY);
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPanel.add(panel, BorderLayout.NORTH);
		
		JLabel lblThngTinChung = new JLabel("Thông tin Chung");
		panel.add(lblThngTinChung);
		
		JPanel panel_1 = new JPanel();
		contentPanel.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);
		
		JLabel lblM = new JLabel("Mã (*)");
		lblM.setBounds(10, 11, 46, 14);
		panel_1.add(lblM);
		
		JLabel lblTn = new JLabel("Tên (*)");
		lblTn.setBounds(10, 36, 46, 14);
		panel_1.add(lblTn);
		
		JLabel lblaCh = new JLabel("Địa chỉ");
		lblaCh.setBounds(10, 61, 46, 14);
		panel_1.add(lblaCh);
		
		JLabel lblMSThu = new JLabel("Mã số thuế");
		lblMSThu.setBounds(10, 86, 59, 14);
		panel_1.add(lblMSThu);
		
		JLabel lblinThoi = new JLabel("Điện thoại");
		lblinThoi.setBounds(10, 111, 59, 14);
		panel_1.add(lblinThoi);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 136, 46, 14);
		panel_1.add(lblEmail);
		
		JLabel lblTiKhon = new JLabel("Tài Khoản");
		lblTiKhon.setBounds(10, 161, 59, 14);
		panel_1.add(lblTiKhon);
		
		JLabel lblGiiHnN = new JLabel("Giới Hạn Nợ");
		lblGiiHnN.setBounds(10, 186, 59, 14);
		panel_1.add(lblGiiHnN);
		
		JLabel lblChitKhu = new JLabel("Chiết Khấu");
		lblChitKhu.setBounds(10, 211, 59, 14);
		panel_1.add(lblChitKhu);
		
		JLabel lblNgiLinH = new JLabel("Người Liên hệ");
		lblNgiLinH.setBounds(10, 236, 65, 14);
		panel_1.add(lblNgiLinH);
		
		txtNguoiLienHe = new JTextField();
		txtNguoiLienHe.setBounds(85, 233, 146, 20);
		panel_1.add(txtNguoiLienHe);
		txtNguoiLienHe.setColumns(10);
		
		txtChietKhau = new JTextField();
		txtChietKhau.setBounds(85, 208, 146, 20);
		panel_1.add(txtChietKhau);
		txtChietKhau.setColumns(10);
		
		txtGioiHanNo = new JTextField();
		txtGioiHanNo.setBounds(85, 183, 146, 20);
		panel_1.add(txtGioiHanNo);
		txtGioiHanNo.setColumns(10);
		
		txtTaiKhoan = new JTextField();
		txtTaiKhoan.setBounds(85, 158, 146, 20);
		panel_1.add(txtTaiKhoan);
		txtTaiKhoan.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(85, 133, 146, 20);
		panel_1.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtDienThoai = new JTextField();
		txtDienThoai.setBounds(85, 108, 146, 20);
		panel_1.add(txtDienThoai);
		txtDienThoai.setColumns(10);
		
		txtMaSoThue = new JTextField();
		txtMaSoThue.setBounds(85, 83, 146, 20);
		panel_1.add(txtMaSoThue);
		txtMaSoThue.setColumns(10);
		
		txtDiaChi = new JTextField();
		txtDiaChi.setBounds(85, 58, 412, 20);
		panel_1.add(txtDiaChi);
		txtDiaChi.setColumns(10);
		
		txtTen = new JTextField();
		txtTen.setBounds(85, 33, 412, 20);
		panel_1.add(txtTen);
		txtTen.setColumns(10);
		
		txtMa = new JTextField();
		txtMa.setBounds(85, 8, 146, 20);
		panel_1.add(txtMa);
		txtMa.setColumns(10);
		
		JLabel lblKhuVc = new JLabel("Khu Vực (*)");
		lblKhuVc.setBounds(241, 11, 59, 14);
		panel_1.add(lblKhuVc);
		
		JLabel lblFax = new JLabel("Fax");
		lblFax.setBounds(241, 86, 46, 14);
		panel_1.add(lblFax);
		
		JLabel lblMobile = new JLabel("Mobile");
		lblMobile.setBounds(241, 111, 46, 14);
		panel_1.add(lblMobile);
		
		JLabel lblWebsite = new JLabel("Website");
		lblWebsite.setBounds(241, 136, 46, 14);
		panel_1.add(lblWebsite);
		
		JLabel lblNgnHng = new JLabel("Ngân Hàng");
		lblNgnHng.setBounds(241, 161, 53, 14);
		panel_1.add(lblNgnHng);
		
		JLabel lblNHinTi = new JLabel("Nợ Hiện Tại");
		lblNHinTi.setBounds(241, 186, 59, 14);
		panel_1.add(lblNHinTi);
		
		JLabel lblChcV = new JLabel("Chức vụ");
		lblChcV.setBounds(241, 236, 46, 14);
		panel_1.add(lblChcV);
		
		txtNoHienTai = new JTextField();
		txtNoHienTai.setBounds(310, 183, 187, 20);
		panel_1.add(txtNoHienTai);
		txtNoHienTai.setColumns(10);
		
		txtChucVu = new JTextField();
		txtChucVu.setBounds(310, 233, 187, 20);
		panel_1.add(txtChucVu);
		txtChucVu.setColumns(10);
		
		txtNganHang = new JTextField();
		txtNganHang.setBounds(310, 158, 187, 20);
		panel_1.add(txtNganHang);
		txtNganHang.setColumns(10);
		
		txtWebsite = new JTextField();
		txtWebsite.setBounds(310, 133, 187, 20);
		panel_1.add(txtWebsite);
		txtWebsite.setColumns(10);
		
		txtMobile = new JTextField();
		txtMobile.setBounds(310, 108, 187, 20);
		panel_1.add(txtMobile);
		txtMobile.setColumns(10);
		
		txtFax = new JTextField();
		txtFax.setBounds(310, 83, 187, 20);
		panel_1.add(txtFax);
		txtFax.setColumns(10);
		
		cbbKhuVuc = new JComboBox();
		cbbKhuVuc.setBounds(310, 8, 187, 20);
		
		NhaCungCap_Controller_Luat nccct=new NhaCungCap_Controller_Luat();
		try{
			
			nccct.LoadKhuVuc_Dialog();
		}
		catch(Exception ex)
		{
			
		}
		panel_1.add(cbbKhuVuc);
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			
			JCheckBox chckbxCn = new JCheckBox("Còn quản lý");
			chckbxCn.setSelected(true);
			{
				okButton = new JButton("Lưu");
				okButton.setIcon(new ImageIcon("E:\\Java\\QuanLyBanHang\\HinhAnh\\1475176105_Save.png"));
				okButton.addActionListener(new ActionListener() 
				{
					public void actionPerformed(ActionEvent e) 
					{
						NhaCungCap_Controller_Luat.ThaoTacThem_KhiBamThem();						
					}
				});
				okButton.setActionCommand("OK");
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Đóng");
				cancelButton.setIcon(new ImageIcon("E:\\Java\\QuanLyBanHang\\HinhAnh\\1475176204_close.png"));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
			}
			GroupLayout gl_buttonPane = new GroupLayout(buttonPane);
			gl_buttonPane.setHorizontalGroup(
				gl_buttonPane.createParallelGroup(Alignment.TRAILING)
					.addGroup(Alignment.LEADING, gl_buttonPane.createSequentialGroup()
						.addContainerGap()
						.addComponent(chckbxCn)
						.addPreferredGap(ComponentPlacement.RELATED, 250, Short.MAX_VALUE)
						.addComponent(okButton)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(cancelButton)
						.addContainerGap())
			);
			gl_buttonPane.setVerticalGroup(
				gl_buttonPane.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_buttonPane.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_buttonPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(chckbxCn)
							.addComponent(cancelButton)
							.addComponent(okButton))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
			);
			buttonPane.setLayout(gl_buttonPane);
			
		}
		
	}
}
