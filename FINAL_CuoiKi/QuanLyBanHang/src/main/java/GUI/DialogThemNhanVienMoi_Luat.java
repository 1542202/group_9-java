package GUI;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import controller.NhaCungCap_Controller_Luat;
import controller.NhanVien_Controller_Luat;

import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DialogThemNhanVienMoi_Luat extends JDialog {
	public static JTextField txtMaNV;
	public static JTextField txtTenNV;
	public static JTextField txtDienThoai;
	public static JTextField txtEmail;
	public static JTextField txtDiaChi;
	public static JTextField txtDiDong;
	public static JComboBox cbcBoPhan;	
	public static JComboBox cbcNVQuanLy;
	public static JComboBox cbcChucVu;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DialogThemNhanVienMoi_Luat dialog = new DialogThemNhanVienMoi_Luat();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public DialogThemNhanVienMoi_Luat() {
		setResizable(false);
		setTitle("Thêm Nhân Viên");
		setBounds(100, 100, 542, 378);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 662, Short.MAX_VALUE)
				.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 662, Short.MAX_VALUE)
				.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 662, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE))
		);
		panel_2.setLayout(null);
		
		JButton btnLuu = new JButton("Lưu");
		btnLuu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Save button
				NhanVien_Controller_Luat.ThaoTacThem();
			}
		});
		btnLuu.setBounds(10, 11, 89, 23);
		panel_2.add(btnLuu);
		
		JButton btnDong = new JButton("Đóng");
		btnDong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Close button
				dispose();
			}
		});
		btnDong.setBounds(109, 11, 89, 23);
		panel_2.add(btnDong);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.LIGHT_GRAY);
		FlowLayout flowLayout_1 = (FlowLayout) panel_5.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		panel_1.add(panel_5, BorderLayout.NORTH);
		
		JLabel lblNewLabel_3 = new JLabel("Quản Lý");
		panel_5.add(lblNewLabel_3);
		
		JPanel panel_6 = new JPanel();
		panel_1.add(panel_6, BorderLayout.CENTER);
		panel_6.setLayout(null);
		
		JLabel lblNewLabel_4 = new JLabel("Chức Vụ");
		lblNewLabel_4.setBounds(10, 11, 46, 14);
		panel_6.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Đại Chỉ");
		lblNewLabel_5.setBounds(10, 36, 46, 14);
		panel_6.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Email");
		lblNewLabel_6.setBounds(10, 61, 46, 14);
		panel_6.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Điện Thoại");
		lblNewLabel_7.setBounds(10, 86, 58, 14);
		panel_6.add(lblNewLabel_7);
		
		JLabel lblNewLabel_8 = new JLabel("Bộ Phận");
		lblNewLabel_8.setBounds(10, 111, 46, 14);
		panel_6.add(lblNewLabel_8);
		
		JLabel lblNewLabel_9 = new JLabel("Quản Lý");
		lblNewLabel_9.setBounds(10, 136, 46, 14);
		panel_6.add(lblNewLabel_9);
		
		txtDienThoai = new JTextField();
		txtDienThoai.setBounds(78, 83, 188, 20);
		panel_6.add(txtDienThoai);
		txtDienThoai.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(78, 58, 444, 20);
		panel_6.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtDiaChi = new JTextField();
		txtDiaChi.setBounds(78, 33, 444, 20);
		panel_6.add(txtDiaChi);
		txtDiaChi.setColumns(10);
		
		cbcBoPhan = new JComboBox();
		cbcBoPhan.setBounds(78, 108, 444, 20);
		panel_6.add(cbcBoPhan);
		
		cbcNVQuanLy = new JComboBox();
		cbcNVQuanLy.setBounds(78, 133, 444, 20);
		panel_6.add(cbcNVQuanLy);
		
		JLabel lblDing = new JLabel("Di Động");
		lblDing.setBounds(276, 86, 46, 14);
		panel_6.add(lblDing);
		
		txtDiDong = new JTextField();
		txtDiDong.setBounds(332, 83, 190, 20);
		panel_6.add(txtDiDong);
		txtDiDong.setColumns(10);
		
		cbcChucVu = new JComboBox();
		cbcChucVu.setBounds(78, 8, 444, 20);
		panel_6.add(cbcChucVu);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.LIGHT_GRAY);
		FlowLayout flowLayout = (FlowLayout) panel_3.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel.add(panel_3, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("Cơ Bản");
		panel_3.add(lblNewLabel);
		
		JPanel panel_4 = new JPanel();
		panel.add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Mã");
		lblNewLabel_1.setBounds(10, 11, 46, 14);
		panel_4.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Tên");
		lblNewLabel_2.setBounds(10, 36, 46, 14);
		panel_4.add(lblNewLabel_2);
		
		txtMaNV = new JTextField();
		txtMaNV.setBounds(79, 8, 339, 20);
		panel_4.add(txtMaNV);
		txtMaNV.setColumns(10);
		
		txtTenNV = new JTextField();
		txtTenNV.setBounds(79, 33, 442, 20);
		panel_4.add(txtTenNV);
		txtTenNV.setColumns(10);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Còn Quản Lý");
		chckbxNewCheckBox.setSelected(true);
		chckbxNewCheckBox.setBounds(424, 7, 97, 23);
		panel_4.add(chckbxNewCheckBox);
		getContentPane().setLayout(groupLayout);
		///////// Load Init
		NhanVien_Controller_Luat.Load_Dialog_ThemNhanVienMoi();
	}
}
