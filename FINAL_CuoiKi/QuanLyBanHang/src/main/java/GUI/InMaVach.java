package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Insets;
import java.awt.Point;

import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import com.barcode_coder.java_barcode.*;  

import controller.dmInMaVach_controller;
import java.awt.Window.Type;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PrinterException;
public class InMaVach extends JFrame {
	
		public static Barcode createBarcode(BarcodeType barcodeType, String code) {
		return null;
	}  
	public static Barcode createBarcode(BarcodeType barcodeType, String code, boolean crc) {
		return null;
	} 
	private JPanel contentPane;
	public static JTable table;
	
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InMaVach frame = new InMaVach();
					frame.setVisible(true);
					frame.setTitle("In Ma Vach");
					//frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					dmInMaVach_controller.LoadData();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InMaVach() {
		setTitle("In Mã Vạch");
		
//		Barcode b = BarcodeFactory.createBarcode(BarcodeType.Code128,"12345678");  
//		b.export("png",1,50,true,"/Users/XuanThai/Desktop/image.png");  
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{424, 0};
		gbl_contentPane.rowHeights = new int[]{0, 10, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.anchor = GridBagConstraints.NORTHWEST;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		
		JButton btnXem = new JButton("Xem");
		btnXem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dmInMaVach_controller.LoadData();
			}
		});
		btnXem.setIcon(new ImageIcon("HinhAnh\\view.png"));
		panel.add(btnXem);
		
		JButton btnXuat = new JButton("Xuất");
		btnXuat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
//				dmInMaVach_controller.thaydoisolanin();
//				dmInMaVach_controller.LoadData();
				dmInMaVach_controller.saveimgtoexcel(null);
			}
		});
		btnXuat.setIcon(new ImageIcon("HinhAnh\\forward.png"));
		panel.add(btnXuat);
		
		JButton btnIn = new JButton("In");
		btnIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					dmInMaVach_controller.infile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (PrinterException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
//				XWPFDocument doc = new XWPFDocument();
//		        XWPFParagraph p = doc.createParagraph();
//		        XWPFRun xwpfRun = p.createRun();
//		        String[] IMageargs={"HinhAnh\\view.png"
//		                
//		        };
//		        for (String imgFile : IMageargs) {
//		            int format=XWPFDocument.PICTURE_TYPE_JPEG;
//		            xwpfRun.setText(imgFile);
//		            xwpfRun.addBreak();
//		            try {
//						xwpfRun.addPicture (new FileInputStream(imgFile), format, imgFile, Units.toEMU(200), Units.toEMU(200));
//					 FileOutputStream out = new FileOutputStream("C:\\Users\\XuanThai\\Desktop\\test.xls");
//		        doc.write(out);
//		        out.close();
//		            } catch (InvalidFormatException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (FileNotFoundException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} // 200x200 pixels
//		            //xwpfRun.addBreak(BreakType.PAGE);
//		        }
		       
				
			}
		});
		btnIn.setIcon(new ImageIcon("HinhAnh\\print.png"));
		panel.add(btnIn);
		
		JButton btnSuaSoLan = new JButton("Sua so lan in");
		btnSuaSoLan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InMaVach_solanin h = new InMaVach_solanin();
				h.setVisible(true);
			}
		});
		panel.add(btnSuaSoLan);
		
		JButton btnDong = new JButton("Đóng");
		btnDong.setIcon(new ImageIcon("HinhAnh\\close.png"));
		panel.add(btnDong);
		
		table = new JTable();
		
//		table.setModel(new DefaultTableModel(
//			new Object[][] {
//				{"1", "Ma001", "Tem t\u1EEB gi\u1EA5y  4x4", "100000", "ANS0001", "cai"},
//				{"1", "Ma002", "Tem t\u1EEB gi\u1EA5y 3x3", "100000", "ANS0002", "cai"},
//				{"1", "Ma003", "D\u00E2y \u0111eo kim lo\u1EA1i BB L002", "100000", "ANS0003", "cai"},
//				{"1", "Ma004", "D\u00E2y \u0111eo nh\u1EF1a BB L003/G", "100000", "ANS0004", "cai"},
//				{"1", "Ma005", "M\u1EDF kh\u00F3a DO-0004", "100000", "ANS0005", null},
//			},
//			new String[] {
//				"S\u1ED1 L\u1EA7n In", "M\u00E3 H\u00E0ng", "T\u00EAn Ti\u1EBFng Vi\u1EC7t", "Gi\u00E1 B\u00E1n", "M\u00E3 V\u1EA1ch", "\u0110\u01A1n V\u1ECB"
//			}
//		));
		//table.getColumnModel().getColumn(2).setPreferredWidth(95);
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridwidth = 2;
		gbc_table.gridheight = 2;
		gbc_table.gridx = 0;
		gbc_table.gridy = 1;
		JScrollPane scrollPane = new JScrollPane(table);
		contentPane.add(scrollPane, gbc_table);
	}
	

}
