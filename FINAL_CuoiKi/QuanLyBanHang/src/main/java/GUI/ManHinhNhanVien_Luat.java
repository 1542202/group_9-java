package GUI;

import java.awt.EventQueue;


import javax.swing.JInternalFrame;

import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JTable;

import controller.NhanVien_Controller_Luat;

import java.awt.GridBagConstraints;
import javax.swing.JScrollPane;
import javax.swing.AbstractButton;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ManHinhNhanVien_Luat extends JInternalFrame 
{
	public static JTable table;
	public static JButton btnSuaChua;
	public static JButton btnXoa;
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					ManHinhNhanVien_Luat frame = new ManHinhNhanVien_Luat();
					frame.setVisible(true);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ManHinhNhanVien_Luat() {
		setClosable(true);
		setMaximizable(true);
		setTitle("Nhân Viên");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_2.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 0;
		panel.add(panel_2, gbc_panel_2);
		
		JButton btnThem = new JButton("Thêm");
		btnThem.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				//Thêm Nhân Viên Mới
				DialogThemNhanVienMoi_Luat tnvm=new DialogThemNhanVienMoi_Luat();
				tnvm.show();
			}
		});		
		
		btnThem.setIcon(new ImageIcon("E:\\Java\\QuanLyBanHang\\HinhAnh\\addIcon.png"));
		btnThem.setVerticalTextPosition(AbstractButton.BOTTOM);
		btnThem.setHorizontalTextPosition(AbstractButton.CENTER);
		panel_2.add(btnThem);
		
		btnSuaChua = new JButton("Sửa Chữa");
		btnSuaChua.setEnabled(false);
		btnSuaChua.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				DialogSuaThongTinNhanVien_Luat snv=new DialogSuaThongTinNhanVien_Luat();
				snv.show();				
			}
		});
		btnSuaChua.setIcon(new ImageIcon("E:\\Java\\QuanLyBanHang\\HinhAnh\\Repair.png"));
		btnSuaChua.setVerticalTextPosition(AbstractButton.BOTTOM);
		btnSuaChua.setHorizontalTextPosition(AbstractButton.CENTER);
		panel_2.add(btnSuaChua);
		
		btnXoa = new JButton("Xóa");
		btnXoa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				NhanVien_Controller_Luat.ThaoTacXoa();
			}
		});
		btnXoa.setEnabled(false);
		btnXoa.setIcon(new ImageIcon("E:\\Java\\QuanLyBanHang\\HinhAnh\\1475175899_delete-file.png"));
		btnXoa.setVerticalTextPosition(AbstractButton.BOTTOM);
		btnXoa.setHorizontalTextPosition(AbstractButton.CENTER);
		panel_2.add(btnXoa);
		
		JButton btnNapLai = new JButton("Nạp Lại");
		btnNapLai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNapLai.setEnabled(false);
		btnNapLai.setIcon(new ImageIcon("E:\\Java\\QuanLyBanHang\\HinhAnh\\1475175986_free-38.png"));
		btnNapLai.setVerticalTextPosition(AbstractButton.BOTTOM);
		btnNapLai.setHorizontalTextPosition(AbstractButton.CENTER);
		panel_2.add(btnNapLai);
		
		JButton btnXuat = new JButton("Xuất");
		btnXuat.setEnabled(false);
		btnXuat.setIcon(new ImageIcon("E:\\Java\\QuanLyBanHang\\HinhAnh\\go-into-icon.png"));
		btnXuat.setVerticalTextPosition(AbstractButton.BOTTOM);
		btnXuat.setHorizontalTextPosition(AbstractButton.CENTER);
		panel_2.add(btnXuat);
		
		JButton btnDong = new JButton("Đóng");
		btnDong.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();
			}
		});
		btnDong.setIcon(new ImageIcon("E:\\Java\\QuanLyBanHang\\HinhAnh\\1475176170_Close_Box_Red.png"));
		btnDong.setVerticalTextPosition(AbstractButton.BOTTOM);
		btnDong.setHorizontalTextPosition(AbstractButton.CENTER);
		panel_2.add(btnDong);
		
		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_3.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 0;
		gbc_panel_3.gridy = 1;
		panel.add(panel_3, gbc_panel_3);
		
		JPanel panel_1 = new JPanel();
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				btnSuaChua.setEnabled(true);
				btnXoa.setEnabled(true);
			}
		});
		scrollPane.setViewportView(table);
		
		/*
		 * Khởi chạy --> Load thông tin nhân viên ra table
		 */
		NhanVien_Controller_Luat.LoadtableNhanVien();
		
	}
	
	
}
