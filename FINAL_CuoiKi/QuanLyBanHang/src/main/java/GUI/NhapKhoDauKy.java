package GUI;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.GridBagConstraints;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class NhapKhoDauKy extends JInternalFrame {
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NhapKhoDauKy frame = new NhapKhoDauKy();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NhapKhoDauKy() {
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setMaximizable(true);
		setClosable(true);
		setTitle("Nhập Kho Đầu Kỳ");
		setBounds(100, 100, 523, 299);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.anchor = GridBagConstraints.WEST;
		gbc_panel.fill = GridBagConstraints.VERTICAL;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		getContentPane().add(panel, gbc_panel);
		
		JButton btnNewButton = new JButton("Tạo Mới");
		btnNewButton.setIcon(new ImageIcon("HinhAnh\\addIcon.png"));
		panel.add(btnNewButton);
		
		JButton btnLu = new JButton("Lưu");
		btnLu.setIcon(new ImageIcon("HinhAnh\\save.png"));
		panel.add(btnLu);
		
		JButton btnNhp = new JButton("Nhập");
		btnNhp.setIcon(new ImageIcon("HinhAnh\\import.png"));
		panel.add(btnNhp);
		
		JButton btnng = new JButton("Đóng");
		btnng.setIcon(new ImageIcon("HinhAnh\\close.png"));
		panel.add(btnng);
		
		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 5);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		getContentPane().add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{72, 56, 86, 46, 86, 0};
		gbl_panel_1.rowHeights = new int[]{20, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblNgiNhp = new JLabel("Người Nhập");
		GridBagConstraints gbc_lblNgiNhp = new GridBagConstraints();
		gbc_lblNgiNhp.anchor = GridBagConstraints.WEST;
		gbc_lblNgiNhp.insets = new Insets(0, 0, 5, 5);
		gbc_lblNgiNhp.gridx = 0;
		gbc_lblNgiNhp.gridy = 0;
		panel_1.add(lblNgiNhp, gbc_lblNgiNhp);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridwidth = 2;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 0;
		panel_1.add(textField, gbc_textField);
		textField.setColumns(10);
		
		JLabel lblKho = new JLabel("Kho Nhập");
		GridBagConstraints gbc_lblKho = new GridBagConstraints();
		gbc_lblKho.anchor = GridBagConstraints.WEST;
		gbc_lblKho.insets = new Insets(0, 0, 5, 5);
		gbc_lblKho.gridx = 3;
		gbc_lblKho.gridy = 0;
		panel_1.add(lblKho, gbc_lblKho);
		
		textField_1 = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.insets = new Insets(0, 0, 5, 0);
		gbc_textField_1.gridx = 4;
		gbc_textField_1.gridy = 0;
		panel_1.add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		JLabel lblGhiCh = new JLabel("Ghi Chú");
		GridBagConstraints gbc_lblGhiCh = new GridBagConstraints();
		gbc_lblGhiCh.anchor = GridBagConstraints.WEST;
		gbc_lblGhiCh.insets = new Insets(0, 0, 0, 5);
		gbc_lblGhiCh.gridx = 0;
		gbc_lblGhiCh.gridy = 1;
		panel_1.add(lblGhiCh, gbc_lblGhiCh);
		
		textField_2 = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.gridwidth = 4;
		gbc_textField_2.insets = new Insets(0, 0, 0, 5);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 1;
		gbc_textField_2.gridy = 1;
		panel_1.add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 1;
		gbc_panel_2.gridy = 1;
		getContentPane().add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{26, 25, 0};
		gbl_panel_2.rowHeights = new int[]{14, 0, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		JLabel lblPhiu = new JLabel("Phiếu");
		GridBagConstraints gbc_lblPhiu = new GridBagConstraints();
		gbc_lblPhiu.anchor = GridBagConstraints.WEST;
		gbc_lblPhiu.insets = new Insets(0, 0, 5, 5);
		gbc_lblPhiu.gridx = 0;
		gbc_lblPhiu.gridy = 0;
		panel_2.add(lblPhiu, gbc_lblPhiu);
		
		textField_3 = new JTextField();
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.insets = new Insets(0, 0, 5, 0);
		gbc_textField_3.fill = GridBagConstraints.BOTH;
		gbc_textField_3.gridx = 1;
		gbc_textField_3.gridy = 0;
		panel_2.add(textField_3, gbc_textField_3);
		textField_3.setColumns(10);
		
		JLabel lblNgy = new JLabel("Ngày");
		GridBagConstraints gbc_lblNgy = new GridBagConstraints();
		gbc_lblNgy.anchor = GridBagConstraints.WEST;
		gbc_lblNgy.insets = new Insets(0, 0, 0, 5);
		gbc_lblNgy.gridx = 0;
		gbc_lblNgy.gridy = 1;
		panel_2.add(lblNgy, gbc_lblNgy);
		
		textField_4 = new JTextField();
		GridBagConstraints gbc_textField_4 = new GridBagConstraints();
		gbc_textField_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_4.gridx = 1;
		gbc_textField_4.gridy = 1;
		panel_2.add(textField_4, gbc_textField_4);
		textField_4.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.gridwidth = 2;
		gbc_panel_3.insets = new Insets(0, 0, 0, 5);
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 0;
		gbc_panel_3.gridy = 2;
		getContentPane().add(panel_3, gbc_panel_3);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[]{450, 0};
		gbl_panel_3.rowHeights = new int[]{96, 0};
		gbl_panel_3.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_3.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel_3.setLayout(gbl_panel_3);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
			},
			new String[] {
				"Mã Hàng", "Tên Hàng", "Đơn Vị", "Số Lượng", "Đơn Giá", "Thành Tiền"
			}
		));
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridx = 0;
		gbc_table.gridy = 0;
		panel_3.add(new JScrollPane(table), gbc_table);

	}}

