package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JToolBar;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Insets;
import javax.swing.JTabbedPane;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JDesktopPane;

public class NhapSoDuDauKy extends JInternalFrame {
	JDesktopPane desktopPane = new JDesktopPane();

	

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NhapSoDuDauKy frame = new NhapSoDuDauKy();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	/**
	 * Create the frame.
	 */
	public NhapSoDuDauKy() {
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setMaximizable(true);
		setClosable(true);
		setBounds(100, 100, 634, 555);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JPanel panel_menuLeft = new JPanel();
		GridBagConstraints gbc_panel_menuLeft = new GridBagConstraints();
		gbc_panel_menuLeft.insets = new Insets(0, 0, 0, 5);
		gbc_panel_menuLeft.anchor = GridBagConstraints.WEST;
		gbc_panel_menuLeft.fill = GridBagConstraints.VERTICAL;
		gbc_panel_menuLeft.gridx = 0;
		gbc_panel_menuLeft.gridy = 0;
		panel.add(panel_menuLeft, gbc_panel_menuLeft);
		GridBagLayout gbl_panel_menuLeft = new GridBagLayout();
		gbl_panel_menuLeft.columnWidths = new int[]{65, 0};
		gbl_panel_menuLeft.rowHeights = new int[]{43, 0, 0, 0, 0, 0};
		gbl_panel_menuLeft.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel_menuLeft.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_menuLeft.setLayout(gbl_panel_menuLeft);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setOrientation(SwingConstants.VERTICAL);
		GridBagConstraints gbc_toolBar = new GridBagConstraints();
		gbc_toolBar.anchor = GridBagConstraints.WEST;
		gbc_toolBar.insets = new Insets(0, 0, 5, 0);
		gbc_toolBar.gridx = 0;
		gbc_toolBar.gridy = 0;
		panel_menuLeft.add(toolBar, gbc_toolBar);
		
		final JCheckBox chckbxTKKD = new JCheckBox("");
		
		chckbxTKKD.setIcon(new ImageIcon("HinhAnh\\menuleft1.png"));
		toolBar.add(chckbxTKKD);
		
		final JButton button = new JButton("");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NhapKhoDauKy nhapKhoDauKy = new NhapKhoDauKy();
				desktopPane.add(nhapKhoDauKy);
				nhapKhoDauKy.show();
			}
		});
//		button.setOpaque(true);
		button.setBorderPainted(false);
		
		button.setHorizontalAlignment(SwingConstants.LEADING);
		button.setVisible(false);
		button.setIcon(new ImageIcon("HinhAnh\\Button2.png"));
		toolBar.add(button);
		
		chckbxTKKD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(chckbxTKKD.isSelected()== true)
				{
					button.setVisible(true);
				}
				else
				{
					button.setVisible(false);
				}
			}
		});
		
		
		JToolBar toolBar_1 = new JToolBar();
		toolBar_1.setOrientation(SwingConstants.VERTICAL);
		GridBagConstraints gbc_toolBar_1 = new GridBagConstraints();
		gbc_toolBar_1.insets = new Insets(0, 0, 5, 0);
		gbc_toolBar_1.gridx = 0;
		gbc_toolBar_1.gridy = 1;
		panel_menuLeft.add(toolBar_1, gbc_toolBar_1);
		
		final JCheckBox chckbxNewCheckBox = new JCheckBox("");
		
		chckbxNewCheckBox.setIcon(new ImageIcon("HinhAnh\\menuleft2.png"));
		toolBar_1.add(chckbxNewCheckBox);
		
		final JButton btnTheoK = new JButton("");
		btnTheoK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BangKe_TheoKy bangKe_TheoKy = new BangKe_TheoKy();
				desktopPane.add(bangKe_TheoKy);
				bangKe_TheoKy.show();
			}
		});
		btnTheoK.setIcon(new ImageIcon("HinhAnh\\Button3.png"));
		btnTheoK.setVisible(false);
		btnTheoK.setBorderPainted(false);
		toolBar_1.add(btnTheoK);
		
		final JButton btnTheoHngHa = new JButton("");
		btnTheoHngHa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BangKe_TheoHangHoa bangKe_TheoHangHoa = new BangKe_TheoHangHoa();
				desktopPane.add(bangKe_TheoHangHoa);
				bangKe_TheoHangHoa.show();
			}
		});
		btnTheoHngHa.setIcon(new ImageIcon("HinhAnh\\Button4.png"));
		btnTheoHngHa.setVisible(false);
		btnTheoHngHa.setBorderPainted(false);
		toolBar_1.add(btnTheoHngHa);
		
		chckbxNewCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(chckbxNewCheckBox.isSelected())
				{
					btnTheoK.setVisible(true);
					btnTheoHngHa.setVisible(true);
				}
				else
				{
					btnTheoK.setVisible(false);
					btnTheoHngHa.setVisible(false);
				}
			}
		});
		
		JToolBar toolBar_2 = new JToolBar();
		toolBar_2.setOrientation(SwingConstants.VERTICAL);
		GridBagConstraints gbc_toolBar_2 = new GridBagConstraints();
		gbc_toolBar_2.insets = new Insets(0, 0, 5, 0);
		gbc_toolBar_2.gridx = 0;
		gbc_toolBar_2.gridy = 2;
		panel_menuLeft.add(toolBar_2, gbc_toolBar_2);
		
		final JCheckBox chckbxNewCheckBox_1 = new JCheckBox("");
		
		chckbxNewCheckBox_1.setIcon(new ImageIcon("HinhAnh\\menuleft3.png"));
		toolBar_2.add(chckbxNewCheckBox_1);
		
		final JButton btnKhachhang = new JButton("");
		btnKhachhang.setIcon(new ImageIcon("HinhAnh\\Button5.png"));
		btnKhachhang.setVisible(false);
		btnKhachhang.setBorderPainted(false);
		toolBar_2.add(btnKhachhang);
		
		final JButton btnNhacungcap = new JButton("");
		btnNhacungcap.setIcon(new ImageIcon("HinhAnh\\Button6.png"));
		btnNhacungcap.setBorderPainted(false);
		btnNhacungcap.setVisible(false);
		toolBar_2.add(btnNhacungcap);
		
		chckbxNewCheckBox_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(chckbxNewCheckBox_1.isSelected()== true)
				{
					btnKhachhang.setVisible(true);
					btnNhacungcap.setVisible(true);
					
				}
				else
				{
					btnKhachhang.setVisible(false);
					btnNhacungcap.setVisible(false);
				}
			}
		});
		
		JToolBar toolBar_3 = new JToolBar();
		toolBar_3.setOrientation(SwingConstants.VERTICAL);
		GridBagConstraints gbc_toolBar_3 = new GridBagConstraints();
		gbc_toolBar_3.insets = new Insets(0, 0, 5, 0);
		gbc_toolBar_3.gridx = 0;
		gbc_toolBar_3.gridy = 3;
		panel_menuLeft.add(toolBar_3, gbc_toolBar_3);
		
		final JCheckBox chckbxNewCheckBox_2 = new JCheckBox("");
		
		chckbxNewCheckBox_2.setIcon(new ImageIcon("HinhAnh\\menuleft4.png"));
		toolBar_3.add(chckbxNewCheckBox_2);
		
		final JButton btnHanghoa = new JButton("");
		btnHanghoa.setIcon(new ImageIcon("HinhAnh\\Button7.png"));
		btnHanghoa.setVisible(false);
		btnHanghoa.setBorderPainted(false);
		toolBar_3.add(btnHanghoa);
		
		final JButton btnKhachhang_1 = new JButton("");
		btnKhachhang_1.setIcon(new ImageIcon("HinhAnh\\Button5.png"));
		btnKhachhang_1.setVisible(false);
		btnKhachhang_1.setBorderPainted(false);
		toolBar_3.add(btnKhachhang_1);
		
		final JButton btnNhacungcap_1 = new JButton("");
		btnNhacungcap_1.setIcon(new ImageIcon("HinhAnh\\Button6.png"));
		btnNhacungcap_1.setVisible(false);
		btnNhacungcap_1.setBorderPainted(false);
		toolBar_3.add(btnNhacungcap_1);
		
		final JButton btnFileMau = new JButton("");
		btnFileMau.setIcon(new ImageIcon("HinhAnh\\Button8.png"));
		btnFileMau.setVisible(false);
		btnFileMau.setBorderPainted(false);
		toolBar_3.add(btnFileMau);
		
		
		
		GridBagConstraints gbc_desktopPane = new GridBagConstraints();
		gbc_desktopPane.fill = GridBagConstraints.BOTH;
		gbc_desktopPane.gridx = 1;
		gbc_desktopPane.gridy = 0;
		panel.add(desktopPane, gbc_desktopPane);
		chckbxNewCheckBox_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(chckbxNewCheckBox_2.isSelected()== true)
				{
					btnHanghoa.setVisible(true);
					btnKhachhang_1.setVisible(true);
					btnNhacungcap_1.setVisible(true);
					btnFileMau.setVisible(true);
				}
				else
				{
					btnHanghoa.setVisible(false);
					btnKhachhang_1.setVisible(false);
					btnNhacungcap_1.setVisible(false);
					btnFileMau.setVisible(false);
				}
			}
		});
	}

}
