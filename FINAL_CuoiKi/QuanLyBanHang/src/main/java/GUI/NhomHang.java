package GUI;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import java.awt.GridLayout;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Color;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.LineBorder;
import Util.*;
import pojo.*;
import dao.*;
import javax.swing.JScrollPane;
import controller.NhomHang_controller;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NhomHang extends JInternalFrame {
	private nhomhangDAO nhomhangDAO = new nhomhangDAO();
	public static JTable jtablenhomhang;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NhomHang frame = new NhomHang();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NhomHang() {
		setBounds(100, 100, 904, 354);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 888, 324);
		getContentPane().add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.ORANGE);
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 0;
		panel.add(panel_1, gbc_panel_1);
		
		JButton btnThem = new JButton("Thêm");
		btnThem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dialogThemNhomHang themnhomhang= new dialogThemNhomHang();
				themnhomhang.setVisible(true);
				
			}
		});
		panel_1.add(btnThem);
		
		JButton buttonSua = new JButton("Sua");
		buttonSua.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dialogSuaNhomHang dialogSuaNhomHang = new dialogSuaNhomHang();
				dialogSuaNhomHang.setVisible(true);
			}
		});
		panel_1.add(buttonSua);
		
		JButton btnXoa = new JButton("Xoa");
		panel_1.add(btnXoa);
		
		JButton button_2 = new JButton("New button");
		panel_1.add(button_2);
		
		JButton button_3 = new JButton("New button");
		panel_1.add(button_3);
		
		JButton button_4 = new JButton("New button");
		panel_1.add(button_4);
		
		JButton button_5 = new JButton("New button");
		panel_1.add(button_5);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.LIGHT_GRAY);
		panel_2.setForeground(Color.GRAY);
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 1;
		panel.add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_2.rowHeights = new int[]{0, 0, 0};
		gbl_panel_2.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		JScrollPane scrollPaneNhomHang = new JScrollPane();
		GridBagConstraints gbc_scrollPaneNhomHang = new GridBagConstraints();
		gbc_scrollPaneNhomHang.gridheight = 2;
		gbc_scrollPaneNhomHang.gridwidth = 9;
		gbc_scrollPaneNhomHang.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPaneNhomHang.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneNhomHang.gridx = 0;
		gbc_scrollPaneNhomHang.gridy = 0;
		panel_2.add(scrollPaneNhomHang, gbc_scrollPaneNhomHang);
		
		jtablenhomhang = new JTable();
		scrollPaneNhomHang.setViewportView(jtablenhomhang);
		NhomHang_controller.LoadData();
	}
	
}
