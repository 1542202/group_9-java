package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.dmMuaHang_NhapPhieu;
import dao.nhaphangDAO;
import pojo.Sanpham;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.awt.event.ActionEvent;

public class PhieuNhapHang_Them extends JDialog {

	private final JPanel contentPanel = new JPanel();
	public static JTextField txtGhiChu;
	public static JTextField txtGia;
	public static JTextField txtSoLuong;
	public static JTextField txtDonVi;
	public static  JComboBox cddMaHang;
	public static JComboBox cbbTenHang;
	DefaultComboBoxModel dfc1 = new DefaultComboBoxModel();
	DefaultComboBoxModel dfc2 = new DefaultComboBoxModel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PhieuNhapHang_Them dialog = new PhieuNhapHang_Them();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PhieuNhapHang_Them() {
		setTitle("Thêm Phiếu Nhập Hàng");
		setBounds(100, 100, 365, 265);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblNewLabel = new JLabel("Mã Hàng");
			GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
			gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
			gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
			gbc_lblNewLabel.gridx = 0;
			gbc_lblNewLabel.gridy = 0;
			contentPanel.add(lblNewLabel, gbc_lblNewLabel);
		}
		{
			
			 cddMaHang = new JComboBox();
			 cddMaHang.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent arg0) {
			 		
			 			Sanpham sp = nhaphangDAO.tim_TenSP(cddMaHang.getSelectedItem().toString());
			 		//System.out.println(sp.getTenSp());
			 			dfc2.setSelectedItem(sp.getTenSp().toString());
			 			cbbTenHang.setModel(dfc2);
			 			
			 			

						nhaphangDAO nhd = new nhaphangDAO();
						//Sanpham sp = nhd.tim_TenSP(cddMaHang.getSelectedItem().toString());
						txtDonVi.setText(sp.getDonvi().getTenDv());
						txtGia.setText(sp.getGiaBanLe().toString());
						
			 	
			 		
			 	}
			 });
			cddMaHang.setEditable(true);
			GridBagConstraints gbc_cddMaHang = new GridBagConstraints();
			gbc_cddMaHang.insets = new Insets(0, 0, 5, 0);
			gbc_cddMaHang.fill = GridBagConstraints.HORIZONTAL;
			gbc_cddMaHang.gridx = 1;
			gbc_cddMaHang.gridy = 0;
			contentPanel.add(cddMaHang, gbc_cddMaHang);
			
			
			nhaphangDAO nhd = new nhaphangDAO();
			for(Sanpham sp : nhd.finall_sanpham())
			{
				dfc1.addElement(sp.getSanPhamId());
			}
			
			cddMaHang.setModel(dfc1);
		}
		{
			JLabel lblTnHng = new JLabel("Tên Hàng");
			GridBagConstraints gbc_lblTnHng = new GridBagConstraints();
			gbc_lblTnHng.anchor = GridBagConstraints.EAST;
			gbc_lblTnHng.insets = new Insets(0, 0, 5, 5);
			gbc_lblTnHng.gridx = 0;
			gbc_lblTnHng.gridy = 1;
			contentPanel.add(lblTnHng, gbc_lblTnHng);
		}
		{
			 cbbTenHang = new JComboBox();
			 
			cbbTenHang.setEditable(true);
			GridBagConstraints gbc_cbbTenHang = new GridBagConstraints();
			gbc_cbbTenHang.insets = new Insets(0, 0, 5, 0);
			gbc_cbbTenHang.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbbTenHang.gridx = 1;
			gbc_cbbTenHang.gridy = 1;
			contentPanel.add(cbbTenHang, gbc_cbbTenHang);
			
		
			nhaphangDAO nhd = new nhaphangDAO();
			for(Sanpham sp : nhd.finall_sanpham())
			{
				dfc2.addElement(sp.getTenSp());
			}
			
			cbbTenHang.setModel(dfc2);
			
			cbbTenHang.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent arg0) {
			 		
			 		dmMuaHang_NhapPhieu dm = new dmMuaHang_NhapPhieu();
			 		Sanpham sp = dm.tim_MaSP(cbbTenHang.getSelectedItem().toString());
			 		
			 		dfc1.setSelectedItem(sp.getSanPhamId().toString());
			 		cddMaHang.setModel(dfc1);
			 		
			 		
			 	}
			 });
		}
		{
			
			
			JLabel lblnV = new JLabel("Đơn Vị");
			GridBagConstraints gbc_lblnV = new GridBagConstraints();
			gbc_lblnV.anchor = GridBagConstraints.EAST;
			gbc_lblnV.insets = new Insets(0, 0, 5, 5);
			gbc_lblnV.gridx = 0;
			gbc_lblnV.gridy = 2;
			contentPanel.add(lblnV, gbc_lblnV);
		}
		{
			txtDonVi = new JTextField();
			txtDonVi.setEditable(false);
			GridBagConstraints gbc_txtDonVi = new GridBagConstraints();
			gbc_txtDonVi.insets = new Insets(0, 0, 5, 0);
			gbc_txtDonVi.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtDonVi.gridx = 1;
			gbc_txtDonVi.gridy = 2;
			contentPanel.add(txtDonVi, gbc_txtDonVi);
			txtDonVi.setColumns(10);
			
		}
		{
			JLabel lblSLng = new JLabel("Số Lượng");
			GridBagConstraints gbc_lblSLng = new GridBagConstraints();
			gbc_lblSLng.anchor = GridBagConstraints.EAST;
			gbc_lblSLng.insets = new Insets(0, 0, 5, 5);
			gbc_lblSLng.gridx = 0;
			gbc_lblSLng.gridy = 3;
			contentPanel.add(lblSLng, gbc_lblSLng);
		}
		{
			txtSoLuong = new JTextField();
			GridBagConstraints gbc_txtSoLuong = new GridBagConstraints();
			gbc_txtSoLuong.insets = new Insets(0, 0, 5, 0);
			gbc_txtSoLuong.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtSoLuong.gridx = 1;
			gbc_txtSoLuong.gridy = 3;
			contentPanel.add(txtSoLuong, gbc_txtSoLuong);
			txtSoLuong.setColumns(10);
		}
		{
			JLabel lblnGi = new JLabel("Đơn Giá");
			GridBagConstraints gbc_lblnGi = new GridBagConstraints();
			gbc_lblnGi.anchor = GridBagConstraints.EAST;
			gbc_lblnGi.insets = new Insets(0, 0, 5, 5);
			gbc_lblnGi.gridx = 0;
			gbc_lblnGi.gridy = 4;
			contentPanel.add(lblnGi, gbc_lblnGi);
		}
		{
			txtGia = new JTextField();
			txtGia.setEditable(false);
			GridBagConstraints gbc_txtGia = new GridBagConstraints();
			gbc_txtGia.insets = new Insets(0, 0, 5, 0);
			gbc_txtGia.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtGia.gridx = 1;
			gbc_txtGia.gridy = 4;
			contentPanel.add(txtGia, gbc_txtGia);
			txtGia.setColumns(10);
		}
		{
			JLabel lblGhiCh = new JLabel("Ghi Chú");
			GridBagConstraints gbc_lblGhiCh = new GridBagConstraints();
			gbc_lblGhiCh.insets = new Insets(0, 0, 5, 5);
			gbc_lblGhiCh.anchor = GridBagConstraints.EAST;
			gbc_lblGhiCh.gridx = 0;
			gbc_lblGhiCh.gridy = 5;
			contentPanel.add(lblGhiCh, gbc_lblGhiCh);
		}
		{
			txtGhiChu = new JTextField();
			GridBagConstraints gbc_txtGhiChu = new GridBagConstraints();
			gbc_txtGhiChu.insets = new Insets(0, 0, 5, 0);
			gbc_txtGhiChu.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtGhiChu.gridx = 1;
			gbc_txtGhiChu.gridy = 5;
			contentPanel.add(txtGhiChu, gbc_txtGhiChu);
			txtGhiChu.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dmMuaHang_NhapPhieu.add_chitiet_nhaphang();
						dmMuaHang_NhapPhieu.load_data_chitiet();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
