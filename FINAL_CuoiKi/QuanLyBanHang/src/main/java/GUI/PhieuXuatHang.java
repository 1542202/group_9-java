package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import controller.cnBanHang_PhieuXuat;
import controller.dmMuaHang_NhapPhieu;
import dao.muahangDAO;
import dao.nhanvienDAO;
import dao.nhaphangDAO;
import dao.xuathangDAO;
//import eu.schudt.javafx.controls.calendar.DatePicker;
import pojo.Chucvu;
import pojo.Khachang;
import pojo.Khohang;
import pojo.Nhacungcap;
import pojo.Nhanvien;
import pojo.Nhaphang;
import pojo.Xuathang;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.Insets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.SpringLayout;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.table.DefaultTableModel;

public class PhieuXuatHang extends JInternalFrame {

	private JPanel contentPane;
	public static JTextField txtDC;
	public static JTextField txtGhiChu;
	public static JTextField txtDT;
	public static JTextField txtVat;
	public static JTextField txtSoPhieu;
	public static JTextField textField_7;
	private SpringLayout springLayout;
	public static JTextField txtMaPhieu;
	public static JTextField textField_9;
	public static JComboBox cbbKH;
	public static JComboBox cbbNhanVien;
	public static JComboBox cbbDK;
	public static JComboBox cbbKho;
	public static JComboBox cbbHT;
	public static Date selectedDate_ngay;
	public static Date selectedDate_ngaythanhtoan;
	public static Date selectedDate_ngaygiao;
	public static JComboBox cbbMaPhieu;
	public static JComboBox cbbMaKH;
	DefaultComboBoxModel dfmakh;
	
	public  JDatePickerImpl ngay= ngaythang();
	public  JDatePickerImpl ngay_hanthanhtoan= ngaythang();
	public JDatePickerImpl ngay_giao= ngaythang();
	DefaultComboBoxModel dfc = new DefaultComboBoxModel();
	public static JTable table;
	private JTextField textField;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PhieuXuatHang frame = new PhieuXuatHang();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public  JDatePickerImpl ngaythang()
	{
		UtilDateModel model1 = new UtilDateModel();
		Properties p1 = new Properties();
		JDatePanelImpl datePanel = new JDatePanelImpl(model1, p1);
		JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		return datePicker;
	}
	/**
	 * Create the frame.
	 */
	class DateLabelFormatter  extends AbstractFormatter {

        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String datePattern = "yyyy-MM-dd";
        private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

        @Override
        public Object stringToValue(String text) throws ParseException {
            return dateFormatter.parseObject(text);
        }

        @Override
        public String valueToString(Object value) throws ParseException {
            if (value != null) {
                Calendar cal = (Calendar) value;
                return dateFormatter.format(cal.getTime());
            }

            return "";
        }

    }
	
	public PhieuXuatHang() {
		
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		setTitle("Phiếu Xuất Hàng");
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setClosable(true);
		setMaximizable(true);
		setBounds(100, 100, 780, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JButton btnNewButton = new JButton("Thêm Phiếu");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			selectedDate_ngay = (Date) ngay.getModel().getValue();
				
			selectedDate_ngaythanhtoan =  (Date) ngay_hanthanhtoan.getModel().getValue();
				
			selectedDate_ngaygiao = (Date) ngay_giao.getModel().getValue();
					cnBanHang_PhieuXuat.add_PhieuXuat();
			
	
					DefaultComboBoxModel df_maphieu = new DefaultComboBoxModel();
					xuathangDAO xhd_maphieu = new xuathangDAO();
					for(Xuathang xh: xhd_maphieu.findAll_XuatHang())
					{
						df_maphieu.addElement(xh.getXuatHangId());
						
					}
					cbbMaPhieu.setModel(df_maphieu);
	
				
			}
		});
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("New button");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				//DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
				 
				 
				// System.out.println(selectedDate_ngay);
			
					
				
				 
			}
		});
		panel.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("New button");
		panel.add(btnNewButton_2);
		
		JButton button = new JButton("New button");
		panel.add(button);
		
		JButton button_1 = new JButton("New button");
		panel.add(button_1);
		
		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		contentPane.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblTnNcc = new JLabel("Khách Hàng");
		GridBagConstraints gbc_lblTnNcc = new GridBagConstraints();
		gbc_lblTnNcc.anchor = GridBagConstraints.WEST;
		gbc_lblTnNcc.insets = new Insets(0, 0, 5, 5);
		gbc_lblTnNcc.gridx = 0;
		gbc_lblTnNcc.gridy = 0;
		panel_1.add(lblTnNcc, gbc_lblTnNcc);
		
		 cbbKH = new JComboBox();
		 cbbKH.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		//System.out.println(cbbTenNCC.getSelectedItem().toString());
		 		//dmMuaHang_NhapPhieu dm =null;
		 	//	String n = dm.timMANCC(cbbKH.getSelectedItem().toString()).getMaNcc();
		 		
		 		
		 		//txtMaKH.setText(n);
		 		muahangDAO mhd = new muahangDAO();
		 		Khachang kh = mhd.find_Makh(cbbKH.getSelectedItem().toString());
		 		dfmakh.setSelectedItem(kh.getMaKhachHang().toString());
		 	
		 		cbbMaKH.setModel(dfmakh);
		 		
		 	}
		 });
		cbbKH.setEditable(true);
		final DefaultComboBoxModel dfkh = new DefaultComboBoxModel();
		muahangDAO mhdao = new muahangDAO();
		for(Khachang kh : mhdao.findAll_KH())
		{
			dfkh.addElement(kh.getTenKh());
			
			
		}
		this.cbbKH.setModel(dfkh);
		
		
		GridBagConstraints gbc_cbbKH = new GridBagConstraints();
		gbc_cbbKH.insets = new Insets(0, 0, 5, 5);
		gbc_cbbKH.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbbKH.gridx = 1;
		gbc_cbbKH.gridy = 0;
		panel_1.add(cbbKH, gbc_cbbKH);
		
		JLabel lblMNcc = new JLabel("Mã KH");
		GridBagConstraints gbc_lblMNcc = new GridBagConstraints();
		gbc_lblMNcc.anchor = GridBagConstraints.WEST;
		gbc_lblMNcc.insets = new Insets(0, 0, 5, 5);
		gbc_lblMNcc.gridx = 2;
		gbc_lblMNcc.gridy = 0;
		panel_1.add(lblMNcc, gbc_lblMNcc);
		
		 cbbMaKH = new JComboBox();
		 cbbMaKH.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		
		 		Khachang kh = muahangDAO.find_Tenkh(cbbMaKH.getSelectedItem().toString());
		 		dfkh.setSelectedItem(kh.getTenKh().toString());
		 		cbbKH.setModel(dfkh);
		 		
		 		
		 	}
		 });
		GridBagConstraints gbc_cbbMaKH = new GridBagConstraints();
		gbc_cbbMaKH.insets = new Insets(0, 0, 5, 5);
		gbc_cbbMaKH.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbbMaKH.gridx = 3;
		gbc_cbbMaKH.gridy = 0;
		panel_1.add(cbbMaKH, gbc_cbbMaKH);
		
		
		 dfmakh = new DefaultComboBoxModel();
		muahangDAO mhdao_mahang = new muahangDAO();
		for(Khachang kh : mhdao_mahang.findAll_KH())
		{
			dfmakh.addElement(kh.getMaKhachHang());
			
			
		}
		this.cbbMaKH.setModel(dfmakh);
		
		
		JLabel lblMPhiu = new JLabel("Mã Phiếu");
		GridBagConstraints gbc_lblMPhiu = new GridBagConstraints();
		gbc_lblMPhiu.anchor = GridBagConstraints.WEST;
		gbc_lblMPhiu.insets = new Insets(0, 0, 5, 5);
		gbc_lblMPhiu.gridx = 4;
		gbc_lblMPhiu.gridy = 0;
		panel_1.add(lblMPhiu, gbc_lblMPhiu);
		
		txtMaPhieu = new JTextField();
		GridBagConstraints gbc_txtMaPhieu = new GridBagConstraints();
		gbc_txtMaPhieu.insets = new Insets(0, 0, 5, 5);
		gbc_txtMaPhieu.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtMaPhieu.gridx = 5;
		gbc_txtMaPhieu.gridy = 0;
		panel_1.add(txtMaPhieu, gbc_txtMaPhieu);
		txtMaPhieu.setColumns(10);
		
		JLabel lblaCh = new JLabel("Địa Chỉ");
		GridBagConstraints gbc_lblaCh = new GridBagConstraints();
		gbc_lblaCh.anchor = GridBagConstraints.WEST;
		gbc_lblaCh.insets = new Insets(0, 0, 5, 5);
		gbc_lblaCh.gridx = 0;
		gbc_lblaCh.gridy = 1;
		panel_1.add(lblaCh, gbc_lblaCh);
		
		txtDC = new JTextField();
		txtDC.setColumns(10);
		GridBagConstraints gbc_txtDC = new GridBagConstraints();
		gbc_txtDC.insets = new Insets(0, 0, 5, 5);
		gbc_txtDC.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDC.gridx = 1;
		gbc_txtDC.gridy = 1;
		panel_1.add(txtDC, gbc_txtDC);
		
		JLabel lblinThoi = new JLabel("Điện Thoại");
		GridBagConstraints gbc_lblinThoi = new GridBagConstraints();
		gbc_lblinThoi.anchor = GridBagConstraints.WEST;
		gbc_lblinThoi.insets = new Insets(0, 0, 5, 5);
		gbc_lblinThoi.gridx = 2;
		gbc_lblinThoi.gridy = 1;
		panel_1.add(lblinThoi, gbc_lblinThoi);
		
		txtDT = new JTextField();
		GridBagConstraints gbc_txtDT = new GridBagConstraints();
		gbc_txtDT.insets = new Insets(0, 0, 5, 5);
		gbc_txtDT.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDT.gridx = 3;
		gbc_txtDT.gridy = 1;
		panel_1.add(txtDT, gbc_txtDT);
		txtDT.setColumns(10);
		
		JLabel lblNgy = new JLabel("Ngày");
		GridBagConstraints gbc_lblNgy = new GridBagConstraints();
		gbc_lblNgy.anchor = GridBagConstraints.WEST;
		gbc_lblNgy.insets = new Insets(0, 0, 5, 5);
		gbc_lblNgy.gridx = 4;
		gbc_lblNgy.gridy = 1;
		panel_1.add(lblNgy, gbc_lblNgy);
		
		//textField_9 = new JTextField();
		GridBagConstraints gbc_textField_9 = new GridBagConstraints();
		gbc_textField_9.insets = new Insets(0, 0, 5, 0);
		gbc_textField_9.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_9.gridx = 5;
		gbc_textField_9.gridy = 1;
//		
		
		panel_1.add(ngay, gbc_textField_9);
		
		
		//textField_9.setColumns(10);
		
		JLabel lblGhiCh = new JLabel("Ghi Chú");
		GridBagConstraints gbc_lblGhiCh = new GridBagConstraints();
		gbc_lblGhiCh.anchor = GridBagConstraints.WEST;
		gbc_lblGhiCh.insets = new Insets(0, 0, 5, 5);
		gbc_lblGhiCh.gridx = 0;
		gbc_lblGhiCh.gridy = 2;
		panel_1.add(lblGhiCh, gbc_lblGhiCh);
		
		txtGhiChu = new JTextField();
		txtGhiChu.setColumns(10);
		GridBagConstraints gbc_txtGhiChu = new GridBagConstraints();
		gbc_txtGhiChu.insets = new Insets(0, 0, 5, 5);
		gbc_txtGhiChu.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtGhiChu.gridx = 1;
		gbc_txtGhiChu.gridy = 2;
		panel_1.add(txtGhiChu, gbc_txtGhiChu);
		
		JLabel lblSHan = new JLabel("Số Hóa Đơn VAT");
		GridBagConstraints gbc_lblSHan = new GridBagConstraints();
		gbc_lblSHan.anchor = GridBagConstraints.WEST;
		gbc_lblSHan.insets = new Insets(0, 0, 5, 5);
		gbc_lblSHan.gridx = 2;
		gbc_lblSHan.gridy = 2;
		panel_1.add(lblSHan, gbc_lblSHan);
		
		txtVat = new JTextField();
		GridBagConstraints gbc_txtVat = new GridBagConstraints();
		gbc_txtVat.insets = new Insets(0, 0, 5, 5);
		gbc_txtVat.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtVat.gridx = 3;
		gbc_txtVat.gridy = 2;
		panel_1.add(txtVat, gbc_txtVat);
		txtVat.setColumns(10);
		
		JLabel lblNhnVin = new JLabel("Nhân Viên");
		GridBagConstraints gbc_lblNhnVin = new GridBagConstraints();
		gbc_lblNhnVin.anchor = GridBagConstraints.WEST;
		gbc_lblNhnVin.insets = new Insets(0, 0, 5, 5);
		gbc_lblNhnVin.gridx = 4;
		gbc_lblNhnVin.gridy = 2;
		panel_1.add(lblNhnVin, gbc_lblNhnVin);
		
		 cbbNhanVien = new JComboBox();
		 DefaultComboBoxModel dfnv= new DefaultComboBoxModel();
		 nhanvienDAO nvd = new nhanvienDAO();
		 for(Nhanvien nv : nvd.findAll())
		 {
			 dfnv.addElement(nv.getMaNv());
		 }
		 this.cbbNhanVien.setModel(dfnv);
		 
		
		 
		GridBagConstraints gbc_cbbNhanVien = new GridBagConstraints();
		gbc_cbbNhanVien.insets = new Insets(0, 0, 5, 5);
		gbc_cbbNhanVien.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbbNhanVien.gridx = 5;
		gbc_cbbNhanVien.gridy = 2;
		panel_1.add(cbbNhanVien, gbc_cbbNhanVien);
		
		JLabel lblkThanhTon = new JLabel("ĐK Thanh Toán");
		GridBagConstraints gbc_lblkThanhTon = new GridBagConstraints();
		gbc_lblkThanhTon.insets = new Insets(0, 0, 5, 5);
		gbc_lblkThanhTon.anchor = GridBagConstraints.WEST;
		gbc_lblkThanhTon.gridx = 0;
		gbc_lblkThanhTon.gridy = 3;
		panel_1.add(lblkThanhTon, gbc_lblkThanhTon);
		
		 cbbDK = new JComboBox();
		 cbbDK.setEditable(true);
		 
		 cbbDK.addItem("Công nợ");
		 cbbDK.addItem("Thanh toán ngay");
		 cbbDK.addItem("Chọn điều khoảng thanh toán");
		 cbbDK.setSelectedIndex(2);
		 
		GridBagConstraints gbc_cbbDK = new GridBagConstraints();
		gbc_cbbDK.insets = new Insets(0, 0, 5, 5);
		gbc_cbbDK.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbbDK.gridx = 1;
		gbc_cbbDK.gridy = 3;
		panel_1.add(cbbDK, gbc_cbbDK);
		
		JLabel lblSPhiuVit = new JLabel("Số Phiếu Viết Tay");
		GridBagConstraints gbc_lblSPhiuVit = new GridBagConstraints();
		gbc_lblSPhiuVit.anchor = GridBagConstraints.WEST;
		gbc_lblSPhiuVit.insets = new Insets(0, 0, 5, 5);
		gbc_lblSPhiuVit.gridx = 2;
		gbc_lblSPhiuVit.gridy = 3;
		panel_1.add(lblSPhiuVit, gbc_lblSPhiuVit);
		
		txtSoPhieu = new JTextField();
		GridBagConstraints gbc_txtSoPhieu = new GridBagConstraints();
		gbc_txtSoPhieu.insets = new Insets(0, 0, 5, 5);
		gbc_txtSoPhieu.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSoPhieu.gridx = 3;
		gbc_txtSoPhieu.gridy = 3;
		panel_1.add(txtSoPhieu, gbc_txtSoPhieu);
		txtSoPhieu.setColumns(10);
		
		JLabel lblKhoNhp = new JLabel("Kho Nhập");
		GridBagConstraints gbc_lblKhoNhp = new GridBagConstraints();
		gbc_lblKhoNhp.anchor = GridBagConstraints.WEST;
		gbc_lblKhoNhp.insets = new Insets(0, 0, 5, 5);
		gbc_lblKhoNhp.gridx = 4;
		gbc_lblKhoNhp.gridy = 3;
		panel_1.add(lblKhoNhp, gbc_lblKhoNhp);
		
		 cbbKho = new JComboBox();
		 cbbKho.setEditable(true);
		 nhaphangDAO nhd = new nhaphangDAO();
		 for(Khohang kho : nhd.findAll() )
		 {
			 dfc.addElement(kho.getKhoId());
			 //dfc.addElement(kho.getTenKho());
		 }
		 this.cbbKho.setModel(dfc);
		GridBagConstraints gbc_cbbKho = new GridBagConstraints();
		gbc_cbbKho.insets = new Insets(0, 0, 5, 5);
		gbc_cbbKho.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbbKho.gridx = 5;
		gbc_cbbKho.gridy = 3;
		panel_1.add(cbbKho, gbc_cbbKho);
		
		JLabel lblHtThanhTon = new JLabel("HT Thanh Toán");
		GridBagConstraints gbc_lblHtThanhTon = new GridBagConstraints();
		gbc_lblHtThanhTon.insets = new Insets(0, 0, 0, 5);
		gbc_lblHtThanhTon.anchor = GridBagConstraints.WEST;
		gbc_lblHtThanhTon.gridx = 0;
		gbc_lblHtThanhTon.gridy = 4;
		panel_1.add(lblHtThanhTon, gbc_lblHtThanhTon);
		
		 cbbHT = new JComboBox();
		 cbbHT.setEditable(true);
		 
		
		cbbHT.addItem("Tiền mặt");
		cbbHT.addItem("Chuyển Khoảng");
		cbbHT.addItem("Chọn hình thức thanh toán");
		cbbHT.setSelectedIndex(2);
		
		 
		 
		 
		GridBagConstraints gbc_cbbHT = new GridBagConstraints();
		gbc_cbbHT.insets = new Insets(0, 0, 0, 5);
		gbc_cbbHT.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbbHT.gridx = 1;
		gbc_cbbHT.gridy = 4;
		panel_1.add(cbbHT, gbc_cbbHT);
		
		JLabel lblHnThanhTon = new JLabel("Hạn Thanh Toán");
		GridBagConstraints gbc_lblHnThanhTon = new GridBagConstraints();
		gbc_lblHnThanhTon.anchor = GridBagConstraints.WEST;
		gbc_lblHnThanhTon.insets = new Insets(0, 0, 0, 5);
		gbc_lblHnThanhTon.gridx = 2;
		gbc_lblHnThanhTon.gridy = 4;
		panel_1.add(lblHnThanhTon, gbc_lblHnThanhTon);
		
//		UtilDateModel model = new UtilDateModel();
//		Properties p = new Properties();
//		JDatePanelImpl datePanel2 = new JDatePanelImpl(model, p);
//		JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel, new DateLabelFormatter());
//	
		
		
		//textField_7 = new JTextField();
		GridBagConstraints gbc_textField_7 = new GridBagConstraints();
		gbc_textField_7.insets = new Insets(0, 0, 0, 5);
		gbc_textField_7.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_7.gridx = 3;
		gbc_textField_7.gridy = 4;
//		
		
		panel_1.add(ngay_hanthanhtoan, gbc_textField_7);
		
		JLabel lblNgyGiao = new JLabel("Ngày Giao");
		GridBagConstraints gbc_lblNgyGiao = new GridBagConstraints();
		gbc_lblNgyGiao.anchor = GridBagConstraints.EAST;
		gbc_lblNgyGiao.insets = new Insets(0, 0, 0, 5);
		gbc_lblNgyGiao.gridx = 4;
		gbc_lblNgyGiao.gridy = 4;
		panel_1.add(lblNgyGiao, gbc_lblNgyGiao);
		
		//textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 0, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 5;
		gbc_textField.gridy = 4;
		panel_1.add(ngay_giao, gbc_textField);
		
		
		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.anchor = GridBagConstraints.WEST;
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.fill = GridBagConstraints.VERTICAL;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 2;
		contentPane.add(panel_2, gbc_panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		cbbMaPhieu = new JComboBox();
		cbbMaPhieu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				cnBanHang_PhieuXuat.load_data_chitiet();
			}
		});
		panel_2.add(cbbMaPhieu);
		DefaultComboBoxModel df_maphieu = new DefaultComboBoxModel();
		xuathangDAO xhd_maphieu = new xuathangDAO();
		for(Xuathang xh: xhd_maphieu.findAll_XuatHang())
		{
			df_maphieu.addElement(xh.getXuatHangId());
			
		}
		cbbMaPhieu.setModel(df_maphieu);
		
		JButton btnThmHngHa = new JButton("Thêm Hàng Hóa");
		btnThmHngHa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PhieuXuatHang_Them phnt= new PhieuXuatHang_Them();
				phnt.setVisible(true);
			}
		});
		panel_2.add(btnThmHngHa);
		table = new JTable();
		
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.gridheight = 2;
		gbc_table.insets = new Insets(0, 0, 5, 0);
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridx = 0;
		gbc_table.gridy = 3;
		
		JScrollPane scrollPane = new JScrollPane(table);
		contentPane.add(scrollPane, gbc_table);
		//datePicker.setColumns(10);
		
		
	}
}
