package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.border.EmptyBorder;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import GUI.PhieuNhapHang.DateLabelFormatter;
import controller.cnThongKe_NhapHang;
import controller.cnThongKe_XuatHang;
import controller.dmMuaHang_NhapPhieu;

import java.awt.GridBagLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ThongKe_XuatHang extends JInternalFrame {

	private JPanel contentPane;
	public  JDatePickerImpl ngay_tu= ngaythang();
	public  JDatePickerImpl ngay_den= ngaythang();
	public static JTextField textField;
	public static JTextField textField_1;
	public static JTable table;
	public static Date selectedDate_ngay_tu;
	public static Date selectedDate_ngay_den;
	public static JTable table_chitietnhaphang;
	public static String machungtu;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ThongKe_XuatHang frame = new ThongKe_XuatHang();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public  JDatePickerImpl ngaythang()
	{
		UtilDateModel model1 = new UtilDateModel();
		Properties p1 = new Properties();
		JDatePanelImpl datePanel = new JDatePanelImpl(model1, p1);
		JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		return datePicker;
	}

	class DateLabelFormatter  extends AbstractFormatter {

        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String datePattern = "yyyy-MM-dd";
        private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

        @Override
        public Object stringToValue(String text) throws ParseException {
            return dateFormatter.parseObject(text);
        }

        @Override
        public String valueToString(Object value) throws ParseException {
            if (value != null) {
                Calendar cal = (Calendar) value;
                return dateFormatter.format(cal.getTime());
            }

            return "";
        }

    }
	
	/**
	 * Create the frame.
	 */
	public ThongKe_XuatHang() {
		setTitle("Bảng Kê Xuất Hàng");
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setClosable(true);
		setMaximizable(true);
		setBounds(100, 100, 600, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblTu = new JLabel("Từ");
		GridBagConstraints gbc_lblTu = new GridBagConstraints();
		gbc_lblTu.insets = new Insets(0, 0, 5, 5);
		gbc_lblTu.anchor = GridBagConstraints.EAST;
		gbc_lblTu.gridx = 0;
		gbc_lblTu.gridy = 0;
		panel.add(lblTu, gbc_lblTu);
		
		//textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.anchor = GridBagConstraints.WEST;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 0;
		panel.add(ngay_tu, gbc_textField);
		//textField.setColumns(10);
		
		JLabel lblDen = new JLabel("Đến");
		GridBagConstraints gbc_lblDen = new GridBagConstraints();
		gbc_lblDen.anchor = GridBagConstraints.EAST;
		gbc_lblDen.insets = new Insets(0, 0, 5, 5);
		gbc_lblDen.gridx = 2;
		gbc_lblDen.gridy = 0;
		panel.add(lblDen, gbc_lblDen);
		
		//textField_1 = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 0);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 3;
		gbc_textField_1.gridy = 0;
		panel.add(ngay_den, gbc_textField_1);
		
		JButton btnXem = new JButton("Xem");
		btnXem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				selectedDate_ngay_tu = (Date) ngay_tu.getModel().getValue();
				selectedDate_ngay_den = (Date) ngay_den.getModel().getValue();
				cnThongKe_XuatHang.load_data_chitiet();
			}
		});
		GridBagConstraints gbc_btnXem = new GridBagConstraints();
		gbc_btnXem.insets = new Insets(0, 0, 5, 5);
		gbc_btnXem.gridx = 4;
		gbc_btnXem.gridy = 0;
		panel.add(btnXem, gbc_btnXem);
		
		JButton btnXuat = new JButton("Xuat");
		btnXuat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cnThongKe_XuatHang.xuatfile(null);
			}
		});
		GridBagConstraints gbc_btnXuat = new GridBagConstraints();
		gbc_btnXuat.insets = new Insets(0, 0, 5, 0);
		gbc_btnXuat.gridx = 5;
		gbc_btnXuat.gridy = 0;
		panel.add(btnXuat, gbc_btnXuat);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				 machungtu = table.getValueAt(table.getSelectedRow(), 0).toString();
				cnThongKe_XuatHang.load_data_xuathangchitiet();
				
			}
		});
		
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.insets = new Insets(0, 0, 5, 0);
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridx = 0;
		gbc_table.gridy = 1;
		JScrollPane scrollPane = new JScrollPane(table);
		contentPane.add(scrollPane, gbc_table);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.gridheight = 2;
		gbc_scrollPane_1.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 2;
		contentPane.add(scrollPane_1, gbc_scrollPane_1);
		
		table_chitietnhaphang = new JTable();
		
		scrollPane_1.setViewportView(table_chitietnhaphang);
		//textField_1.setColumns(10);
	}

}
