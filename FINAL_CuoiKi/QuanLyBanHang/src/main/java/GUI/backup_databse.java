package GUI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.backup_restore;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.io.File;
import java.text.SimpleDateFormat;



public class backup_databse extends JInternalFrame {
	public String path = null;
	public String filename ;
	public JPanel contentPane;
	public static JTextField txtpath;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					backup_databse frame = new backup_databse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public backup_databse() {
		setTitle("Backup");
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setClosable(true);
		setMaximizable(true);
		setBounds(100, 100, 450, 125);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblPath = new JLabel("Path:");
		GridBagConstraints gbc_lblPath = new GridBagConstraints();
		gbc_lblPath.insets = new Insets(0, 0, 5, 5);
		gbc_lblPath.anchor = GridBagConstraints.WEST;
		gbc_lblPath.gridx = 0;
		gbc_lblPath.gridy = 0;
		contentPane.add(lblPath, gbc_lblPath);
		
		txtpath = new JTextField();
		GridBagConstraints gbc_txtpath = new GridBagConstraints();
		gbc_txtpath.insets = new Insets(0, 0, 5, 5);
		gbc_txtpath.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtpath.gridx = 1;
		gbc_txtpath.gridy = 0;
		contentPane.add(txtpath, gbc_txtpath);
		txtpath.setColumns(10);
		
		JButton btnNewButton = new JButton("Browse");
		
		
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				 backup_restore.backup_choose();
				
			}
		});
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.gridx = 2;
		gbc_btnNewButton.gridy = 0;
		contentPane.add(btnNewButton, gbc_btnNewButton);
		
		JButton btnBackup = new JButton("Backup");
		btnBackup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				backup_restore.backup();
			}
				
		});
		GridBagConstraints gbc_btnBackup = new GridBagConstraints();
		gbc_btnBackup.insets = new Insets(0, 0, 0, 5);
		gbc_btnBackup.gridx = 0;
		gbc_btnBackup.gridy = 1;
		contentPane.add(btnBackup, gbc_btnBackup);
	}

}
