package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class banhang_dialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			banhang_dialog dialog = new banhang_dialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public banhang_dialog() {
		setTitle("Bán Hàng");
		setBounds(100, 100, 450, 96);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JButton btnPhiuXutHng = new JButton("Phiếu Xuất Hàng");
			btnPhiuXutHng.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					PhieuXuatHang pxh= new PhieuXuatHang();
					Main.desktopPane.add(pxh);
					pxh.show();
					dispose();
				}
			});
			contentPanel.add(btnPhiuXutHng);
		}
		{
			JButton btnThngKXut = new JButton("Thống Kê Xuất Hàng");
			btnThngKXut.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					ThongKe_XuatHang tkxh= new ThongKe_XuatHang();
					Main.desktopPane.add(tkxh);
					tkxh.show();
					dispose();
					
				}
			});
			contentPanel.add(btnThngKXut);
		}
	}

}
