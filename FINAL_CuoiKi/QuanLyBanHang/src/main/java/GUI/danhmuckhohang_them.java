package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dao.khohangDAO;
import dao.nhanvienDAO;
import pojo.Khohang;
import pojo.Nhanvien;
import GUI.KhoHang;
import controller.dmKhoHang_controller;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class danhmuckhohang_them extends JDialog {
	public static  JComboBox cbbNVQL = new JComboBox();
	
	private final JPanel contentPanel = new JPanel();
	public static JTextField txtMa;
	public static JTextField txtTen;
	public static JTextField txtDC;
	public static JTextField txtDT;
	public static JTextField txtEmail;
	public static JTextField txtGhiChu;
	public static JTextField txtFax;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			danhmuckhohang_them dialog = new danhmuckhohang_them();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
			
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void loadData()
	{
		DefaultComboBoxModel dcm = new DefaultComboBoxModel();
		nhanvienDAO nvd = new nhanvienDAO();
		for(Nhanvien nv : nvd.findAll())
		{
			
			
			dcm.addElement(nv.getTenNv());
		}
		this.cbbNVQL.setModel(dcm);
	}

	/**
	 * Create the dialog.
	 */
	public danhmuckhohang_them() {
		loadData();
		setBounds(100, 100, 369, 319);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblThngTinBc = new JLabel("Thông Tin Bắc Buộc");
			GridBagConstraints gbc_lblThngTinBc = new GridBagConstraints();
			gbc_lblThngTinBc.insets = new Insets(0, 0, 5, 5);
			gbc_lblThngTinBc.gridx = 0;
			gbc_lblThngTinBc.gridy = 0;
			contentPanel.add(lblThngTinBc, gbc_lblThngTinBc);
		}
		{
			JLabel lblM = new JLabel("Mã");
			GridBagConstraints gbc_lblM = new GridBagConstraints();
			gbc_lblM.anchor = GridBagConstraints.EAST;
			gbc_lblM.insets = new Insets(0, 0, 5, 5);
			gbc_lblM.gridx = 0;
			gbc_lblM.gridy = 1;
			contentPanel.add(lblM, gbc_lblM);
		}
		{
			txtMa = new JTextField();
			GridBagConstraints gbc_txtMa = new GridBagConstraints();
			gbc_txtMa.insets = new Insets(0, 0, 5, 0);
			gbc_txtMa.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtMa.gridx = 1;
			gbc_txtMa.gridy = 1;
			contentPanel.add(txtMa, gbc_txtMa);
			txtMa.setColumns(10);
		}
		{
			JLabel lblTn = new JLabel("Tên");
			GridBagConstraints gbc_lblTn = new GridBagConstraints();
			gbc_lblTn.anchor = GridBagConstraints.EAST;
			gbc_lblTn.insets = new Insets(0, 0, 5, 5);
			gbc_lblTn.gridx = 0;
			gbc_lblTn.gridy = 2;
			contentPanel.add(lblTn, gbc_lblTn);
		}
		{
			txtTen = new JTextField();
			GridBagConstraints gbc_txtTen = new GridBagConstraints();
			gbc_txtTen.insets = new Insets(0, 0, 5, 0);
			gbc_txtTen.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtTen.gridx = 1;
			gbc_txtTen.gridy = 2;
			contentPanel.add(txtTen, gbc_txtTen);
			txtTen.setColumns(10);
		}
		{
			JLabel lblNgiQunL = new JLabel("Người Quản Lý");
			GridBagConstraints gbc_lblNgiQunL = new GridBagConstraints();
			gbc_lblNgiQunL.anchor = GridBagConstraints.EAST;
			gbc_lblNgiQunL.insets = new Insets(0, 0, 5, 5);
			gbc_lblNgiQunL.gridx = 0;
			gbc_lblNgiQunL.gridy = 3;
			contentPanel.add(lblNgiQunL, gbc_lblNgiQunL);
		}
		{
			
			GridBagConstraints gbc_cbbNVQL = new GridBagConstraints();
			gbc_cbbNVQL.insets = new Insets(0, 0, 5, 0);
			gbc_cbbNVQL.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbbNVQL.gridx = 1;
			gbc_cbbNVQL.gridy = 3;
			contentPanel.add(cbbNVQL, gbc_cbbNVQL);
		}
		{
			JLabel lblThngTinKhc = new JLabel("Thông Tin Khác");
			GridBagConstraints gbc_lblThngTinKhc = new GridBagConstraints();
			gbc_lblThngTinKhc.insets = new Insets(0, 0, 5, 5);
			gbc_lblThngTinKhc.gridx = 0;
			gbc_lblThngTinKhc.gridy = 4;
			contentPanel.add(lblThngTinKhc, gbc_lblThngTinKhc);
		}
		{
			JLabel lblaCh = new JLabel("Địa chỉ");
			GridBagConstraints gbc_lblaCh = new GridBagConstraints();
			gbc_lblaCh.anchor = GridBagConstraints.EAST;
			gbc_lblaCh.insets = new Insets(0, 0, 5, 5);
			gbc_lblaCh.gridx = 0;
			gbc_lblaCh.gridy = 5;
			contentPanel.add(lblaCh, gbc_lblaCh);
		}
		{
			txtDC = new JTextField();
			GridBagConstraints gbc_txtDC = new GridBagConstraints();
			gbc_txtDC.insets = new Insets(0, 0, 5, 0);
			gbc_txtDC.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtDC.gridx = 1;
			gbc_txtDC.gridy = 5;
			contentPanel.add(txtDC, gbc_txtDC);
			txtDC.setColumns(10);
		}
		{
			JLabel lblinThoi = new JLabel("Điện Thoại");
			GridBagConstraints gbc_lblinThoi = new GridBagConstraints();
			gbc_lblinThoi.anchor = GridBagConstraints.EAST;
			gbc_lblinThoi.insets = new Insets(0, 0, 5, 5);
			gbc_lblinThoi.gridx = 0;
			gbc_lblinThoi.gridy = 6;
			contentPanel.add(lblinThoi, gbc_lblinThoi);
		}
		{
			txtDT = new JTextField();
			GridBagConstraints gbc_txtDT = new GridBagConstraints();
			gbc_txtDT.insets = new Insets(0, 0, 5, 0);
			gbc_txtDT.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtDT.gridx = 1;
			gbc_txtDT.gridy = 6;
			contentPanel.add(txtDT, gbc_txtDT);
			txtDT.setColumns(10);
		}
		{
			JLabel lblFax = new JLabel("Fax");
			GridBagConstraints gbc_lblFax = new GridBagConstraints();
			gbc_lblFax.anchor = GridBagConstraints.EAST;
			gbc_lblFax.insets = new Insets(0, 0, 5, 5);
			gbc_lblFax.gridx = 0;
			gbc_lblFax.gridy = 7;
			contentPanel.add(lblFax, gbc_lblFax);
		}
		{
			txtFax = new JTextField();
			GridBagConstraints gbc_txtFax = new GridBagConstraints();
			gbc_txtFax.anchor = GridBagConstraints.NORTH;
			gbc_txtFax.insets = new Insets(0, 0, 5, 0);
			gbc_txtFax.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtFax.gridx = 1;
			gbc_txtFax.gridy = 7;
			contentPanel.add(txtFax, gbc_txtFax);
			txtFax.setColumns(10);
		}
		{
			JLabel lblEmail = new JLabel("Email");
			GridBagConstraints gbc_lblEmail = new GridBagConstraints();
			gbc_lblEmail.anchor = GridBagConstraints.EAST;
			gbc_lblEmail.insets = new Insets(0, 0, 5, 5);
			gbc_lblEmail.gridx = 0;
			gbc_lblEmail.gridy = 8;
			contentPanel.add(lblEmail, gbc_lblEmail);
		}
		{
			txtEmail = new JTextField();
			GridBagConstraints gbc_txtEmail = new GridBagConstraints();
			gbc_txtEmail.insets = new Insets(0, 0, 5, 0);
			gbc_txtEmail.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtEmail.gridx = 1;
			gbc_txtEmail.gridy = 8;
			contentPanel.add(txtEmail, gbc_txtEmail);
			txtEmail.setColumns(10);
		}
		{
			JLabel lblGhiCh = new JLabel("Ghi Chú");
			GridBagConstraints gbc_lblGhiCh = new GridBagConstraints();
			gbc_lblGhiCh.anchor = GridBagConstraints.EAST;
			gbc_lblGhiCh.insets = new Insets(0, 0, 0, 5);
			gbc_lblGhiCh.gridx = 0;
			gbc_lblGhiCh.gridy = 9;
			contentPanel.add(lblGhiCh, gbc_lblGhiCh);
		}
		{
			txtGhiChu = new JTextField();
			GridBagConstraints gbc_txtGhiChu = new GridBagConstraints();
			gbc_txtGhiChu.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtGhiChu.gridx = 1;
			gbc_txtGhiChu.gridy = 9;
			contentPanel.add(txtGhiChu, gbc_txtGhiChu);
			txtGhiChu.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						dmKhoHang_controller.themKhoHang();
//												
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
