package GUI;

import java.awt.BorderLayout;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import dao.*;
import pojo.*;
import controller.*;
public class dialogThemBoPhan extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private final JTextField textField_Ma;
	private JTextField textField_Ten;
	private JTextField textField_GhiChu;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			dialogThemBoPhan dialog = new dialogThemBoPhan();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public dialogThemBoPhan() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JPanel panel = new JPanel();
			panel.setBackground(Color.LIGHT_GRAY);
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.anchor = GridBagConstraints.NORTH;
			gbc_panel.fill = GridBagConstraints.HORIZONTAL;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 0;
			contentPanel.add(panel, gbc_panel);
			{
				JLabel lblNewLabel = new JLabel("Thông Tin");
				panel.add(lblNewLabel);
			}
		}
		{
			JPanel panelthem = new JPanel();
			panelthem.setBackground(Color.GRAY);
			GridBagConstraints gbc_panelthem = new GridBagConstraints();
			gbc_panelthem.fill = GridBagConstraints.BOTH;
			gbc_panelthem.gridx = 0;
			gbc_panelthem.gridy = 1;
			contentPanel.add(panelthem, gbc_panelthem);
			GridBagLayout gbl_panelthem = new GridBagLayout();
			gbl_panelthem.columnWidths = new int[]{0, 0, 0, 0, 0};
			gbl_panelthem.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
			gbl_panelthem.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
			gbl_panelthem.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelthem.setLayout(gbl_panelthem);
			{
				JLabel lblNewLabel_1 = new JLabel("Mã :");
				GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
				gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_1.gridx = 1;
				gbc_lblNewLabel_1.gridy = 1;
				panelthem.add(lblNewLabel_1, gbc_lblNewLabel_1);
			}
			{
				textField_Ma = new JTextField();
				GridBagConstraints gbc_textField_Ma = new GridBagConstraints();
				gbc_textField_Ma.fill = GridBagConstraints.HORIZONTAL;
				gbc_textField_Ma.anchor = GridBagConstraints.NORTH;
				gbc_textField_Ma.insets = new Insets(0, 0, 5, 0);
				gbc_textField_Ma.gridx = 3;
				gbc_textField_Ma.gridy = 1;
				panelthem.add(textField_Ma, gbc_textField_Ma);
				textField_Ma.setColumns(10);
			}
			{
				JLabel lblNewLabel_2 = new JLabel("Tên :");
				GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
				gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_2.gridx = 1;
				gbc_lblNewLabel_2.gridy = 3;
				panelthem.add(lblNewLabel_2, gbc_lblNewLabel_2);
			}
			{
				textField_Ten = new JTextField();
				textField_Ten.setColumns(10);
				GridBagConstraints gbc_textField_Ten = new GridBagConstraints();
				gbc_textField_Ten.insets = new Insets(0, 0, 5, 0);
				gbc_textField_Ten.fill = GridBagConstraints.HORIZONTAL;
				gbc_textField_Ten.gridx = 3;
				gbc_textField_Ten.gridy = 3;
				panelthem.add(textField_Ten, gbc_textField_Ten);
			}
			{
				JLabel lblNewLabel_3 = new JLabel("Ghi Chú:");
				GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
				gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_3.gridx = 1;
				gbc_lblNewLabel_3.gridy = 5;
				panelthem.add(lblNewLabel_3, gbc_lblNewLabel_3);
			}
			{
				textField_GhiChu = new JTextField();
				textField_GhiChu.setColumns(10);
				GridBagConstraints gbc_textField_GhiChu = new GridBagConstraints();
				gbc_textField_GhiChu.insets = new Insets(0, 0, 5, 0);
				gbc_textField_GhiChu.fill = GridBagConstraints.HORIZONTAL;
				gbc_textField_GhiChu.gridx = 3;
				gbc_textField_GhiChu.gridy = 5;
				panelthem.add(textField_GhiChu, gbc_textField_GhiChu);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Lưu");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ThemBoPhan_controller thembophan = new ThemBoPhan_controller();
						thembophan.themBoPhan(textField_Ma, textField_Ten, textField_GhiChu);
//						nhomhangDAO nhomhangDao =new nhomhangDAO();
//						Nhomhang pojoNhomhang = new Nhomhang();
//						pojoNhomhang.setNhomHangId(textField_Ma.getText());
//						pojoNhomhang.setTenNhomHang(textField_Ten.getText());
//						pojoNhomhang.setGhiChu(textField_GhiChu.getText());
//						if(nhomhangDao.saveorupdate(pojoNhomhang)){
//							JOptionPane.showMessageDialog(null, "Thêm nhóm hàng thành công");
//							NhomHang_controller.LoadData();
//						}
//						else{
//							JOptionPane.showMessageDialog(null, "Thêm nhóm hàng thất bại");
//
//						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
