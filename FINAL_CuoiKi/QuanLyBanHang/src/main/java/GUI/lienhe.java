package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import java.awt.Color;

public class lienhe extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtTNgcXun;
	private JTextField txtKpPtan;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			lienhe dialog = new lienhe();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setUndecorated(true);
			dialog.setVisible(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public lienhe() {
		getContentPane().setBackground(Color.DARK_GRAY);
		setBounds(100, 100, 450, 277);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();
			}
		});
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("HinhAnh\\baner.jpg"));
		panel.add(lblNewLabel);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		
		JLabel lblThngTin = new JLabel("THÔNG TIN");
		
		GridBagConstraints gbc_lblThngTin = new GridBagConstraints();
		gbc_lblThngTin.gridwidth = 2;
		gbc_lblThngTin.insets = new Insets(0, 0, 5, 5);
		gbc_lblThngTin.gridx = 0;
		gbc_lblThngTin.gridy = 0;
		contentPanel.add(lblThngTin, gbc_lblThngTin);
		
		JLabel lblTnCngTy = new JLabel("Tên Công Ty");
		GridBagConstraints gbc_lblTnCngTy = new GridBagConstraints();
		gbc_lblTnCngTy.anchor = GridBagConstraints.WEST;
		gbc_lblTnCngTy.insets = new Insets(0, 0, 5, 5);
		gbc_lblTnCngTy.gridx = 0;
		gbc_lblTnCngTy.gridy = 1;
		contentPanel.add(lblTnCngTy, gbc_lblTnCngTy);
		
		txtTNgcXun = new JTextField();
		txtTNgcXun.setText("TỪ NGỌC XUÂN THÁI");
		GridBagConstraints gbc_txtTNgcXun = new GridBagConstraints();
		gbc_txtTNgcXun.insets = new Insets(0, 0, 5, 0);
		gbc_txtTNgcXun.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtTNgcXun.gridx = 1;
		gbc_txtTNgcXun.gridy = 1;
		contentPanel.add(txtTNgcXun, gbc_txtTNgcXun);
		txtTNgcXun.setColumns(10);
		
		JLabel lblaCh = new JLabel("Địa chỉ");
		GridBagConstraints gbc_lblaCh = new GridBagConstraints();
		gbc_lblaCh.anchor = GridBagConstraints.WEST;
		gbc_lblaCh.insets = new Insets(0, 0, 5, 5);
		gbc_lblaCh.gridx = 0;
		gbc_lblaCh.gridy = 2;
		contentPanel.add(lblaCh, gbc_lblaCh);
		
		txtKpPtan = new JTextField();
		txtKpPtan.setText("60/31 KP3 P.TAN CHANH HIEP Q.12");
		GridBagConstraints gbc_txtKpPtan = new GridBagConstraints();
		gbc_txtKpPtan.insets = new Insets(0, 0, 5, 0);
		gbc_txtKpPtan.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtKpPtan.gridx = 1;
		gbc_txtKpPtan.gridy = 2;
		contentPanel.add(txtKpPtan, gbc_txtKpPtan);
		txtKpPtan.setColumns(10);
		
		JLabel lblinThoi = new JLabel("Điện Thoại");
		GridBagConstraints gbc_lblinThoi = new GridBagConstraints();
		gbc_lblinThoi.anchor = GridBagConstraints.WEST;
		gbc_lblinThoi.insets = new Insets(0, 0, 5, 5);
		gbc_lblinThoi.gridx = 0;
		gbc_lblinThoi.gridy = 3;
		contentPanel.add(lblinThoi, gbc_lblinThoi);
		
		textField = new JTextField();
		textField.setText("0972850815");
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 3;
		contentPanel.add(textField, gbc_textField);
		textField.setColumns(10);
		JLabel imgLabel = new JLabel(new ImageIcon("HinhAnh\\sign-question-icon.png"));
	}

}
