package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class muahang_dialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			muahang_dialog dialog = new muahang_dialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public muahang_dialog() {
		setTitle("Nhập Hàng");
		setBounds(100, 100, 450, 87);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JButton btnPhiuNhpHng = new JButton("Phiếu Nhập Hàng");
			btnPhiuNhpHng.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					PhieuNhapHang pnh = new PhieuNhapHang();
					Main.desktopPane.add(pnh);
					pnh.show();
					dispose();
				}
			});
			contentPanel.add(btnPhiuNhpHng);
		}
		{
			JButton btnThngKeNhp = new JButton("Thống Ke Nhập Hàng");
			btnThngKeNhp.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ThongKe_NhapHang tknh= new ThongKe_NhapHang();
					Main.desktopPane.add(tknh);
					tknh.show();
					dispose();
				}
			});
			contentPanel.add(btnThngKeNhp);
		}
	}

}
