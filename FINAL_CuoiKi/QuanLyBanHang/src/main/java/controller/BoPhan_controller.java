package controller;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import dao.*;
import pojo.Bophan;
import pojo.Nhomhang;
import GUI.BoPhan;
import GUI.NhomHang;
public class BoPhan_controller {
	public static void LoadData(){
		 bophanDAO bophanDAO = new bophanDAO();
		DefaultTableModel dtm= new DefaultTableModel();
		dtm.addColumn("Mã");
		dtm.addColumn("Tên");
		dtm.addColumn("Ghi Chú");
		
		for(Bophan bophan:bophanDAO.findAll()){
			dtm.addRow(new Object[]{bophan.getBoPhanId(),bophan.getTenBoPhan(),bophan.getMoTa()} );
		}
		BoPhan.jtablenhomhang.setModel(dtm);
	}
	
	public void suaBoPhan(JTextField id ,JTextField ten ,JTextField chuthich ) {
		bophanDAO bophanDao =new bophanDAO();
		Bophan pojoBophan = new Bophan();
		pojoBophan =bophanDao.find(id.getText().toString());
		pojoBophan.setBoPhanId(id.getText());
		pojoBophan.setTenBoPhan(ten.getText());
		pojoBophan.setMoTa(chuthich.getText());
		if(bophanDao.saveorupdate(pojoBophan)){
			JOptionPane.showMessageDialog(null, "update bộ phận thành công");
			LoadData();
		}
		else{
			JOptionPane.showMessageDialog(null, "update bộ phận thất bại");

		}
	}
	
}
