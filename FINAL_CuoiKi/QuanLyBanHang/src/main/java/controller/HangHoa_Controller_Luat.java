package controller;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import GUI.ManHinhHangHoa_Luat;
import dao.HangHoaDao_Luat;
import pojo.Sanpham;

public class HangHoa_Controller_Luat 
{
	private static List<Sanpham> dsSanPham=HangHoaDao_Luat.LayDanhSachHangHoa();
	public static void LoadDanhSachSanPham()
	{
		DefaultTableModel dftm=new DefaultTableModel();		
		dftm.addColumn("Mã Hàng");
		dftm.addColumn("Tên Hàng");
		dftm.addColumn("Đơn Vị");
		dftm.addColumn("Giá Mua");
		dftm.addColumn("Giá Bán Sỉ");
		dftm.addColumn("Giá Bán Lẻ");
		dftm.addColumn("Tối Thiểu");
		dftm.addColumn("Tính Chất");
		dftm.addColumn("Kho Mặc Định");
		dftm.addColumn("Còn Quản Lý");
		for(int i=0;i<dsSanPham.size();i++)
		{
			Sanpham sp=dsSanPham.get(i);
			dftm.addRow(new Object[]{sp.getSanPhamId(),sp.getTenSp()});			
		}
		ManHinhHangHoa_Luat.table.setModel(dftm);
	}
}
