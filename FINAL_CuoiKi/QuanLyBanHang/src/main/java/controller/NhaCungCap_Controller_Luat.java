package controller;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import GUI.ManHinhNhaCungCap_Luat;
import dao.KhuVucDao_Luat;
import dao.NhaCungCapDao_Luat;
import dao.NhanVienDao_Luat;
import pojo.Khuvuc;
import pojo.Nhacungcap;
import GUI.DialogSuaNhaCungCap_Luat;
import GUI.DialogThemNhaCungCap_Luat;

public class NhaCungCap_Controller_Luat 
{
	private static String KhuVuc=null;
	private static List<Khuvuc> ds=KhuVucDao_Luat.LayDanhSachKhuVuc();
	public static void Load()
	{
		Khuvuc kv = null;
		DefaultComboBoxModel df=new DefaultComboBoxModel();
		
		df.addElement("Tất cả khu vực");
		for(int i=0;i<ds.size();i++)
		{
			kv=ds.get(i);
			df.addElement(kv.getTenKhuVuc());			
		}
		ManHinhNhaCungCap_Luat.comboBox.setModel(df);
		KhuVuc=ds.get(0).getKhuVucId();
	}
	public static void LoadKhuVuc_Dialog()
	{
		List<Khuvuc> ds=KhuVucDao_Luat.LayDanhSachKhuVuc();
		Khuvuc kv = null;
		DefaultComboBoxModel df2=new DefaultComboBoxModel();
		
		for(int i=0;i<ds.size();i++)
		{
			kv=ds.get(i);
			df2.addElement(kv.getTenKhuVuc());			
		}
		DialogThemNhaCungCap_Luat.cbbKhuVuc.setModel(df2);

		DialogThemNhaCungCap_Luat.txtMa.setText(TuPhatSinhMaNCC());
	}
	public static void LoadRowTable()
	{
		DefaultTableModel dt =new DefaultTableModel();
		List<Nhacungcap> dsncc=NhaCungCapDao_Luat.LayDanhSachNhaCungCap1();
		dt.addColumn("Mã Số");
		dt.addColumn("Tên Nhà Cung Cấp");
		dt.addColumn("Người Liên Hệ");
		dt.addColumn("Chức Vụ");
		dt.addColumn("Địa Chỉ");
		dt.addColumn("Điện Thoại");
		dt.addColumn("Di Động");
		dt.addColumn("Fax");
		dt.addColumn("Còn Quản Lý");
		for(int i=0;i<dsncc.size();i++)
		{
			Nhacungcap ncc=dsncc.get(i);
			dt.addRow(new Object[]{ ncc.getMaNcc(),ncc.getTenNcc(),ncc.getNguoiLienHe(),ncc.getChucVu(),ncc.getDiaChi(),ncc.getDt(),ncc.getDiDong(),ncc.getFax()});
		}
		ManHinhNhaCungCap_Luat.table.setModel(dt);
	}
	public static void ReLoadTable(int i)
	{
		if(i>0)
		{
			DefaultTableModel dt =new DefaultTableModel();
			String maKhuVuc;		
			maKhuVuc=ds.get(i-1).getKhuVucId();
			List<Nhacungcap> dsncc=NhaCungCapDao_Luat.LayDanhSachNhaCungCap(maKhuVuc);
			dt.addColumn("Mã Số");
			dt.addColumn("Tên Nhà Cung Cấp");
			dt.addColumn("Người Liên Hệ");
			dt.addColumn("Chức Vụ");
			dt.addColumn("Địa Chỉ");
			dt.addColumn("Điện Thoại");
			dt.addColumn("Di Động");
			dt.addColumn("Fax");
			dt.addColumn("Còn Quản Lý");
			for(int i1=0;i1<dsncc.size();i1++)
			{
				Nhacungcap ncc=dsncc.get(i1);
				dt.addRow(new Object[]{ ncc.getMaNcc(),ncc.getTenNcc(),ncc.getNguoiLienHe(),ncc.getChucVu(),ncc.getDiaChi(),ncc.getDt(),ncc.getDiDong(),ncc.getFax()});
			}
			ManHinhNhaCungCap_Luat.table.setModel(dt);
			ManHinhNhaCungCap_Luat.table.repaint();
			ManHinhNhaCungCap_Luat.table.revalidate();
			//ManHinhNhaCungCap_Luat.comboBox.setSelectedIndex(i);
		}
		else
			LoadRowTable();
	}
	public static void ThemNhaCungCap()
	{
		Nhacungcap ncc=new Nhacungcap();
		NhaCungCapDao_Luat nccd=new NhaCungCapDao_Luat();
		
		ncc.setMaNcc(DialogThemNhaCungCap_Luat.txtMa.getText());
		ncc.setTenNcc(DialogThemNhaCungCap_Luat.txtTen.getText());
		ncc.setDiaChi(DialogThemNhaCungCap_Luat.txtDiaChi.getText());
		ncc.setMaSoThue(DialogThemNhaCungCap_Luat.txtMaSoThue.getText());
		ncc.setKhuVucId(LayMaKhuVuc());
		///////////////////////////////////////////////////////////////////////////////
		try
		{
			ncc.setDt(Integer.parseInt(DialogThemNhaCungCap_Luat.txtDienThoai.getText()));
		}catch(Exception ex){}
		
		try
		{
			ncc.setFax(Integer.parseInt(DialogThemNhaCungCap_Luat.txtFax.getText()));
		}catch(Exception ex){}
		try
		{
			ncc.setFax(Integer.parseInt(DialogThemNhaCungCap_Luat.txtMobile.getText()));
		}catch(Exception ex){}
		
		ncc.setWebSite(DialogThemNhaCungCap_Luat.txtWebsite.getText());
		ncc.setTaiKhoan(DialogThemNhaCungCap_Luat.txtTaiKhoan.getText());
		ncc.setNganHang(DialogThemNhaCungCap_Luat.txtNganHang.getText());
		ncc.setEmail(DialogThemNhaCungCap_Luat.txtEmail.getText());
		///////////////////////////////////////////////////////////////////////////////
		BigDecimal i = null;
		ncc.setGioiHanNo(i);
		ncc.setNoHienTai(i);
		ncc.setChietKhau(i);
		///////////////////////////////////////////////////////////////////////////////
		if (nccd.ThemNhaCungCap(ncc))
		{
			JOptionPane.showConfirmDialog(null, "Thêm Thành Công.","",JOptionPane.CLOSED_OPTION);
			ReLoadTable(DialogThemNhaCungCap_Luat.cbbKhuVuc.getSelectedIndex()+1);
			ManHinhNhaCungCap_Luat.comboBox.setSelectedIndex(DialogThemNhaCungCap_Luat.cbbKhuVuc.getSelectedIndex()+1);
		}
		else
		{
			JOptionPane.showConfirmDialog(null, "Thêm Thất Bại!","",JOptionPane.CLOSED_OPTION);
		}		
	}
	public static void ThaoTacThem_KhiBamThem()
	{
		if(isThemOk())
		{
			if(!isMaNCCTonTai(DialogThemNhaCungCap_Luat.txtMa.getText()))
			{
				ThemNhaCungCap();
				XoaTextFiel();
				DialogThemNhaCungCap_Luat.txtMa.setText(TuPhatSinhMaNCC());
			}
			else
				JOptionPane.showConfirmDialog(null, "Mã NCC đã tồn tại!","",JOptionPane.CLOSED_OPTION);
		}		
	}
	public static String LayMaKhuVuc()
	{
		int i=DialogThemNhaCungCap_Luat.cbbKhuVuc.getSelectedIndex();		
		return ds.get(i).getKhuVucId();
	}
	public static boolean isThemOk()
	{
		if(DialogThemNhaCungCap_Luat.txtMa.getText().isEmpty()||
				DialogThemNhaCungCap_Luat.txtTen.getText().isEmpty())
		{
			JOptionPane.showConfirmDialog(null, "Nhập thiếu thông tin quan trọng.","",JOptionPane.CLOSED_OPTION);
			return false;
		}		
		return true;
	}
	public static void XoaTextFiel()
	{
		DialogThemNhaCungCap_Luat.txtMa.setText(null);
		DialogThemNhaCungCap_Luat.txtNguoiLienHe.setText(null);
		DialogThemNhaCungCap_Luat.txtChietKhau.setText(null);
		DialogThemNhaCungCap_Luat.txtGioiHanNo.setText(null);
		DialogThemNhaCungCap_Luat.txtTaiKhoan.setText(null);
		DialogThemNhaCungCap_Luat.txtEmail.setText(null);
		DialogThemNhaCungCap_Luat.txtDienThoai.setText(null);
		DialogThemNhaCungCap_Luat.txtMaSoThue.setText(null);
		DialogThemNhaCungCap_Luat.txtDiaChi.setText(null);
		DialogThemNhaCungCap_Luat.txtTen.setText(null);
		DialogThemNhaCungCap_Luat.txtNoHienTai.setText(null);
		DialogThemNhaCungCap_Luat.txtChucVu.setText(null);
		DialogThemNhaCungCap_Luat.txtNganHang.setText(null);
		DialogThemNhaCungCap_Luat.txtWebsite.setText(null);
		DialogThemNhaCungCap_Luat.txtMobile.setText(null);
		DialogThemNhaCungCap_Luat.txtFax.setText(null);
	}
	public static Nhacungcap LoadThongTinNhaCungCap()
	{
		String MaNhaCungCap=ManHinhNhaCungCap_Luat.table.getValueAt(ManHinhNhaCungCap_Luat.table.getSelectedRow(), 0).toString();
		Nhacungcap ncc = NhaCungCapDao_Luat.LayThongTinNhaCungCap(MaNhaCungCap);
		if(ncc==null)
		{
			JOptionPane.showConfirmDialog(null, "Load Nha Cung Cap That Bai.","",JOptionPane.CLOSED_OPTION);
		}
		else
		{
			DialogSuaNhaCungCap_Luat.txtMa.setText(ncc.getMaNcc());
			DialogSuaNhaCungCap_Luat.txtMa.setEditable(false);
			
			DialogSuaNhaCungCap_Luat.txtNguoiLienHe.setText(ncc.getNguoiLienHe());			
			DialogSuaNhaCungCap_Luat.txtTaiKhoan.setText(ncc.getTaiKhoan());
			DialogSuaNhaCungCap_Luat.txtEmail.setText(ncc.getEmail());			
			DialogSuaNhaCungCap_Luat.txtMaSoThue.setText(ncc.getMaSoThue());
			DialogSuaNhaCungCap_Luat.txtDiaChi.setText(ncc.getDiaChi());
			DialogSuaNhaCungCap_Luat.txtTen.setText(ncc.getTenNcc());			
			DialogSuaNhaCungCap_Luat.txtChucVu.setText(ncc.getChucVu());
			DialogSuaNhaCungCap_Luat.txtNganHang.setText(ncc.getNganHang());
			DialogSuaNhaCungCap_Luat.txtWebsite.setText(ncc.getWebSite());
			
			try
			{
				DialogSuaNhaCungCap_Luat.txtMobile.setText(ncc.getDiDong().toString());
			}catch(Exception ex){}
			try
			{
				DialogSuaNhaCungCap_Luat.txtFax.setText(ncc.getFax().toString());
			}catch(Exception ex){}
			try
			{
				DialogSuaNhaCungCap_Luat.txtNoHienTai.setText(ncc.getNoHienTai().toString());
			}catch(Exception ex){}
			try
			{
				DialogSuaNhaCungCap_Luat.txtDienThoai.setText(ncc.getDt().toString());
			}catch(Exception ex){}
			try
			{
				DialogSuaNhaCungCap_Luat.txtChietKhau.setText(ncc.getChietKhau().toString());
			}catch(Exception ex){}
			try
			{
				DialogSuaNhaCungCap_Luat.txtGioiHanNo.setText(ncc.getGioiHanNo().toString());
			}catch(Exception ex){}
			
			String maKhuVuc=ncc.getKhuVucId();
			int vitri=-1;
			Khuvuc kv = null;
			DefaultComboBoxModel df2=new DefaultComboBoxModel();			
			for(int i=0;i<ds.size();i++)
			{
				kv=ds.get(i);
				df2.addElement(kv.getTenKhuVuc());
				if(maKhuVuc.compareTo(kv.getKhuVucId())==0)
				{
					vitri=i;
				}
			}
			DialogSuaNhaCungCap_Luat.cbbKhuVuc.setModel(df2);
			if(vitri!=-1)
			{
				DialogSuaNhaCungCap_Luat.cbbKhuVuc.setSelectedIndex(vitri);
			}			
		}
		return ncc;
				
	}
	public static boolean CapNhat(Nhacungcap ncc)
	{
		ncc.setTenNcc(DialogSuaNhaCungCap_Luat.txtTen.getText());
		ncc.setDiaChi(DialogSuaNhaCungCap_Luat.txtDiaChi.getText());
		ncc.setMaSoThue(DialogSuaNhaCungCap_Luat.txtMaSoThue.getText());
		ncc.setKhuVucId(ds.get(DialogSuaNhaCungCap_Luat.cbbKhuVuc.getSelectedIndex()).getKhuVucId());
		///////////////////////////////////////////////////////////////////////////////
		try
		{
			ncc.setDt(Integer.parseInt(DialogSuaNhaCungCap_Luat.txtDienThoai.getText()));
		}catch(Exception ex){}
		try
		{
			ncc.setFax(Integer.parseInt(DialogSuaNhaCungCap_Luat.txtFax.getText()));
		}catch(Exception ex){}
		try
		{
			ncc.setDiDong(Integer.parseInt(DialogSuaNhaCungCap_Luat.txtMobile.getText()));
		}catch(Exception ex){}		
		///////////////////////////////////////////////////////////////////////////////		
		ncc.setWebSite(DialogSuaNhaCungCap_Luat.txtWebsite.getText());
		ncc.setTaiKhoan(DialogSuaNhaCungCap_Luat.txtTaiKhoan.getText());
		ncc.setNganHang(DialogSuaNhaCungCap_Luat.txtNganHang.getText());
		ncc.setEmail(DialogSuaNhaCungCap_Luat.txtEmail.getText());
		///////////////////////////////////////////////////////////////////////////////
		BigDecimal i = null;
		ncc.setGioiHanNo(i);
		ncc.setNoHienTai(i);
		ncc.setChietKhau(i);
		///////////////////////////////////////////////////////////////////////////////
		if (NhaCungCapDao_Luat.CapNhatThongTinNhaCungCap(ncc))
		{
			JOptionPane.showConfirmDialog(null, "Cập Nhật Thành Công.","",JOptionPane.CLOSED_OPTION);
			ReLoadTable(DialogSuaNhaCungCap_Luat.cbbKhuVuc.getSelectedIndex());
			ManHinhNhaCungCap_Luat.comboBox.setSelectedIndex(DialogSuaNhaCungCap_Luat.cbbKhuVuc.getSelectedIndex()+1);
			return true;
		}
		else
		{
			JOptionPane.showConfirmDialog(null, "Cập Nhật Thất Bại!","",JOptionPane.CLOSED_OPTION);
			
		}
		return false;
	}
	public static void ThaoTacXoa()
	{
		String MaNhaCungCap=ManHinhNhaCungCap_Luat.table.getValueAt(ManHinhNhaCungCap_Luat.table.getSelectedRow(), 0).toString();
		try
		{
			if(NhaCungCapDao_Luat.XoaNhaCungCap(MaNhaCungCap) && !isMaNCCTonTai(MaNhaCungCap))
			{
				JOptionPane.showConfirmDialog(null, "Xóa Thành Công.","",JOptionPane.CLOSED_OPTION);
				ReLoadTable(ManHinhNhaCungCap_Luat.comboBox.getSelectedIndex());
				ManHinhNhaCungCap_Luat.btnSuaChua.setEnabled(false);
				ManHinhNhaCungCap_Luat.btnXoa.setEnabled(false);
			}
			else
			{
				JOptionPane.showConfirmDialog(null, "Xóa Thất Bại!","",JOptionPane.CLOSED_OPTION);
			}
		}
		catch(Exception ex)
		{
			
		}
	}
	public static String TuPhatSinhMaNCC()
	{
		List<Nhacungcap> dsncc=NhaCungCapDao_Luat.LayDanhSachNhaCungCap1();
		if(dsncc.size() > 0)
		{
			String MaNCCCuoiCung=dsncc.get(dsncc.size()-1).getMaNcc();
			String MaNCCtrave;
			int so=Integer.parseInt(MaNCCCuoiCung.substring(3));
			do
			{
				so++;
				MaNCCtrave="NCC"+so;
			}
			while(isMaNCCTonTai(MaNCCtrave));
			
			return MaNCCtrave;
		}
		else
		{
			return "NCC1";
		}		
	}
	public static boolean isMaNCCTonTai(String MaNCC)// Kiểm tra nhân viên
	{
		if(NhaCungCapDao_Luat.LayThongTinNhaCungCap(MaNCC)==null)
		{			
			return false;
		}
		return true;
	}
	
		
	
}
