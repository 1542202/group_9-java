package controller;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import GUI.DialogThemNhanVienMoi_Luat;
import GUI.ManHinhNhanVien_Luat;
import dao.NhanVienDao_Luat;
import dao.nhanvienDAO;
import pojo.Bophan;
import pojo.Chucvu;
import pojo.Khuvuc;
import pojo.Nhanvien;
import GUI.DialogSuaThongTinNhanVien_Luat;
import GUI.DialogThemNhaCungCap_Luat;

public class NhanVien_Controller_Luat 
{
	private static List<Nhanvien> dsnv=NhanVienDao_Luat.LayDanhSachNhanVien();
	private static List<Chucvu> dscv=NhanVienDao_Luat.LayDanhSachChucVu();
	private static List<Bophan> dsbp=NhanVienDao_Luat.LayDanhSachBoPhan();
	
	public static void LoadtableNhanVien()
	{		
		DefaultTableModel dt =new DefaultTableModel();
		dt.addColumn("Mã");
		dt.addColumn("Tên");
		dt.addColumn("Địa Chỉ");
		dt.addColumn("Điện Thoại");
		dt.addColumn("Di Động");
		dt.addColumn("Email");
		dt.addColumn("Còn Quản Lý");
		for(int i=0;i<dsnv.size();i++)
		{
			Nhanvien nv=dsnv.get(i);
			dt.addRow(new Object[]{nv.getMaNv(),nv.getTenNv(),nv.getDiaChi(),nv.getPhone(),nv.getDiDong(),nv.getEmail()});
		}
		ManHinhNhanVien_Luat.table.setModel(dt);
	}
	
	//Bắt Đầu hàm Load dialog thêm nhân viên mới
	public static void Load_Dialog_ThemNhanVienMoi()
	{
		// Chức Vụ
		Chucvu cv = null;
		DefaultComboBoxModel defaultcomboboxChucVu=new DefaultComboBoxModel();
		
		for(int i=0;i<dscv.size();i++)
		{
			cv=dscv.get(i);
			defaultcomboboxChucVu.addElement(cv.getTenChucVu());			
		}
		DialogThemNhanVienMoi_Luat.cbcChucVu.setModel(defaultcomboboxChucVu);
		DialogThemNhanVienMoi_Luat.cbcChucVu.setSelectedIndex(-1);
		
		// Bộ Phận
		Bophan bp = null;
		DefaultComboBoxModel defaultcomboboxBoPhan=new DefaultComboBoxModel();
		
		for(int i=0;i<dsbp.size();i++)
		{
			bp=dsbp.get(i);
			defaultcomboboxBoPhan.addElement(bp.getTenBoPhan());			
		}
		DialogThemNhanVienMoi_Luat.cbcBoPhan.setModel(defaultcomboboxBoPhan);
		DialogThemNhanVienMoi_Luat.cbcBoPhan.setSelectedIndex(-1);
		
		// Nhân Viên
		Nhanvien nvql = null;
		DefaultComboBoxModel defaultcomboboxNhanVienQuanLy=new DefaultComboBoxModel();
		
		
		for(int i=0;i<dsnv.size();i++)
		{
			nvql=dsnv.get(i);
			String chuoiNhanVien=nvql.getTenNv() + "  |  " + nvql.getMaNv();
			defaultcomboboxNhanVienQuanLy.addElement(chuoiNhanVien);			
		}
		DialogThemNhanVienMoi_Luat.cbcNVQuanLy.setModel(defaultcomboboxNhanVienQuanLy);
		DialogThemNhanVienMoi_Luat.cbcNVQuanLy.setSelectedIndex(-1);
		
		//Auto Mã nhân viên
		DialogThemNhanVienMoi_Luat.txtMaNV.setText(TuPhatSinhMaNV());
		
	}//Kết thúc hàm Load dialog thêm nhân viên mới
	
	public static boolean isThemOk()// Kiểm tra các test fiel.
	{
		if(DialogThemNhanVienMoi_Luat.txtMaNV.getText().isEmpty()||
			DialogThemNhanVienMoi_Luat.txtTenNV.getText().isEmpty())
		{
			return false;
		}
		return true;
	}
	
	
	public static boolean isMaNhanVienTonTai(String MaNV)// Kiểm tra nhân viên
	{
		if(NhanVienDao_Luat.LayThongTinNhanVien(MaNV)==null)
		{
			return false;
		}
		else
			return true;
	}
	public static void ThemNhanVien()
	{
		Nhanvien nv=new Nhanvien();
		nv.setMaNv(DialogThemNhanVienMoi_Luat.txtMaNV.getText());
		nv.setTenNv(DialogThemNhanVienMoi_Luat.txtTenNV.getText());		
		nv.setDiaChi(DialogThemNhanVienMoi_Luat.txtDiaChi.getText());
		nv.setDiaChi(DialogThemNhanVienMoi_Luat.txtDiaChi.getText());
		nv.setEmail(DialogThemNhanVienMoi_Luat.txtEmail.getText());
		
		if(DialogThemNhanVienMoi_Luat.cbcNVQuanLy.getSelectedIndex()>=0)
		{
			nv.setNhanvien(dsnv.get(DialogThemNhanVienMoi_Luat.cbcNVQuanLy.getSelectedIndex()));
		}
		if(DialogThemNhanVienMoi_Luat.cbcChucVu.getSelectedIndex()>=0)
		{
			nv.setChucvu(dscv.get(DialogThemNhanVienMoi_Luat.cbcChucVu.getSelectedIndex()));
		}
		if(DialogThemNhanVienMoi_Luat.cbcBoPhan.getSelectedIndex()>=0)
		{
			nv.setBophan(dsbp.get(DialogThemNhanVienMoi_Luat.cbcBoPhan.getSelectedIndex()));
		}
		if(!DialogThemNhanVienMoi_Luat.txtDienThoai.getText().isEmpty())
		{
			nv.setPhone(Integer.parseInt(DialogThemNhanVienMoi_Luat.txtDienThoai.getText()));
		}
		if(!DialogThemNhanVienMoi_Luat.txtDiDong.getText().isEmpty())
		{				
			nv.setDiDong(Integer.parseInt(DialogThemNhanVienMoi_Luat.txtDiDong.getText()));					
		}
		
		if (NhanVienDao_Luat.ThemNhanVien(nv))
		{
			JOptionPane.showConfirmDialog(null, "Thêm Thành Công.","",JOptionPane.CLOSED_OPTION);			
			// Load lại table ở màn hình nhân viên.			
			ReLoadtableNhanVien();
			XoaTextFiel();			
		}
		else
		{
			JOptionPane.showConfirmDialog(null, "Thêm Thất Bại!","",JOptionPane.CLOSED_OPTION);
		}
	}
	
	public static void ReLoadtableNhanVien()
	{
		dsnv=NhanVienDao_Luat.LayDanhSachNhanVien();
		DefaultTableModel dt =new DefaultTableModel();
		dt.addColumn("Mã");
		dt.addColumn("Tên");
		dt.addColumn("Địa Chỉ");
		dt.addColumn("Điện Thoại");
		dt.addColumn("Di Động");
		dt.addColumn("Email");
		dt.addColumn("Còn Quản Lý");
		for(int i=0;i<dsnv.size();i++)
		{
			Nhanvien nv=dsnv.get(i);
			dt.addRow(new Object[]{nv.getMaNv(),nv.getTenNv(),nv.getDiaChi(),nv.getPhone(),nv.getDiDong(),nv.getEmail()});
		}
		ManHinhNhanVien_Luat.table.setModel(dt);
		ManHinhNhanVien_Luat.table.repaint();
		ManHinhNhanVien_Luat.table.revalidate();
	}
	public static void XoaTextFiel()
	{
		DialogThemNhanVienMoi_Luat.txtMaNV.setText(null);
		DialogThemNhanVienMoi_Luat.txtTenNV.setText(null);
		DialogThemNhanVienMoi_Luat.txtDiaChi.setText(null);
		DialogThemNhanVienMoi_Luat.txtDiDong.setText(null);
		DialogThemNhanVienMoi_Luat.txtDienThoai.setText(null);
		// Updating.....
	}
		
	public static Nhanvien LoadThongTinNhanVienVaoDialogSuaNhanVien()
	{
		String MaNV=ManHinhNhanVien_Luat.table.getValueAt(ManHinhNhanVien_Luat.table.getSelectedRow(), 0).toString();
		Nhanvien nv = NhanVienDao_Luat.LayThongTinNhanVien(MaNV);
		if(nv==null)
		{
			JOptionPane.showConfirmDialog(null, "Load Nha Cung Cap That Bai.","",JOptionPane.CLOSED_OPTION);
			return null;
		}
		else
		{
			// Load 3 comboBox.			
			// Chức Vụ
			Chucvu cv = null;
			DefaultComboBoxModel defaultcomboboxChucVu=new DefaultComboBoxModel();
			
			for(int i=0;i<dscv.size();i++)
			{
				cv=dscv.get(i);
				defaultcomboboxChucVu.addElement(cv.getTenChucVu());			
			}
			DialogSuaThongTinNhanVien_Luat.cbcChucVu.setModel(defaultcomboboxChucVu);
			try
			{
				DialogSuaThongTinNhanVien_Luat.cbcChucVu.setSelectedIndex(ChoBietViTriSelectedIndex(nv.getChucvu().getChucVuId(), "ChucVu"));
			}
			catch(Exception ex)
			{
				DialogSuaThongTinNhanVien_Luat.cbcChucVu.setSelectedIndex(-1);
			}
			
			// Bộ Phận
			Bophan bp = null;
			DefaultComboBoxModel defaultcomboboxBoPhan=new DefaultComboBoxModel();
			
			for(int i=0;i<dsbp.size();i++)
			{
				bp=dsbp.get(i);
				defaultcomboboxBoPhan.addElement(bp.getTenBoPhan());			
			}
			DialogSuaThongTinNhanVien_Luat.cbcBoPhan.setModel(defaultcomboboxBoPhan);
			try
			{
				DialogSuaThongTinNhanVien_Luat.cbcBoPhan.setSelectedIndex(ChoBietViTriSelectedIndex(nv.getBophan().getBoPhanId(), "BoPhan"));
			}
			catch(Exception ex)
			{
				DialogSuaThongTinNhanVien_Luat.cbcBoPhan.setSelectedIndex(-1);
			}			
			// Nhân Viên
			Nhanvien nvql = null;
			DefaultComboBoxModel defaultcomboboxNhanVienQuanLy=new DefaultComboBoxModel();			
			
			defaultcomboboxNhanVienQuanLy.addElement("NULL");
			for(int i=0;i<dsnv.size();i++)
			{
				nvql=dsnv.get(i);
				String chuoiNhanVien=nvql.getTenNv() + "  |  " + nvql.getMaNv();
				defaultcomboboxNhanVienQuanLy.addElement(chuoiNhanVien);			
			}
			DialogSuaThongTinNhanVien_Luat.cbcNVQuanLy.setModel(defaultcomboboxNhanVienQuanLy);
			try
			{
				DialogSuaThongTinNhanVien_Luat.cbcNVQuanLy.setSelectedIndex(ChoBietViTriSelectedIndex(nv.getNhanvien().getMaNv() , "QuanLy")+1);
			}
			catch(Exception ex)
			{
				DialogSuaThongTinNhanVien_Luat.cbcNVQuanLy.setSelectedIndex(-1);
			}			
			
			// Kết Thúc Load 3 comboBox.
			
			DialogSuaThongTinNhanVien_Luat.txtMaNV.setText(nv.getMaNv());
			DialogSuaThongTinNhanVien_Luat.txtMaNV.setEditable(false);
			
			DialogSuaThongTinNhanVien_Luat.txtTenNV.setText(nv.getTenNv());
			DialogSuaThongTinNhanVien_Luat.txtDiaChi.setText(nv.getDiaChi());
			DialogSuaThongTinNhanVien_Luat.txtEmail.setText(nv.getEmail());
			try
			{
				DialogSuaThongTinNhanVien_Luat.txtDienThoai.setText(nv.getPhone().toString());
			}catch(Exception ex){}	
			
			try
			{
				DialogSuaThongTinNhanVien_Luat.txtDiDong.setText(nv.getDiDong().toString());
			}catch(Exception ex){}
			////
			
		}
		return nv;
	}
	public static int ChoBietViTriSelectedIndex(String Chuoi,String Loai)
	{
		if(Loai.compareTo("ChucVu")==0)
		{
			for(int i=0;i<dscv.size();i++)
			{
				if(Chuoi.compareTo(dscv.get(i).getChucVuId())==0)
				{
					return i;
				}
			}
		}
		if(Loai.compareTo("BoPhan")==0)
		{
			for(int i=0;i<dsbp.size();i++)
			{
				if(Chuoi.compareTo(dsbp.get(i).getBoPhanId())==0)
				{
					return i;
				}
			}
		}
		if(Loai.compareTo("QuanLy")==0)
		{
			for(int i=0;i<dsnv.size();i++)
			{
				if(Chuoi.compareTo(dsnv.get(i).getMaNv())==0)
				{
					return i;
				}
			}
		}
		
		return -1;
	}
	
	public static boolean ThaoTacCapNhat(Nhanvien nv)
	{			
			nv.setTenNv(DialogSuaThongTinNhanVien_Luat.txtTenNV.getText());
			nv.setDiaChi(DialogSuaThongTinNhanVien_Luat.txtDiaChi.getText());
			nv.setDiaChi(DialogSuaThongTinNhanVien_Luat.txtDiaChi.getText());
			nv.setEmail(DialogSuaThongTinNhanVien_Luat.txtEmail.getText());
			
			if(DialogSuaThongTinNhanVien_Luat.cbcNVQuanLy.getSelectedIndex()>0)
			{
				nv.setNhanvien(dsnv.get(DialogSuaThongTinNhanVien_Luat.cbcNVQuanLy.getSelectedIndex()-1));
			}
			else
				nv.setNhanvien(null);
			
			if(DialogSuaThongTinNhanVien_Luat.cbcChucVu.getSelectedIndex()>=0)
			{
				nv.setChucvu(dscv.get(DialogSuaThongTinNhanVien_Luat.cbcChucVu.getSelectedIndex()));
			}
			if(DialogSuaThongTinNhanVien_Luat.cbcBoPhan.getSelectedIndex()>=0)
			{
				nv.setBophan(dsbp.get(DialogSuaThongTinNhanVien_Luat.cbcBoPhan.getSelectedIndex()));
			}
			
			if(!DialogSuaThongTinNhanVien_Luat.txtDienThoai.getText().isEmpty())
			{
				nv.setPhone(Integer.parseInt(DialogSuaThongTinNhanVien_Luat.txtDienThoai.getText()));
			}
			else
			{
				nv.setPhone(null);
			}
			if(!DialogSuaThongTinNhanVien_Luat.txtDiDong.getText().isEmpty())
			{				
				nv.setDiDong(Integer.parseInt(DialogSuaThongTinNhanVien_Luat.txtDiDong.getText()));					
			}
			else
			{
				nv.setDiDong(null);
			}
			// Bộ Phận
			// NV Quản Lý
			if (NhanVienDao_Luat.CapNhatThongTinNhanVien(nv))
			{
				JOptionPane.showConfirmDialog(null, "Cập Nhật Thành Công.","",JOptionPane.CLOSED_OPTION);
				// Load lại table ở màn hình nhân viên.			
				ReLoadtableNhanVien();
				return true;			
			}
			else
			{
				JOptionPane.showConfirmDialog(null, "Cập Nhật Thất Bại check !","",JOptionPane.CLOSED_OPTION);
			}
		
		return false;
	}
	
	public static void ThaoTacThem()
	{
		try 
		{
			if(NhanVien_Controller_Luat.isThemOk())
			{
				if(NhanVien_Controller_Luat.isMaNhanVienTonTai(DialogThemNhanVienMoi_Luat.txtMaNV.getText()))
				{
					JOptionPane.showConfirmDialog(null, "Mã NV Nhập Bị Trùng!.","",JOptionPane.CLOSED_OPTION);
				}
				else
				{
					ThemNhanVien();
					
					//Xóa text fiel.
					DialogThemNhanVienMoi_Luat.txtMaNV.setText(TuPhatSinhMaNV());
					DialogThemNhanVienMoi_Luat.txtDiDong.setText(null);
					DialogThemNhanVienMoi_Luat.txtDiaChi.setText(null);
					DialogThemNhanVienMoi_Luat.txtDienThoai.setText(null);
					DialogThemNhanVienMoi_Luat.txtEmail.setText(null);
					DialogThemNhanVienMoi_Luat.txtTenNV.setText(null);
					
					DialogThemNhanVienMoi_Luat.cbcBoPhan.setSelectedIndex(-1);
					DialogThemNhanVienMoi_Luat.cbcChucVu.setSelectedIndex(-1);
					DialogThemNhanVienMoi_Luat.cbcNVQuanLy.setSelectedIndex(-1);
				}						
			}
			else
			{
				JOptionPane.showConfirmDialog(null, "Nhap Thieu Cac TextBox Quan Trong.","",JOptionPane.CLOSED_OPTION);
			}
		} 
		catch (Exception e2) 
		{							
			e2.printStackTrace();
		}
	}
	
	public static void ThaoTacXoa() 
	{
		String MaNV = ManHinhNhanVien_Luat.table.getValueAt(ManHinhNhanVien_Luat.table.getSelectedRow(), 0).toString();
		if (NhanVienDao_Luat.XoaNhanVien(MaNV)) 
		{
			if (!isMaNhanVienTonTai(MaNV)) 
			{
				JOptionPane.showConfirmDialog(null, "Xóa Thành Công.", "", JOptionPane.CLOSED_OPTION);
				ReLoadtableNhanVien();
				ManHinhNhanVien_Luat.btnSuaChua.setEnabled(false);
				ManHinhNhanVien_Luat.btnXoa.setEnabled(false);
			} 
			else 
			{
				JOptionPane.showConfirmDialog(null, "Xóa Thất Bại do NV này là quản lý của 1 NV khác!", "", JOptionPane.CLOSED_OPTION);
			}
		} 
		else 
		{
			JOptionPane.showConfirmDialog(null, "Xóa Thất Bại!", "", JOptionPane.CLOSED_OPTION);
		}
	}
	
	public static String TuPhatSinhMaNV()
	{
		if(dsnv.size() > 0)
		{
			String MaNVCuoiCung=dsnv.get(dsnv.size()-1).getMaNv();
			String MaNVtrave;
			int so=Integer.parseInt(MaNVCuoiCung.substring(2));
			do
			{
				so++;
				MaNVtrave="NV"+so;
			}
			while(isMaNhanVienTonTai(MaNVtrave));
			
			return MaNVtrave;
		}
		else
		{
			return "NV1";
		}		
	}
	
	//// End.	
}
