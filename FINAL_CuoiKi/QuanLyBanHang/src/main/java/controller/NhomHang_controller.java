package controller;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import dao.*;
import pojo.Nhomhang;
import GUI.NhomHang;
public class NhomHang_controller {
	public static void LoadData(){
		 nhomhangDAO nhomhangDAO = new nhomhangDAO();
		DefaultTableModel dtm= new DefaultTableModel();
		dtm.addColumn("Mã");
		dtm.addColumn("Tên");
		dtm.addColumn("Ghi Chú");
		
		for(Nhomhang nhomhang:nhomhangDAO.findAll()){
			dtm.addRow(new Object[]{nhomhang.getNhomHangId(),nhomhang.getTenNhomHang(),nhomhang.getGhiChu()} );
		}
		NhomHang.jtablenhomhang.setModel(dtm);
	}
	
	public void suaNhomHang(JTextField id ,JTextField ten ,JTextField chuthich ) {
		nhomhangDAO nhomhangDao =new nhomhangDAO();
		Nhomhang pojoNhomhang = new Nhomhang();
		pojoNhomhang =nhomhangDao.find(id.getText().toString());
		pojoNhomhang.setNhomHangId(id.getText());
		pojoNhomhang.setTenNhomHang(ten.getText());
		pojoNhomhang.setGhiChu(chuthich.getText());
		if(nhomhangDao.saveorupdate(pojoNhomhang)){
			JOptionPane.showMessageDialog(null, "update nhóm hàng thành công");
			LoadData();
		}
		else{
			JOptionPane.showMessageDialog(null, "update nhóm hàng thất bại");

		}
	}
	
}
