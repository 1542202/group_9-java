package controller;
import dao.*;
import pojo.*;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.*;
public class ThemBoPhan_controller {

	public void themBoPhan(JTextField id ,JTextField ten ,JTextField chuthich ) {
		bophanDAO bophanDao =new bophanDAO();
		Bophan pojoBophan = new Bophan();
		pojoBophan.setBoPhanId(id.getText());
		pojoBophan.setTenBoPhan(ten.getText());
		pojoBophan.setMoTa(chuthich.getText());
		if(bophanDao.saveorupdate(pojoBophan)){
			JOptionPane.showMessageDialog(null, "Thêm bộ phận thành công");
			BoPhan_controller.LoadData();
		}
		else{
			JOptionPane.showMessageDialog(null, "Thêm bộ phận thất bại");

		}
	}
}
