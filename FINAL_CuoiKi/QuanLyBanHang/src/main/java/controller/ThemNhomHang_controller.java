package controller;
import dao.*;
import pojo.*;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.*;
public class ThemNhomHang_controller {

	public void themNhomHang(JTextField id ,JTextField ten ,JTextField chuthich ) {
		nhomhangDAO nhomhangDao =new nhomhangDAO();
		Nhomhang pojoNhomhang = new Nhomhang();
		pojoNhomhang.setNhomHangId(id.getText());
		pojoNhomhang.setTenNhomHang(ten.getText());
		pojoNhomhang.setGhiChu(chuthich.getText());
		if(nhomhangDao.saveorupdate(pojoNhomhang)){
			JOptionPane.showMessageDialog(null, "Thêm nhóm hàng thành công");
			NhomHang_controller.LoadData();
		}
		else{
			JOptionPane.showMessageDialog(null, "Thêm nhóm hàng thất bại");

		}
	}
}
