package controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import GUI.backup_databse;
import GUI.restore_database;

public class backup_restore {
	
	static String duongdan = null;
	public static void backup_choose()
	{
		
		JFileChooser chooser  = new JFileChooser();
        
		chooser.showSaveDialog(null);
		String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		try {
			File f = chooser.getSelectedFile();
			duongdan = f.getAbsolutePath();
			duongdan = duongdan.replace('\\', '/');
			duongdan = duongdan + "_" + date + ".sql";
            backup_databse.txtpath.setText(duongdan);
            //System.out.println(duongdan);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public static void restore_choose()
	{
		
		JFileChooser chooser  = new JFileChooser();
        
		chooser.showOpenDialog(null);
		String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		try {
			File f = chooser.getSelectedFile();
			duongdan = f.getAbsolutePath();
			duongdan = duongdan.replace('\\', '/');
			
            restore_database.txtpath.setText(duongdan);
            //System.out.println(duongdan);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public static void restore()
	{
		String us = "root";
		String pass ="123456";
		 String[] restoreCmd = new String[]{"C:/Program Files/MySQL/MySQL Server 5.7/bin/mysql.exe ", "--user=" + us, "--password=" + pass, "-e", "source " + duongdan};
		Process process;
		try {
			process = Runtime.getRuntime().exec(restoreCmd);
			int proCom = process.waitFor();
			if(proCom ==0 )
			{
				JOptionPane.showConfirmDialog(null, "restore thanh cong", "Xac Nhan", JOptionPane.CLOSED_OPTION);
				
			}
			else {
				JOptionPane.showConfirmDialog(null, "restore that bai", "Xac Nhan", JOptionPane.CLOSED_OPTION);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	public static void backup()
	{
		Process p = null;
		try {
			Runtime runtime = Runtime.getRuntime();
			p = runtime.exec("C:/Program Files/MySQL/MySQL Server 5.7/bin/mysqldump.exe -uroot -p123456 --add-drop-database -B test4 -r" + duongdan );
			int processComplete = p.waitFor();
			if(processComplete ==0 )
			{
				JOptionPane.showConfirmDialog(null, "Backup thanh cong", "Xac Nhan", JOptionPane.CLOSED_OPTION);
				
			}
			else {
				JOptionPane.showConfirmDialog(null, "Backup that bai", "Xac Nhan", JOptionPane.CLOSED_OPTION);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	
}
