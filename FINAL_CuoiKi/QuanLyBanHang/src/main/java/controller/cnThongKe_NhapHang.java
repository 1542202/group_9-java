package controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import GUI.KhoHang;
import GUI.PhieuNhapHang;
import GUI.ThongKe_NhapHang;
import dao.nhaphangDAO;
import dao.thongke_nhaphangDAO;
import pojo.NhaphangChitiet;
import pojo.Nhaphang;

public class cnThongKe_NhapHang {

	public static void load_data_chitiet()
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ThongKe_NhapHang tknh = null;
		thongke_nhaphangDAO nhd= new thongke_nhaphangDAO();		
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.addColumn("Chứng Từ");
		dtm.addColumn("Ngày");
		dtm.addColumn("Nhà Cung Cap");
//		dtm.addColumn("Số Lượng");
//		dtm.addColumn("Đơn Giá");
//		dtm.addColumn("Thành Tiền");
//		dtm.addColumn("Ghi Chú");

		String ngay_tu= formatter.format(tknh.selectedDate_ngay_tu);
		
		String ngay_den= formatter.format(tknh.selectedDate_ngay_den);
		for(Nhaphang nh : nhd.ThongKeNhapHang(ngay_tu,ngay_den ))
		{
			
			dtm.addRow(new Object[]{nh.getNhapHangId(), nh.getNgayLap(), nh.getMaNcc()});
		}
		ThongKe_NhapHang.table.setModel(dtm);
		ThongKe_NhapHang.table.repaint();
		ThongKe_NhapHang.table.revalidate();
	
	}
	public static void load_data_nhaphangchitiet()
	{
		
		nhaphangDAO nhd= new nhaphangDAO();		
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.addColumn("Mã Hàng");
		dtm.addColumn("Tên Hàng");
		dtm.addColumn("Đơn Vị");
		dtm.addColumn("Số Lượng");
		dtm.addColumn("Đơn Giá");
		dtm.addColumn("Thành Tiền");
		dtm.addColumn("Ghi Chú");


	//	NhaphangChitiet nhct = (NhaphangChitiet) nhaphangDAO.laydanhsachnhaphangchitiet(noidung);
		//System.out.println(nhct);
		for(NhaphangChitiet nhct : nhd.laydanhsachnhaphangchitiet(ThongKe_NhapHang.machungtu))
		{
			//System.out.println(nhct.getSanpham().getTenSp());
			dtm.addRow(new Object[]{nhct.getSanpham().getSanPhamId(),nhct.getSanpham().getTenSp(),nhct.getSanpham().getDonvi().getTenDv(),nhct.getSoLuong(),nhct.getDonGia(), nhct.getThanhTien(), nhct.getGhiChu()});
		}
		ThongKe_NhapHang.table_chitietnhaphang.setModel(dtm);
		ThongKe_NhapHang.table_chitietnhaphang.repaint();
		ThongKe_NhapHang.table_chitietnhaphang.revalidate();
		
	}
	public static void xuatfile(JFrame frame )
	{
		JFileChooser chooser  = new JFileChooser();
        int i = chooser.showSaveDialog(frame);
        if(i == JFileChooser.APPROVE_OPTION)
        {
       	 File file = chooser.getSelectedFile();
       	 try {
       		
       		 
				FileWriter out = new FileWriter(file+".xls");
				
				List<NhaphangChitiet> nhct = thongke_nhaphangDAO.list_nhaphangchitiet();
				
				BufferedWriter writer =new BufferedWriter(out);
				
				writer.write("Nhap Hang"+"\t" +"Ten San Pham" +"\t" + "Don Vi" +"\t" +"Don Gia" +"\t" +"So Luong"+"\t" +"Thanh Tien");
				writer.write("\n");
					for (int j1 = 0; j1 < nhct.size(); j1++) {
						writer.write(nhct.get(j1).getNhaphang().getNhapHangId()+"\t");
						writer.write(nhct.get(j1).getSanpham().getTenSp()+"\t");
						writer.write(nhct.get(j1).getDonvi().getTenDv()+"\t");						
						writer.write(nhct.get(j1).getDonGia()+"\t");
						writer.write(nhct.get(j1).getSoLuong()+"\t");
						writer.write(nhct.get(j1).getThanhTien()+"\t");
						writer.write("\n");
					}
					
					
					
				writer.close();
				JOptionPane.showConfirmDialog(frame, "Save", "Xác Nhận", JOptionPane.CLOSED_OPTION);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				JOptionPane.showConfirmDialog(frame, "Can not Save","Xác Nhận", JOptionPane.CLOSED_OPTION);
				e.printStackTrace();
			}
        }

		
		
		
	}
}
