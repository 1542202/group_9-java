package controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import GUI.ThongKe_NhapHang;
import GUI.ThongKe_XuatHang;
import dao.nhaphangDAO;
import dao.thongke_nhaphangDAO;
import dao.thongke_xuathangDAO;
import dao.xuathangDAO;
import pojo.Nhaphang;
import pojo.NhaphangChitiet;
import pojo.Xuathang;
import pojo.XuathangChitiet;

public class cnThongKe_XuatHang {
	public static void load_data_chitiet()
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ThongKe_XuatHang tkxh = null;
		thongke_xuathangDAO xhd= new thongke_xuathangDAO();		
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.addColumn("Chứng Từ");
		dtm.addColumn("Ngày");
		dtm.addColumn("Khách Hàng");
//		dtm.addColumn("Số Lượng");
//		dtm.addColumn("Đơn Giá");
//		dtm.addColumn("Thành Tiền");
//		dtm.addColumn("Ghi Chú");

		String ngay_tu= formatter.format(tkxh.selectedDate_ngay_tu);
		
		String ngay_den= formatter.format(tkxh.selectedDate_ngay_den);
		for(Xuathang xh : xhd.ThongKeXuatHang(ngay_tu,ngay_den ))
		{
			
			dtm.addRow(new Object[]{xh.getXuatHangId(), xh.getNgayLap(), xh.getKhachang().getTenKh()});
		}
		ThongKe_XuatHang.table.setModel(dtm);
		ThongKe_XuatHang.table.repaint();
		ThongKe_XuatHang.table.revalidate();
	
	}
	public static void load_data_xuathangchitiet()
	{
		
		xuathangDAO nhd= new xuathangDAO();		
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.addColumn("Mã Hàng");
		dtm.addColumn("Tên Hàng");
		dtm.addColumn("Đơn Vị");
		dtm.addColumn("Số Lượng");
		dtm.addColumn("Đơn Giá");
		
		dtm.addColumn("Chiết Khấu");
		dtm.addColumn("Thành Tiền");

	//	NhaphangChitiet nhct = (NhaphangChitiet) nhaphangDAO.laydanhsachnhaphangchitiet(noidung);
		//System.out.println(nhct);
		for(XuathangChitiet xhct : nhd.laydanhsachxuathangchitiet(ThongKe_XuatHang.machungtu))
		{
			//System.out.println(nhct.getSanpham().getTenSp());
			dtm.addRow(new Object[]{xhct.getSanpham().getSanPhamId(), xhct.getSanpham().getTenSp(), xhct.getDonvi().getTenDv(), xhct.getId().getSoLuong(), xhct.getId().getDonGia(), xhct.getId().getChietKhau(), xhct.getId().getThanhTien()});
		}
		ThongKe_XuatHang.table_chitietnhaphang.setModel(dtm);
		ThongKe_XuatHang.table_chitietnhaphang.repaint();
		ThongKe_XuatHang.table_chitietnhaphang.revalidate();
		
	}
	public static void xuatfile(JFrame frame )
	{
		JFileChooser chooser  = new JFileChooser();
        int i = chooser.showSaveDialog(frame);
        if(i == JFileChooser.APPROVE_OPTION)
        {
       	 File file = chooser.getSelectedFile();
       	 try {
       		
       		 
				FileWriter out = new FileWriter(file+".xls");
				
				List<XuathangChitiet> xhct = thongke_xuathangDAO.list_xuathangchitiet();
				
				BufferedWriter writer =new BufferedWriter(out);
				
				writer.write("Xuat Hang"+"\t" +"Ten San Pham" +"\t" + "Don Vi" +"\t" +"Don Gia" +"\t" +"So Luong"+"\t"+"Chiet Khau" +"\t"+"Thanh Tien");
				writer.write("\n");
					for (int j1 = 0; j1 < xhct.size(); j1++) {
						writer.write(xhct.get(j1).getXuathang().getXuatHangId()+"\t");
						writer.write(xhct.get(j1).getSanpham().getTenSp()+"\t");
						writer.write(xhct.get(j1).getDonvi().getTenDv()+"\t");						
						writer.write(xhct.get(j1).getId().getDonGia()+"\t");
						writer.write(xhct.get(j1).getId().getSoLuong()+"\t");
						writer.write(xhct.get(j1).getId().getChietKhau()+"\t");
						writer.write(xhct.get(j1).getId().getThanhTien()+"\t");
						writer.write("\n");
					}
					
					
					
				writer.close();
				JOptionPane.showConfirmDialog(frame, "Save", "Xác Nhận", JOptionPane.CLOSED_OPTION);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				JOptionPane.showConfirmDialog(frame, "Can not Save","Xác Nhận", JOptionPane.CLOSED_OPTION);
				e.printStackTrace();
			}
        }

		
		
		
	}
}
