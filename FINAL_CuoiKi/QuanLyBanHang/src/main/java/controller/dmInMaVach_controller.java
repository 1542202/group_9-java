package controller;

import java.awt.Desktop;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackReader;

import javax.imageio.ImageIO;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;

import org.hibernate.boot.jaxb.internal.stax.BufferedXMLEventReader;

import GUI.InMaVach;
import GUI.InMaVach_solanin;
import dao.inmavachDAO;

import pojo.Sanpham;
import com.barcode_coder.java_barcode.*; 

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import dao.inmavachDAO;
import java.awt.print.PrinterException;

public class dmInMaVach_controller  {
	public static void LoadData()
	{
		
		inmavachDAO imvd = new inmavachDAO();
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.addColumn("Mã Sản phẩm");
		dtm.addColumn("Tên Sản Phẩm");
		dtm.addColumn("Giá Bán Lẻ");
		dtm.addColumn("Đơn Vị");
		dtm.addColumn("Số Lần In");
		
		for(Sanpham sp : imvd.findAll())
		{	
			dtm.addRow(new Object[]{sp.getSanPhamId(), sp.getTenSp(),sp.getGiaBanLe(), sp.getDonvi().getDonViId(), sp.getSoLanInMaVach()});
		}
		
		InMaVach.table.setModel(dtm);
		InMaVach.table.repaint();
		InMaVach.table.revalidate();
		
		
	}
	public static void thaydoisolanin()
	{
		InMaVach_solanin sln=  null;
		inmavachDAO imvd = new inmavachDAO();
		InMaVach imv_GUi = null;
		//Sanpham sp_pojo = new Sanpham();
		
		String solan = sln.txtsolanin.getText().toString();
		String un = imv_GUi.table.getValueAt(imv_GUi.table.getSelectedRow(), 0).toString();
		Sanpham sp = imvd.find(un);
		
		sp.setSoLanInMaVach(Integer.parseInt(solan));
		if(imvd.update(sp))
		{
			JOptionPane.showConfirmDialog(null, "Doi thanh cong", "xac nhan", JOptionPane.CLOSED_OPTION);
		}
		else 
			JOptionPane.showConfirmDialog(null, "doi kong thanh cong","xac nhac", JOptionPane.CLOSED_OPTION);
	}
	public static void infile() throws IOException, PrinterException
	{
		
        {
       
//       	
//        	JTextPane textPane = new JTextPane();
//
//            textPane.set("C:\\Users\\XuanThai\\Desktop\\csdl.txt");
//
//            textPane.print();

        }
	}
	public static void saveimgtoexcel(JFrame frame)
	{
		JFileChooser chooser  = new JFileChooser();
        int i = chooser.showSaveDialog(frame);
        if(i == JFileChooser.APPROVE_OPTION)
        {
       	 File file = chooser.getSelectedFile();
       	 try {
       		 
		       		inmavachDAO imvd = new inmavachDAO();
		    		DefaultTableModel dtm = new DefaultTableModel();
		    		int m =0;
    	
       				FileOutputStream out = new FileOutputStream(file+".xls");
       				HSSFWorkbook  my_workbook = new HSSFWorkbook();
       				HSSFSheet my_sheet = my_workbook.createSheet("InMaVach"); 
       			
				 try {
					 	int cot =0;
	    				int dong =0;
		    			for(Sanpham sp: imvd.findAll())
		    			{
		    				
		    				String giatri = sp.getSanPhamId();
		    				for (int j2 = 0; j2 <sp.getSoLanInMaVach(); j2++) {
		    				Barcode b = BarcodeFactory.createBarcode(BarcodeType.Code128,giatri);  
		    				System.out.print(b);
		    				b.export("png",1,50,true,"/Users/XuanThai/Desktop/mavach/image"+m+".png"); 
		    				
		    				m++;
		    				}
		    			}
		    				for(int p =0; p<m;p++)
		    				{
		    					HSSFPatriarch drawing = my_sheet.createDrawingPatriarch();
		    				ClientAnchor my_anchor = new HSSFClientAnchor(); 
		    				
		    			
		    						 InputStream my_banner_image = new FileInputStream("C:\\Users\\XuanThai\\Desktop\\mavach\\image"+p+".png");
				    				 byte[] bytes = IOUtils.toByteArray(my_banner_image);
				    				 int my_picture_id = my_workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
				    				 my_banner_image.close();  
		    				  		
				    				 
				    				 my_anchor.setCol1(cot);
				    				 my_anchor.setRow1(dong);
				    				 cot = cot+4;
				    				// my_anchor.setCol2(cot);
				    				 HSSFPicture  my_picture = drawing.createPicture(my_anchor, my_picture_id);
				    				 my_picture.resize();
				    				  
				    				 if(cot > 9 )
				    				 {	cot = 0;
				    				 	dong = dong + 6;
				    				 }
				    				 
		    				}
		    				
				    											
		    				
						
		    	                
		    				
		    			
		    			JOptionPane.showConfirmDialog(null, "In Thanh Cong","xac nhan", JOptionPane.CLOSED_OPTION);
		    		} catch (Exception e) {
		    			// TODO: handle exception
		    			JOptionPane.showConfirmDialog(null, "In that bai","xac nhan", JOptionPane.CLOSED_OPTION);
		    		}
				 
		                my_workbook.write(out);
		                out.close();
			
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				
				e.printStackTrace();
			}
        }
	}
	
	
	
//	public static void saveimgtoexcel(JFrame frame)
//	{
//		JFileChooser chooser  = new JFileChooser();
//        int i = chooser.showSaveDialog(frame);
//        if(i == JFileChooser.APPROVE_OPTION)
//        {
//       	 File file = chooser.getSelectedFile();
//       	 try {
//       		 
//		       		inmavachDAO imvd = new inmavachDAO();
//		    		DefaultTableModel dtm = new DefaultTableModel();
//		    		int m =0;
//    	
//       				FileOutputStream out = new FileOutputStream(file+".xls");
//       				HSSFWorkbook  my_workbook = new HSSFWorkbook();
//       				HSSFSheet my_sheet = my_workbook.createSheet("InMaVach"); 
//       			
//				 try {
//					 	int cot =0;
//	    				int dong =0;
//		    			for(Sanpham sp: imvd.findAll())
//		    			{
//		    				
//		    				String giatri = sp.getSanPhamId();
//		    			
//		    				Barcode b = BarcodeFactory.createBarcode(BarcodeType.Code128,giatri);  
//		    				System.out.print(b);
//		    				b.export("png",1,50,true,"/Users/XuanThai/Desktop/mavach/image"+m+".png"); 
//		    				
//		    				
//		    				HSSFPatriarch drawing = my_sheet.createDrawingPatriarch();
//		    				ClientAnchor my_anchor = new HSSFClientAnchor(); 
//		    				
//		    				for (int j2 = 0; j2 <sp.getSoLanInMaVach(); j2++) {
//		    						 InputStream my_banner_image = new FileInputStream("C:\\Users\\XuanThai\\Desktop\\mavach\\image"+m+".png");
//				    				 byte[] bytes = IOUtils.toByteArray(my_banner_image);
//				    				 int my_picture_id = my_workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
//				    				 my_banner_image.close();  
//		    				  		
//				    				 
//				    				 my_anchor.setCol1(cot);
//				    				 my_anchor.setRow1(dong);
//				    				 cot = cot+4;
//				    				// my_anchor.setCol2(cot);
//				    				 HSSFPicture  my_picture = drawing.createPicture(my_anchor, my_picture_id);
//				    				 my_picture.resize();
//				    				  
//				    				 if(cot > 9 )
//				    				 {	cot = 0;
//				    				 	dong = dong + 6;
//				    				 }
//				    				 
//				    											
//		    				}
//						
//		    	                
//		    				m++;
//		    			}
//		    			JOptionPane.showConfirmDialog(null, "In Thanh Cong","xac nhan", JOptionPane.CLOSED_OPTION);
//		    		} catch (Exception e) {
//		    			// TODO: handle exception
//		    			JOptionPane.showConfirmDialog(null, "In that bai","xac nhan", JOptionPane.CLOSED_OPTION);
//		    		}
//				 
//		                my_workbook.write(out);
//		                out.close();
//			
//				
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				
//				e.printStackTrace();
//			}
//        }
//	}
	public static void inmavach()
	{
		inmavachDAO imvd = new inmavachDAO();
		DefaultTableModel dtm = new DefaultTableModel();
		int m =0;
		try {
			for(Sanpham sp: imvd.findAll())
			{
				
				String giatri = sp.getSanPhamId();
			
				Barcode b = BarcodeFactory.createBarcode(BarcodeType.Code128,giatri);  
				System.out.print(b);
				b.export("png",2,50,true,"/Users/XuanThai/Desktop/mavach/image"+m+".png"); 
				m++;
			}
			JOptionPane.showConfirmDialog(null, "In Thanh Cong","xac nhan", JOptionPane.CLOSED_OPTION);
		} catch (Exception e) {
			// TODO: handle exception
			JOptionPane.showConfirmDialog(null, "In that bai","xac nhan", JOptionPane.CLOSED_OPTION);
		}
		
		
		
		
		
	}
	
}
