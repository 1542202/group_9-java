package controller;


import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import org.hibernate.Query;
import org.hibernate.Session;

import GUI.PhieuNhapHang;
import GUI.PhieuNhapHang_Them;
import Util.HibernateUtil;
import dao.khohangDAO;
import dao.nhanvienDAO;
import dao.nhaphangDAO;
import pojo.Donvi;
import pojo.Khohang;
import pojo.Nhacungcap;
import pojo.Nhanvien;
import pojo.Nhaphang;
import pojo.NhaphangChitiet;
import pojo.Sanpham;

public class dmMuaHang_NhapPhieu {
	
	
	public static void add_chitiet_nhaphang()
	{
		PhieuNhapHang pnh = null;
		PhieuNhapHang_Them pnht = null;
		nhaphangDAO nhd = new nhaphangDAO();
		NhaphangChitiet nhct= new NhaphangChitiet();
		//nhct.setDonvi(pnht.txtDonVi.getText().toString());
		nhct.setGhiChu(pnht.txtGhiChu.getText().toString());
		Nhaphang nh = nhd.tim_nhaphang(pnh.cbbMaPhieu.getSelectedItem().toString());
		//nh.getNhapHangId();
		
		nhct.setNhaphang(nh);
		BigDecimal dongia = new BigDecimal(pnht.txtGia.getText().toString());
		nhct.setDonGia(dongia);
		//System.out.println(giatri);
		Sanpham sp = nhd.tim_TenSP(pnht.cddMaHang.getSelectedItem().toString());
		nhct.setSanpham(sp);
		//System.out.println(sp);
		BigDecimal soLuong = new BigDecimal(pnht.txtSoLuong.getText().toString());
		nhct.setSoLuong(soLuong);
		int thanhtien = soLuong.intValue()*dongia.intValue();
		BigDecimal setthanhtien = new BigDecimal(thanhtien);
		nhct.setThanhTien(setthanhtien);
		
		String dv= tim_donvi(pnht.txtDonVi.getText().toString());
		Donvi donvi = nhd.tim_donvi(dv);
		System.out.println(nh);
		nhct.setDonvi(donvi);
	
		if(nhd.saveorupdate_chitiet(nhct))
		{
			JOptionPane.showConfirmDialog(null, "Them Thanh COng", "xac nhan", JOptionPane.CLOSED_OPTION);
		}
		else {
			JOptionPane.showConfirmDialog(null, "Them That Bai", "xac nhan", JOptionPane.CLOSED_OPTION);
		}
	
	}
	
	public static String tim_donvi(String tenDv)
	{
		Donvi dv= new Donvi();
		Session session= HibernateUtil.getSessionFactory().openSession();
		try {
			String hql = "select dv from Donvi dv where dv.tenDv = :tenDv";
			Query query= session.createQuery(hql);
			query.setString("tenDv", tenDv);
			dv = (Donvi) query.uniqueResult();
			
			return dv.getDonViId();
		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}finally {
			session.close();
		}
	}
	public static Sanpham tim_MaSP(String tenSp)
	{
		Sanpham sp = new Sanpham();
		Session session= HibernateUtil.getSessionFactory().openSession();
		try {
			
		String hql = "select sp from Sanpham sp where sp.tenSp = :tenSp";
		Query query= session.createQuery(hql);
		query.setString("tenSp", tenSp);
		
		sp = (Sanpham) query.uniqueResult();
		return sp;
		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}
		finally {
			session.close();
		}
		
		
		
	}
	public static void load_data_chitiet()
	{
		String noidung = PhieuNhapHang.cbbMaPhieu.getSelectedItem().toString();
		nhaphangDAO nhd= new nhaphangDAO();		
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.addColumn("Mã Hàng");
		dtm.addColumn("Tên Hàng");
		dtm.addColumn("Đơn Vị");
		dtm.addColumn("Số Lượng");
		dtm.addColumn("Đơn Giá");
		dtm.addColumn("Thành Tiền");
		dtm.addColumn("Ghi Chú");


	//	NhaphangChitiet nhct = (NhaphangChitiet) nhaphangDAO.laydanhsachnhaphangchitiet(noidung);
		//System.out.println(nhct);
		for(NhaphangChitiet nhct : nhd.laydanhsachnhaphangchitiet(noidung))
		{
			//System.out.println(nhct.getSanpham().getTenSp());
			dtm.addRow(new Object[]{nhct.getSanpham().getSanPhamId(),nhct.getSanpham().getTenSp(),nhct.getSanpham().getDonvi().getTenDv(),nhct.getSoLuong(),nhct.getDonGia(), nhct.getThanhTien(), nhct.getGhiChu()});
		}
		PhieuNhapHang.table.setModel(dtm);
		PhieuNhapHang.table.repaint();
		PhieuNhapHang.table.revalidate();
		
	}
	public static void addNhapPhieu()
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		PhieuNhapHang pnh = null ;
		nhaphangDAO nhd = new nhaphangDAO();
		Nhaphang nh =new Nhaphang();
		nh.setNhapHangId(pnh.txtMaPhieu.getText().toString());
		nh.setDiaChi(pnh.txtDT.getText().toString());
		nh.setHtthanhToan(pnh.cbbHT.getSelectedItem().toString());
		nh.setGhiChu(pnh.txtGhiChu.getText().toString());
		nh.setDt(Integer.parseInt(pnh.txtDT.getText()));
		String a= pnh.cbbKho.getSelectedItem().toString();
		Khohang kh = khohangDAO.find(pnh.cbbKho.getSelectedItem().toString());
		kh.getKhoId();
		nh.setKhohang(kh);
		nh.setSoPhieuVietTay(pnh.txtSoPhieu.getText().toString());
		nh.setSoHoaDonVat(pnh.txtVat.getText().toString());
		nh.setDkthanhToan(pnh.cbbDK.getSelectedItem().toString());
		nh.setMaNcc(pnh.textFieldMNCC.getText().toString());
		Nhanvien nv = nhanvienDAO.find_NV(pnh.cbbNhanVien.getSelectedItem().toString());
		nv.getMaNv();
		nh.setNhanvien(nv);
		//Date ngay = (Date) pnh.ngay.getModel().getValue();
		//System.out.println(pnh.selectedDate_ngay);
		//System.out.println(formatter.format(pnh.selectedDate_ngay));
		nh.setNgayLap(formatter.format(pnh.selectedDate_ngay));
		nh.setHanThanhToan(formatter.format(pnh.selectedDate_ngaythanhtoan));
		//nh.setHanThanhToan(pnh.selectedDate_ngaythanhtoan.toString());
		//java.util.Date ngay1 = pnh.ngay1;
		//nh.setHanThanhToan(formatter.parse(pnh.selectedDate_ngaythanhtoan.toString()));
		//nh.setNgayLap(formatter.parse(pnh.selectedDate_ngay.toString()));
		//System.out.println(formatter.parse(pnh.selectedDate_ngaythanhtoan));
		//System.out.println(nh.setKhohang(kh).);
		//nh.setNhanvien(pnh.cbbNhanVien.getSelectedItem().getClass().);
		//System.out.println(pnh.cbbKho.getSelectedItem().toString());
		if(nhd.saveorupdate(nh))
		{
			JOptionPane.showConfirmDialog(null, "Them Thanh COng", "xac nhan", JOptionPane.CLOSED_OPTION);
			
		}
		else
			JOptionPane.showConfirmDialog(null, "Them That Bai", "xac nhan", JOptionPane.CLOSED_OPTION);
		//nhd.saveorupdate(nh);
	}
	
	public static Nhacungcap timMANCC(String tenNcc)	
	{	
		Nhacungcap ncc = new Nhacungcap(); 
		Session session= HibernateUtil.getSessionFactory().openSession();
	try 
	{
		
		
		
		String hql ="select ncc from Nhacungcap ncc where ncc.tenNcc = :tenNcc";
		Query query= session.createQuery(hql);
		query.setString("tenNcc", tenNcc);
		ncc = (Nhacungcap) query.uniqueResult();
	} 
	catch (Exception e) 
	{
		System.err.println(e);
		// TODO: handle exception
	}
	finally 
	{

			session.close();

	}
		
		
		
		return ncc;
		//nhapHang.textFieldMNCC.setText(mncc.getMaNcc());
	}

}
