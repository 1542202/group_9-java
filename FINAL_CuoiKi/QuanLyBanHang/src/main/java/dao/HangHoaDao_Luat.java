package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import Util.HibernateUtil;
import pojo.Nhacungcap;
import pojo.Sanpham;

public class HangHoaDao_Luat {
	public static List<Sanpham> LayDanhSachHangHoa()
	{
		List<Sanpham> ds = null;
		Session session= HibernateUtil.getSessionFactory().openSession();
		try 
		{
			session.beginTransaction();
			Query query = session.createQuery("select sp from Sanpham sp");					
			ds = query.list();
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		finally 
		{
			session.close();
		}
		return ds;
	}
	/*
	public boolean ThemNhaCungCap(Nhacungcap ncc)
	{		
			Session session = HibernateUtil.getSessionFactory().openSession();
			try 
			{
				session.beginTransaction();
				session.save(ncc);
				session.getTransaction().commit();
			
			} catch (HibernateException ex) {
				//Log the exception
				session.getTransaction().rollback();
				System.err.println(ex);
			}
			return true;
	}
	
	public static Nhacungcap LayThongTinNhaCungCap(String MaNhaCungCap)
	{
			Nhacungcap ncc = null;
			Session session = HibernateUtil.getSessionFactory().openSession();
			try 
			{
				ncc = (Nhacungcap)session.get(Nhacungcap.class,MaNhaCungCap);
			} 
			catch (HibernateException ex) 
			{
				//Log the exception
				System.err.println(ex);
			} 
			finally 
			{
				session.close();
			}
			return ncc;
	}
	public static boolean CapNhatThongTinNhaCungCap(Nhacungcap ncc) 
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		if (LayThongTinNhaCungCap(ncc.getMaNcc())==null) 
		{
			return false;
		}
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.update(ncc);
			transaction.commit();
		} 
		catch (HibernateException ex) 
		{
			//Log the exception
			transaction.rollback();
			System.err.println(ex);
		} 
		finally 
		{
			session.close();
		}
		return true;
	}
	public static boolean XoaNhaCungCap(String MaNhaCungCap) 
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Nhacungcap ncc = LayThongTinNhaCungCap(MaNhaCungCap);
		if(ncc==null)
		{
			return false;
		}
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.delete(ncc);
			transaction.commit();
		} 
		catch (HibernateException ex) 
		{
			//Log the exception
			transaction.rollback();
			System.err.println(ex);
		} 
		finally 
		{
			session.close();
		}
		return true;
	}
	*/
	
}
