package dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import Util.HibernateUtil;
import pojo.Khuvuc;

public class KhuVucDao_Luat {
	public static List<Khuvuc> LayDanhSachKhuVuc()
	{
		List<Khuvuc> ds = null;
		Session session= HibernateUtil.getSessionFactory().openSession();
		try 
		{
			session.beginTransaction();
			Query query = session.createQuery("select kv from Khuvuc kv");
			ds = query.list();
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		finally 
		{
			session.close();
		}
		return ds;
	}
}
