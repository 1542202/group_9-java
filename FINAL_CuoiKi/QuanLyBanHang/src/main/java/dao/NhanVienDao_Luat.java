package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import Util.HibernateUtil;
import pojo.Bophan;
import pojo.Chucvu;
import pojo.Khuvuc;
import pojo.Nhacungcap;
import pojo.Nhanvien;

public class NhanVienDao_Luat {
	public static List<Nhanvien> LayDanhSachNhanVien()
	{
		List<Nhanvien> ds = null;
		Session session= HibernateUtil.getSessionFactory().openSession();
		try 
		{
			session.beginTransaction();
			Query query = session.createQuery("select nv from Nhanvien nv");		
			ds = query.list();
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		finally 
		{
			session.close();
		}
		return ds;
	}
	public static boolean ThemNhanVien(Nhanvien nv)
	{		
			Session session = HibernateUtil.getSessionFactory().openSession();
			try 
			{
				session.beginTransaction();
				session.save(nv);
				session.getTransaction().commit();
			
			} catch (HibernateException ex) {
				//Log the exception
				session.getTransaction().rollback();
				System.err.println(ex);
			}
			return true;
	}
	public static Nhanvien LayThongTinNhanVien(String MaNhanVien)
	{
			Nhanvien ncc = null;
			Session session = HibernateUtil.getSessionFactory().openSession();
			try 
			{
				ncc = (Nhanvien)session.get(Nhanvien.class,MaNhanVien);
			} 
			catch (HibernateException ex) 
			{
				//Log the exception
				System.err.println(ex);
			} 
			finally 
			{
				session.close();
			}
			return ncc;
	}
	
	
	
	public static boolean CapNhatThongTinNhanVien(Nhanvien nv) 
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		if (LayThongTinNhanVien(nv.getMaNv())==null) 
		{
			return false;
		}
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.update(nv);
			transaction.commit();
		} 
		catch (HibernateException ex) 
		{
			//Log the exception
			transaction.rollback();
			System.err.println(ex);
		} 
		finally 
		{
			session.close();
		}
		return true;
	}
	public static boolean XoaNhanVien(String MaNhanVien) 
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Nhanvien nv = LayThongTinNhanVien(MaNhanVien);
		if(nv==null)
		{
			return false;
		}
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.delete(nv);
			transaction.commit();
		} 
		catch (HibernateException ex) 
		{
			//Log the exception
			transaction.rollback();
			System.err.println(ex);
		} 
		finally 
		{
			session.close();
		}
		return true;
	}
	public static List<Chucvu> LayDanhSachChucVu()
	{
		List<Chucvu> ds = null;
		Session session= HibernateUtil.getSessionFactory().openSession();
		try 
		{
			session.beginTransaction();
			Query query = session.createQuery("select cv from Chucvu cv");		
			ds = query.list();
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		finally 
		{
			session.close();
		}
		return ds;
	}
	
	public static List<Bophan> LayDanhSachBoPhan()
	{
		List<Bophan> ds = null;
		Session session= HibernateUtil.getSessionFactory().openSession();
		try 
		{
			session.beginTransaction();
			Query query = session.createQuery("select bp from Bophan bp");		
			ds = query.list();
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		finally 
		{
			session.close();
		}
		return ds;
	}
	
	
	
}
