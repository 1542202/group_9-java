package dao;
import java.util.List;

import org.hibernate.SessionFactory;

import Util.*;
import pojo.*;

public class bophanDAO {
private SessionFactory sf= HibernateUtil.getSessionFactory();
public List<Bophan> findAll(){
	try {
		sf.getCurrentSession().beginTransaction();
		return sf.getCurrentSession().createCriteria(Bophan.class).list();
	} catch (Exception e) {
		return null;
	}
}
public Bophan find(String Ma){
	try {
		sf.getCurrentSession().beginTransaction();
		return (Bophan) sf.getCurrentSession().get(Bophan.class, Ma);
	} catch (Exception e) {
		// TODO: handle exception
		return null;
	}
}
public boolean saveorupdate(Bophan bophan){
	try {
		sf.getCurrentSession().beginTransaction();
		sf.getCurrentSession().saveOrUpdate(bophan);
		sf.getCurrentSession().getTransaction().commit();
		return true;
		
	} catch (Exception e) {
		sf.getCurrentSession().getTransaction().rollback();
		return false;
		// TODO: handle exception
	}
}
}
