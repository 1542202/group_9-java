package dao;

import pojo.Sanpham;

import java.util.List;

import org.hibernate.SessionFactory;

import Util.HibernateUtil;;
public class inmavachDAO {
	private final SessionFactory sf = HibernateUtil.getSessionFactory();
	public List<Sanpham> findAll()
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().createCriteria(Sanpham.class).list();
			
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
		
	}
	public Sanpham find(String un)
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().get(Sanpham.class, un);
		} catch (Exception e) {
			// TODO: handle exception
				return null;
		}
	
		
	}
	public boolean update(Sanpham sp) {
		try {
			sf.getCurrentSession().beginTransaction();
			sf.getCurrentSession().update(sp);
			sf.getCurrentSession().getTransaction().commit();
			return true;
		} catch (Exception e) {
			sf.getCurrentSession().getTransaction().rollback();
			// TODO: handle exception
			return false;
		}
		
	}
}
