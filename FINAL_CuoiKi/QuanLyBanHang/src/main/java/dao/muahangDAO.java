package dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import Util.HibernateUtil;
import pojo.Khachang;


public class muahangDAO {
	private final static SessionFactory sf = HibernateUtil.getSessionFactory();
	public List<Khachang> findAll_KH()
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().createCriteria(Khachang.class).list();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	public static Khachang find_Tenkh(String name)
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().get(Khachang.class,name);
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	
	public static Khachang find_Makh(String tenKh)
	{
		Khachang kh = new Khachang();
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			String hql = "select kh  from Khachang kh where kh.tenKh = :tenKh";
			Query query= session.createQuery(hql);
			query.setString("tenKh", tenKh);
			kh = (Khachang) query.uniqueResult();
			return kh;

		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}
		finally {
			session.close();
		}
	}
}
