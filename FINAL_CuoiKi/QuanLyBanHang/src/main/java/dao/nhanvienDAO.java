package dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import Util.HibernateUtil;
import pojo.Nhacungcap;
import pojo.Nhanvien;

public class nhanvienDAO {
	private final static SessionFactory sf = HibernateUtil.getSessionFactory();
	public static List<Nhanvien> laydanhsachnhanvien(){
		List<Nhanvien> ds = null;
		Session session= HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Query query = session.createQuery("select nv from Nhanvien nv");
			ds = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
		}finally {
			session.close();
		}
		return ds;
	} 
	public List<Nhanvien> findAll()
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().createCriteria(Nhanvien.class).list();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	public static   Nhanvien find_NV(String name)
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().get(Nhanvien.class, name);
		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}
	}

}
