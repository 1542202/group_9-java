package dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import Util.HibernateUtil;
import pojo.Chucvu;
import pojo.Donvi;
import pojo.Khohang;
import pojo.Nhacungcap;
import pojo.Nhaphang;
import pojo.NhaphangChitiet;
import pojo.Sanpham;


public class nhaphangDAO {
	private final static SessionFactory sf = HibernateUtil.getSessionFactory();
	public static List<NhaphangChitiet> laydanhsachnhaphangchitiet(String nhaphang){
		List<NhaphangChitiet> ds = null;
		Session session= HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Query query = session.createQuery("select nhct from NhaphangChitiet nhct where nhct.nhaphang = :nhaphang ");
			query.setString("nhaphang", nhaphang);
			ds =  query.list();
			
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			session.getTransaction().rollback();
		}finally {
			session.close();
		}
		return ds;
	} 
	public boolean saveorupdate_chitiet(NhaphangChitiet nhct)
	{
		try {
			sf.getCurrentSession().beginTransaction();
			sf.getCurrentSession().saveOrUpdate(nhct);
			sf.getCurrentSession().getTransaction().commit();
			return true;
		} catch (Exception e) {
			return false;
			// TODO: handle exception
		}
	}
	public boolean saveorupdate(Nhaphang nh) {
		try {
			sf.getCurrentSession().beginTransaction();
			sf.getCurrentSession().saveOrUpdate(nh);
			sf.getCurrentSession().getTransaction().commit();
			return true;
		} catch (Exception e) {
			sf.getCurrentSession().getTransaction().rollback();
			// TODO: handle exception
			return false;
		}
		
	}

	public List<Sanpham> finall_sanpham()
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().createCriteria(Sanpham.class).list();
		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}
	}
	public static Nhaphang tim_nhaphang(String name)
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().get(Nhaphang.class, name);
			
		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}
	}
	public static Donvi tim_donvi(String name)
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().get(Donvi.class, name);
			
		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}
	}
	public static Sanpham tim_TenSP(String name)
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().get(Sanpham.class, name);
			
		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}
	}
	public List<Nhaphang> findAll_nhaphang()
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().createCriteria(Nhaphang.class).list();
			
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	public List<Khohang> findAll()
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().createCriteria(Khohang.class).list();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	public List<Nhacungcap> findAll_NCC()
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().createCriteria(Nhacungcap.class).list();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	public NhaphangChitiet find_nhaphangchitiet(String name)
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().get(NhaphangChitiet.class, name);
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
	}
	public static  Nhacungcap find_NCC(String name)
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().get(Nhacungcap.class, name);
		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}
	}
}
