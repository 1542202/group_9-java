package dao;
import java.util.List;

import org.hibernate.SessionFactory;

import Util.*;
import pojo.*;

public class nhomhangDAO {
private SessionFactory sf= HibernateUtil.getSessionFactory();
public List<Nhomhang> findAll(){
	try {
		sf.getCurrentSession().beginTransaction();
		return sf.getCurrentSession().createCriteria(Nhomhang.class).list();
	} catch (Exception e) {
		return null;
	}
}
public Nhomhang find(String Ma){
	try {
		sf.getCurrentSession().beginTransaction();
		return (Nhomhang) sf.getCurrentSession().get(Nhomhang.class, Ma);
	} catch (Exception e) {
		// TODO: handle exception
		return null;
	}
}
public boolean saveorupdate(Nhomhang nhomhang){
	try {
		sf.getCurrentSession().beginTransaction();
		sf.getCurrentSession().saveOrUpdate(nhomhang);
		sf.getCurrentSession().getTransaction().commit();
		return true;
		
	} catch (Exception e) {
		sf.getCurrentSession().getTransaction().rollback();
		return false;
		// TODO: handle exception
	}
}
}
