package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import Util.HibernateUtil;
import pojo.Nhaphang;
import pojo.NhaphangChitiet;

public class thongke_nhaphangDAO {
	private final static SessionFactory sf = HibernateUtil.getSessionFactory();
	public static List<Nhaphang> ThongKeNhapHang(String ngay_tu, String ngay_den)
	{
		List<Nhaphang> ds = null;
		Session session= HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Query query = session.createQuery("select nh from Nhaphang nh where nh.ngayLap BETWEEN :ngay_tu AND :ngay_den ");
			query.setString("ngay_tu", ngay_tu);
			query.setString("ngay_den", ngay_den);
			ds =  query.list();
			
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			session.getTransaction().rollback();
		}finally {
			session.close();
		}
		return ds;
		
		
	}
	public static List<NhaphangChitiet> list_nhaphangchitiet()
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return  sf.getCurrentSession().createCriteria(NhaphangChitiet.class).list();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

}
