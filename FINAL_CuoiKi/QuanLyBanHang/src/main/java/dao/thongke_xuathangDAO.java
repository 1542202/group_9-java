package dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import Util.HibernateUtil;
import pojo.Nhaphang;
import pojo.NhaphangChitiet;
import pojo.Xuathang;
import pojo.XuathangChitiet;

public class thongke_xuathangDAO {
	private final static SessionFactory sf = HibernateUtil.getSessionFactory();
	public static List<Xuathang> ThongKeXuatHang(String ngay_tu, String ngay_den)
	{
		List<Xuathang> ds = null;
		Session session= HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Query query = session.createQuery("select nh from Xuathang nh where nh.ngayLap BETWEEN :ngay_tu AND :ngay_den ");
			query.setString("ngay_tu", ngay_tu);
			query.setString("ngay_den", ngay_den);
			ds =  query.list();
			
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			session.getTransaction().rollback();
		}finally {
			session.close();
		}
		return ds;
		
		
	}
	public static List<XuathangChitiet> list_xuathangchitiet()
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return  sf.getCurrentSession().createCriteria(XuathangChitiet.class).list();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
}
