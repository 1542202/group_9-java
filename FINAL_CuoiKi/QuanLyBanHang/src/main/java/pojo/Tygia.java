package pojo;
// Generated Sep 28, 2016 10:29:33 PM by Hibernate Tools 5.1.0.CR1

import java.math.BigDecimal;

/**
 * Tygia generated by hbm2java
 */
public class Tygia implements java.io.Serializable {

	private String tyGiaId;
	private String tenTyGia;
	private BigDecimal tyGiaQuyDoi;

	public Tygia() {
	}

	public Tygia(String tyGiaId) {
		this.tyGiaId = tyGiaId;
	}

	public Tygia(String tyGiaId, String tenTyGia, BigDecimal tyGiaQuyDoi) {
		this.tyGiaId = tyGiaId;
		this.tenTyGia = tenTyGia;
		this.tyGiaQuyDoi = tyGiaQuyDoi;
	}

	public String getTyGiaId() {
		return this.tyGiaId;
	}

	public void setTyGiaId(String tyGiaId) {
		this.tyGiaId = tyGiaId;
	}

	public String getTenTyGia() {
		return this.tenTyGia;
	}

	public void setTenTyGia(String tenTyGia) {
		this.tenTyGia = tenTyGia;
	}

	public BigDecimal getTyGiaQuyDoi() {
		return this.tyGiaQuyDoi;
	}

	public void setTyGiaQuyDoi(BigDecimal tyGiaQuyDoi) {
		this.tyGiaQuyDoi = tyGiaQuyDoi;
	}

}
