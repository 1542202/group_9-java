package DO_AN.JAVA;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.Panel;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.BevelBorder;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.ButtonGroup;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.GridLayout;
import net.miginfocom.swing.MigLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;

public class MainDialog {

	private JFrame frmPhnMmQun;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainDialog window = new MainDialog();
					window.frmPhnMmQun.setVisible(true);
					window.frmPhnMmQun.setExtendedState(JFrame.MAXIMIZED_BOTH);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainDialog() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		
		frmPhnMmQun = new JFrame();
		frmPhnMmQun.setResizable(false);
		frmPhnMmQun.setIconImage(Toolkit.getDefaultToolkit().getImage(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473067987_23.png")));
		frmPhnMmQun.setTitle("Phần Mềm Quản Lý Bán Hàng");
		frmPhnMmQun.setFont(new Font("Dialog", Font.PLAIN, 10));
		frmPhnMmQun.setBounds(100, 100, 1320, 500);
		frmPhnMmQun.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		
		
		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new LineBorder(new Color(0, 0, 0)));
		frmPhnMmQun.getContentPane().add(panel_7, BorderLayout.SOUTH);
		panel_7.setLayout(new MigLayout("", "[46px]", "[14px]"));
		
		JLabel lblNewLabel = new JLabel("Version 00001");
		panel_7.add(lblNewLabel, "cell 0 0,alignx left,aligny top");
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		tabbedPane.setForeground(Color.DARK_GRAY);
		frmPhnMmQun.getContentPane().add(tabbedPane, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		tabbedPane.addTab("Hệ Thống", null, panel, null);
		panel.setLayout(new MigLayout("", "[10px]", "[10px]"));
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		panel.add(panel_4, "flowx,cell 0 0,alignx left,aligny top");
		
		JButton btnNewButton = new JButton("Kết Thúc");
		btnNewButton.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/Exit.png")));
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_4.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Thông Tin");
		btnNewButton_1.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/Information.png")));
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_4.add(btnNewButton_1);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.add(panel_5, "cell 0 0");
		
		JButton btnNewButton_2 = new JButton("Phân Quyền");
		btnNewButton_2.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/Users.png")));
		btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_5.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Đổi Mật Khẩu");
		btnNewButton_3.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/ChangePassword.png")));
		btnNewButton_3.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_5.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Nhật Ký Hệ Thống");
		btnNewButton_4.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/BlogSystem.png")));
		btnNewButton_4.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_5.add(btnNewButton_4);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.add(panel_6, "cell 0 0");
		
		JButton btnNewButton_5 = new JButton("Sao Lưu");
		btnNewButton_5.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/Backup.png")));
		btnNewButton_5.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_6.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("Phục Hồi");
		btnNewButton_6.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/Restore.png")));
		btnNewButton_6.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_6.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("Sửa Chữa");
		btnNewButton_7.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/Repair.png")));
		btnNewButton_7.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_6.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("Kết Chuyển");
		btnNewButton_8.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1472899624_Switch.png")));
		btnNewButton_8.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_6.add(btnNewButton_8);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		tabbedPane.addTab("Doanh Mục", null, panel_1, null);
		panel_1.setLayout(new MigLayout("", "[10px]", "[10px]"));
		
		JPanel panel_8 = new JPanel();
		panel_8.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.add(panel_8, "flowx,cell 0 0,alignx left,aligny top");
		
		JButton btnNewButton_9 = new JButton("Nhập Danh Mục");
		btnNewButton_9.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/Restore.png")));
		btnNewButton_9.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_8.add(btnNewButton_9);
		
		JPanel panel_9 = new JPanel();
		panel_9.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.add(panel_9, "cell 0 0");
		
		JButton btnNewButton_10 = new JButton("Khu Vực");
		btnNewButton_10.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/area_chart.png")));
		btnNewButton_10.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_9.add(btnNewButton_10);
		
		JButton btnNewButton_11 = new JButton("Khách Hàng");
		btnNewButton_11.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473016787_office-87.png")));
		btnNewButton_11.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_9.add(btnNewButton_11);
		
		JButton btnNewButton_12 = new JButton("Nhà Cung Cấp");
		btnNewButton_12.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473018155_customers.png")));
		btnNewButton_12.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_9.add(btnNewButton_12);
		
		JPanel panel_10 = new JPanel();
		panel_10.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.add(panel_10, "cell 0 0");
		
		JButton btnNewButton_13 = new JButton("Kho Hàng");
		btnNewButton_13.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473016858_warehouse.png")));
		btnNewButton_13.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_10.add(btnNewButton_13);
		
		JButton btnNewButton_14 = new JButton("Đơn Vị Tính");
		btnNewButton_14.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473016985_scales_of_Balance.png")));
		btnNewButton_14.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_10.add(btnNewButton_14);
		
		JButton btnNewButton_15 = new JButton("Nhóm Hàng");
		btnNewButton_15.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/or.png")));
		btnNewButton_15.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_10.add(btnNewButton_15);
		
		JButton btnNewButton_16 = new JButton("Hàng Hóa");
		btnNewButton_16.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/product.png")));
		btnNewButton_16.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_10.add(btnNewButton_16);
		
		JButton btnNewButton_17 = new JButton("In Mã Vạch");
		btnNewButton_17.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473018228_stock_id.png")));
		btnNewButton_17.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_10.add(btnNewButton_17);
		
		JButton btnNewButton_18 = new JButton("Tỷ Giá");
		btnNewButton_18.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473018267_money_bag.png")));
		btnNewButton_18.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_10.add(btnNewButton_18);
		
		JPanel panel_11 = new JPanel();
		panel_11.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.add(panel_11, "cell 0 0");
		
		JButton btnNewButton_19 = new JButton("Bộ Phận");
		btnNewButton_19.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473018340_binary-tree.png")));
		btnNewButton_19.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_11.add(btnNewButton_19);
		
		JButton btnNewButton_20 = new JButton("Nhân Viên");
		btnNewButton_20.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473018377_Manager.png")));
		btnNewButton_20.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_11.add(btnNewButton_20);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		tabbedPane.addTab("Chức Năng", null, panel_2, null);
		panel_2.setLayout(new MigLayout("", "[10px]", "[10px]"));
		
		JPanel panel_12 = new JPanel();
		panel_12.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_2.add(panel_12, "flowx,cell 0 0,alignx left,aligny top");
		
		JButton btnNewButton_21 = new JButton("Mua Hàng");
		btnNewButton_21.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/Buy.png")));
		btnNewButton_21.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_12.add(btnNewButton_21);
		
		JButton btnNewButton_22 = new JButton("Bán Hàng");
		btnNewButton_22.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/Sale.png")));
		btnNewButton_22.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_12.add(btnNewButton_22);
		
		JPanel panel_13 = new JPanel();
		panel_13.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_2.add(panel_13, "cell 0 0");
		
		JButton btnNewButton_23 = new JButton("Tồn Kho");
		btnNewButton_23.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/Inventory.png")));
		btnNewButton_23.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_13.add(btnNewButton_23);
		
		JButton btnNewButton_24 = new JButton("Chuyển Kho");
		btnNewButton_24.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/Swap_Invetory.png")));
		btnNewButton_24.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_13.add(btnNewButton_24);
		
		JButton btnNewButton_25 = new JButton("Nhập Số Dư Đầu Kỳ");
		btnNewButton_25.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473067927_note_1.png")));
		btnNewButton_25.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_13.add(btnNewButton_25);
		
		JPanel panel_14 = new JPanel();
		panel_14.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_2.add(panel_14, "cell 0 0");
		
		JButton btnNewButton_26 = new JButton("Thu Tiền");
		btnNewButton_26.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473067987_23.png")));
		btnNewButton_26.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_14.add(btnNewButton_26);
		
		JButton btnNewButton_27 = new JButton("Trả Tiền");
		btnNewButton_27.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473067987_23.png")));
		btnNewButton_27.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_14.add(btnNewButton_27);
		
		JPanel panel_15 = new JPanel();
		panel_15.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_2.add(panel_15, "cell 0 0");
		
		JButton btnNewButton_28 = new JButton(" Báo Cáo Kho Hàng");
		btnNewButton_28.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473068087_reports.png")));
		btnNewButton_28.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_15.add(btnNewButton_28);
		
		JButton btnNewButton_29 = new JButton("Báo Cáo Bán Hàng");
		btnNewButton_29.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473068063_sales-report.png")));
		btnNewButton_29.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_15.add(btnNewButton_29);
		
		JButton btnNewButton_30 = new JButton("Chứng Từ");
		btnNewButton_30.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473068242_icon-57.png")));
		btnNewButton_30.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_15.add(btnNewButton_30);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new LineBorder(new Color(0, 0, 0)));
		tabbedPane.addTab("Trợ Giúp", null, panel_3, null);
		panel_3.setLayout(new MigLayout("", "[10px]", "[10px]"));
		
		JPanel panel_16 = new JPanel();
		panel_16.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_3.add(panel_16, "flowx,cell 0 0,alignx left,aligny top");
		
		JButton btnNewButton_31 = new JButton("Hổ Trợ Trực Tuyến");
		btnNewButton_31.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473069258_supportmale.png")));
		btnNewButton_31.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_16.add(btnNewButton_31);
		
		JButton btnNewButton_32 = new JButton("Hướng Dẫn Sử Dụng");
		btnNewButton_32.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473069314_Help.png")));
		btnNewButton_32.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_16.add(btnNewButton_32);
		
		JButton btnNewButton_33 = new JButton("Liên Hệ");
		btnNewButton_33.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473069342_preferences-contact-list.png")));
		btnNewButton_33.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_16.add(btnNewButton_33);
		
		JPanel panel_17 = new JPanel();
		panel_17.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_3.add(panel_17, "cell 0 0");
		
		JButton btnNewButton_34 = new JButton("Đăng Ký");
		btnNewButton_34.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473069415_encrypted.png")));
		btnNewButton_34.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_17.add(btnNewButton_34);
		
		JButton btnNewButton_35 = new JButton("Thông Tin");
		btnNewButton_35.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/Information.png")));
		btnNewButton_35.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_17.add(btnNewButton_35);
		
		JButton btnNewButton_36 = new JButton("Cập Nhật");
		btnNewButton_36.setIcon(new ImageIcon(MainDialog.class.getResource("/DO_AN/JAVA/Image/1473069539_system-software-update.png")));
		btnNewButton_36.setFont(new Font("Tahoma", Font.PLAIN, 9));
		panel_17.add(btnNewButton_36);
		
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		frmPhnMmQun.getContentPane().add(tabbedPane_1, BorderLayout.CENTER);
	}
}
