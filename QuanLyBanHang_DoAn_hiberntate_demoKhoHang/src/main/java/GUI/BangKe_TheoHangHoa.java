package GUI;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.GridBagConstraints;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class BangKe_TheoHangHoa extends JInternalFrame {
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BangKe_TheoHangHoa frame = new BangKe_TheoHangHoa();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BangKe_TheoHangHoa() {
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setMaximizable(true);
		setClosable(true);
		setTitle("Bảng Kê Theo Hàng Hóa");
		setBounds(100, 100, 450, 300);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		getContentPane().add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JComboBox comboBox = new JComboBox();
		comboBox.addItem("Tùy Chọn");
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 0, 5);
		gbc_comboBox.anchor = GridBagConstraints.WEST;
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 0;
		panel.add(comboBox, gbc_comboBox);
		
		JLabel lblT = new JLabel("Từ");
		GridBagConstraints gbc_lblT = new GridBagConstraints();
		gbc_lblT.insets = new Insets(0, 0, 0, 5);
		gbc_lblT.anchor = GridBagConstraints.EAST;
		gbc_lblT.gridx = 1;
		gbc_lblT.gridy = 0;
		panel.add(lblT, gbc_lblT);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.addItem("1/1/2017");
		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.insets = new Insets(0, 0, 0, 5);
		gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_1.gridx = 2;
		gbc_comboBox_1.gridy = 0;
		panel.add(comboBox_1, gbc_comboBox_1);
		
		JLabel lbln = new JLabel("Đến");
		GridBagConstraints gbc_lbln = new GridBagConstraints();
		gbc_lbln.insets = new Insets(0, 0, 0, 5);
		gbc_lbln.anchor = GridBagConstraints.EAST;
		gbc_lbln.gridx = 3;
		gbc_lbln.gridy = 0;
		panel.add(lbln, gbc_lbln);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.addItem("1/2/2017");
		GridBagConstraints gbc_comboBox_2 = new GridBagConstraints();
		gbc_comboBox_2.insets = new Insets(0, 0, 0, 5);
		gbc_comboBox_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_2.gridx = 4;
		gbc_comboBox_2.gridy = 0;
		panel.add(comboBox_2, gbc_comboBox_2);
		
		JButton btnXem = new JButton("Xem");
		btnXem.setIcon(new ImageIcon("view.png"));
		GridBagConstraints gbc_btnXem = new GridBagConstraints();
		gbc_btnXem.insets = new Insets(0, 0, 0, 5);
		gbc_btnXem.gridx = 5;
		gbc_btnXem.gridy = 0;
		panel.add(btnXem, gbc_btnXem);
		
		JButton btnToMi = new JButton("Tạo Mới");
		btnToMi.setIcon(new ImageIcon("HinhAnh\\addIcon.png"));
		GridBagConstraints gbc_btnToMi = new GridBagConstraints();
		gbc_btnToMi.insets = new Insets(0, 0, 0, 5);
		gbc_btnToMi.gridx = 6;
		gbc_btnToMi.gridy = 0;
		panel.add(btnToMi, gbc_btnToMi);
		
		JButton btnSa = new JButton("Sửa");
		btnSa.setIcon(new ImageIcon("HinhAnh\\update.png"));
		GridBagConstraints gbc_btnSa = new GridBagConstraints();
		gbc_btnSa.insets = new Insets(0, 0, 0, 5);
		gbc_btnSa.gridx = 7;
		gbc_btnSa.gridy = 0;
		panel.add(btnSa, gbc_btnSa);
		
		JButton btnXa = new JButton("Xóa");
		btnXa.setIcon(new ImageIcon("HinhAnh\\removeIcon.png"));
		GridBagConstraints gbc_btnXa = new GridBagConstraints();
		gbc_btnXa.insets = new Insets(0, 0, 0, 5);
		gbc_btnXa.gridx = 8;
		gbc_btnXa.gridy = 0;
		panel.add(btnXa, gbc_btnXa);
		
		JButton btnIn = new JButton("In");
		btnIn.setIcon(new ImageIcon("HinhAnh\\print.png"));
		GridBagConstraints gbc_btnIn = new GridBagConstraints();
		gbc_btnIn.insets = new Insets(0, 0, 0, 5);
		gbc_btnIn.gridx = 9;
		gbc_btnIn.gridy = 0;
		panel.add(btnIn, gbc_btnIn);
		
		JButton btnXut = new JButton("Xuất");
		btnXut.setIcon(new ImageIcon("HinhAnh\\Export.jpg"));
		GridBagConstraints gbc_btnXut = new GridBagConstraints();
		gbc_btnXut.insets = new Insets(0, 0, 0, 5);
		gbc_btnXut.gridx = 10;
		gbc_btnXut.gridy = 0;
		panel.add(btnXut, gbc_btnXut);
		
		JButton btnng = new JButton("Đóng");
		btnng.setIcon(new ImageIcon("HinhAnh\\close.png"));
		GridBagConstraints gbc_btnng = new GridBagConstraints();
		gbc_btnng.gridx = 11;
		gbc_btnng.gridy = 0;
		panel.add(btnng, gbc_btnng);
		
		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		getContentPane().add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{434, 0};
		gbl_panel_1.rowHeights = new int[]{80, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
			},
			new String[] {
				"Chứng Từ", "Ngày", "Mã Hàng", "Tên Hàng", "Kho Hàng", "Đơn Vị", "Số Lượng", "Đơn Giá", "Thành Tiền"
			}
		));
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;
		JScrollPane scrollPane = new JScrollPane(table);
		panel_1.add(scrollPane, gbc);

	}

}
