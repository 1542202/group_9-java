package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.util.*;
import org.hibernate.annotations.Entity;
import javax.swing.*;
import dao.khohangDAO;
import pojo.Khohang;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import controller.*;
public class KhoHang {

	//dmKhoHang_controller controller = new dmKhoHang_controller();
	private JFrame frame;
	public static   JTable tableKH;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KhoHang window = new KhoHang();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public KhoHang() {
		initialize();
		dmKhoHang_controller.LoadData();
	}

	
	public static void reload()
	{
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.fireTableDataChanged();
		dmKhoHang_controller.LoadData();
		
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 581, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JButton btnThm = new JButton("Thêm");
		btnThm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				danhmuckhohang_them dmkh = new danhmuckhohang_them();
				dmkh.setVisible(true);
//				tableKH.removeAll();
				
			}
		});
//		btnThm.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/them.png")));
		panel.add(btnThm);
		
		JButton btnXa = new JButton("Xóa");
		btnXa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dmKhoHang_controller.xoaKhoHang();
//				
			}
		});
//		btnXa.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/delete.png")));
		panel.add(btnXa);
		
		JButton btnSa = new JButton("Sửa");
		btnSa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				khohangDAO khd = new khohangDAO();
				Khohang kh = new Khohang();
				danhmuckhohang_sua danhmuckhohang_sua= new danhmuckhohang_sua();
				danhmuckhohang_sua.setVisible(true);
				String un = tableKH.getValueAt(tableKH.getSelectedRow(), 0).toString();
				 kh = khd.find(un);
				danhmuckhohang_sua.txtMa.setText(kh.getKhoId());
				danhmuckhohang_sua.txtDC.setText(kh.getDiaChi());
				danhmuckhohang_sua.txtDT.setText(kh.getDt()+"");
				danhmuckhohang_sua.txtGhiChu.setText(kh.getGhiChu());
				danhmuckhohang_sua.cbbNVQL.setSelectedItem(kh.getNguoiLienHe());
				danhmuckhohang_sua.txtTen.setText(kh.getTenKho());
				danhmuckhohang_sua.txtFax.setText(kh.getFax()+"");
			}
		});
//		btnSa.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/edit.png")));
		panel.add(btnSa);
		
		JButton btnNp = new JButton("Nạp");
		btnNp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				reload();
			}
		});
//		btnNp.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/napvao.png")));
		panel.add(btnNp);
		
		JButton btnXut = new JButton("Xuất");
		btnXut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dmKhoHang_controller.xuatfile(frame);

				
				
				
			}
		});
//		btnXut.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/xuat.png")));
		panel.add(btnXut);
		
		JButton btnNhp = new JButton("Nhập");
//		btnNhp.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/nhap.png")));
		panel.add(btnNhp);
		
		JButton btnng = new JButton("Đóng");
//		btnng.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/dong.png")));
		panel.add(btnng);
		
		JScrollPane scrollPane = new JScrollPane();
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		tableKH = new JTable();

		tableKH.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
			},
			new String[] {
				"Id", "M\u00E3", "T\u00EAn", "Li\u00EAn H\u1EC7", "\u0110\u1ECBa ch\u1EC9", "\u0110i\u1EC7n tho\u1EA1i", "K\u00ED hi\u1EC7u", "Ghi ch\u00FA", "C\u00F2n qu\u1EA3n l\u00FD"
			}
		));
		scrollPane.setViewportView(tableKH);
	}

}
