package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.GridBagConstraints;
import javax.swing.JTabbedPane;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JToolBar;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JDesktopPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Main extends JFrame {
	JDesktopPane desktopPane = new JDesktopPane();
	
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
//					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 894, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.insets = new Insets(0, 0, 5, 0);
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridx = 0;
		gbc_tabbedPane.gridy = 0;
		panel.add(tabbedPane, gbc_tabbedPane);
		
		JPanel panel_hethong = new JPanel();
		tabbedPane.addTab("Hệ Thống", null, panel_hethong, null);
		panel_hethong.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JToolBar toolBar = new JToolBar();
		panel_hethong.add(toolBar);
		
		JButton btnKetThuc = new JButton("Kết Thúc");
		btnKetThuc.setIcon(new ImageIcon("HinhAnh\\Exit.png"));
		toolBar.add(btnKetThuc);
		JButton btnThongTin =  new JButton("Thông Tin");
		toolBar.add(btnThongTin);
		
		JToolBar toolBar_1 = new JToolBar();
		panel_hethong.add(toolBar_1);
		
		JButton btnPhanQuyen = new JButton("Phân Quyền");
		btnPhanQuyen.setIcon(new ImageIcon("HinhAnh\\Users.png"));
		toolBar_1.add(btnPhanQuyen);
		
		JButton btnDoiMK = new JButton("Đổi Mật Khẩu");
		btnDoiMK.setIcon(new ImageIcon("HinhAnh\\ChangPassword.png"));
		toolBar_1.add(btnDoiMK);
		
		JButton btnLog4j = new JButton("Nhật Ký Hệ Thống");
		btnLog4j.setIcon(new ImageIcon("HinhAnh\\BlogSystem.png"));
		toolBar_1.add(btnLog4j);
		
		JToolBar toolBar_2 = new JToolBar();
		panel_hethong.add(toolBar_2);
		
		JButton btnSaoLuu = new JButton("Sao Lưu");
		btnSaoLuu.setIcon(new ImageIcon("HinhAnh\\Backup.png"));
		toolBar_2.add(btnSaoLuu);
		
		JButton btnPhucHoi = new JButton("Phục Hồi");
		btnPhucHoi.setIcon(new ImageIcon("HinhAnh\\Restore.png"));
		toolBar_2.add(btnPhucHoi);
		
		JButton btnSuaChua = new JButton("Sửa Chữa");
		btnSuaChua.setIcon(new ImageIcon("HinhAnh\\Repair.png"));
		toolBar_2.add(btnSuaChua);
		
		JButton btnKetChuyen = new JButton("Kết Chuyển");
		btnKetChuyen.setIcon(new ImageIcon("HinhAnh\\Switch.png"));
		toolBar_2.add(btnKetChuyen);
		
		JPanel panel_danhmuc = new JPanel();
		tabbedPane.addTab("Danh Mục", null, panel_danhmuc, null);
		panel_danhmuc.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JToolBar toolBar_3 = new JToolBar();
		panel_danhmuc.add(toolBar_3);
		
		JButton btnNhapDN = new JButton("Nhập Danh Mục Từ Excel");
		btnNhapDN.setIcon(new ImageIcon("HinhAnh\\Excel.png"));
		toolBar_3.add(btnNhapDN);
		
		JToolBar toolBar_4 = new JToolBar();
		panel_danhmuc.add(toolBar_4);
		
		JButton btnKhuVuc = new JButton("KhuVuc");
		btnKhuVuc.setIcon(new ImageIcon("HinhAnh\\area_chart.png"));
		toolBar_4.add(btnKhuVuc);
		
		JButton btnKhachHang = new JButton("Khách Hàng");
		btnKhachHang.setIcon(new ImageIcon("HinhAnh\\office-87.png"));
		toolBar_4.add(btnKhachHang);
		
		JButton btnNhaCungCap = new JButton("Nhà Cung Cấp");
		btnNhaCungCap.setIcon(new ImageIcon("HinhAnh\\customers.png"));
		toolBar_4.add(btnNhaCungCap);
		
		JToolBar toolBar_5 = new JToolBar();
		panel_danhmuc.add(toolBar_5);
		
		JButton btnKhoHang = new JButton("Kho Hàng");
		btnKhoHang.setIcon(new ImageIcon("HinhAnh\\warehouse.png"));
		toolBar_5.add(btnKhoHang);
		
		JButton btnDonViTinh = new JButton("Đơn Vị Tính");
		btnDonViTinh.setIcon(new ImageIcon("HinhAnh\\scales_of_Balance.png"));
		toolBar_5.add(btnDonViTinh);
		
		JButton btnNhomHang = new JButton("Nhóm Hàng");
		btnNhomHang.setIcon(new ImageIcon("HinhAnh\\or.png"));
		toolBar_5.add(btnNhomHang);
		
		JButton btnHangHoa = new JButton("Hàng Hóa");
		btnHangHoa.setIcon(new ImageIcon("HinhAnh\\product.png"));
		toolBar_5.add(btnHangHoa);
		
		JButton btnInMaVach = new JButton("In Mã Vạch");
		btnInMaVach.setIcon(new ImageIcon("HinhAnh\\stock_id.png"));
		toolBar_5.add(btnInMaVach);
		
		JButton btnTGi = new JButton("Tỷ Giá");
		btnTGi.setIcon(new ImageIcon("HinhAnh\\money_bag.png"));
		toolBar_5.add(btnTGi);
		
		JToolBar toolBar_6 = new JToolBar();
		panel_danhmuc.add(toolBar_6);
		
		JButton btnBPhn = new JButton("Bộ Phận");
		btnBPhn.setIcon(new ImageIcon("HinhAnh\\binary-tree.png"));
		toolBar_6.add(btnBPhn);
		
		JButton btnNhnVin = new JButton("Nhân Viên");
		btnNhnVin.setIcon(new ImageIcon("HinhAnh\\Manager.png"));
		toolBar_6.add(btnNhnVin);
		
		JPanel panel_chucnang = new JPanel();
		tabbedPane.addTab("Chức Năng", null, panel_chucnang, null);
		panel_chucnang.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JToolBar toolBar_7 = new JToolBar();
		panel_chucnang.add(toolBar_7);
		
		JButton btnMuaHang = new JButton("Mua Hàng");
		btnMuaHang.setIcon(new ImageIcon("HinhAnh\\icon_cart.png"));
		toolBar_7.add(btnMuaHang);
		
		JButton btnHangHoa_CN = new JButton("Hàng Hóa");
		btnHangHoa_CN.setIcon(new ImageIcon("HinhAnh\\downloadicon.png"));
		toolBar_7.add(btnHangHoa_CN);
		
		JToolBar toolBar_8 = new JToolBar();
		panel_chucnang.add(toolBar_8);
		
		JButton btnTonKho = new JButton("Tồn Kho");
		btnTonKho.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				TonKho tonKho = new TonKho();
				desktopPane.add(tonKho);
				tonKho.show();
				
			}
		});
		btnTonKho.setIcon(new ImageIcon("HinhAnh\\home_16.png"));
		toolBar_8.add(btnTonKho);
		
		JButton btnChuyenKho = new JButton("Chuyển Kho");
		btnChuyenKho.setIcon(new ImageIcon("HinhAnh\\go-into-icon.png"));
		toolBar_8.add(btnChuyenKho);
		
		JButton btnNhapSD = new JButton("Nhập Số Dư Đầu Kỳ");
		btnNhapSD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NhapSoDuDauKy nhapSoDuDauKy = new NhapSoDuDauKy();
				desktopPane.add(nhapSoDuDauKy);
				nhapSoDuDauKy.show();
			}
		});
		btnNhapSD.setIcon(new ImageIcon("HinhAnh\\Books2.png"));
		toolBar_8.add(btnNhapSD);
		
		JToolBar toolBar_9 = new JToolBar();
		panel_chucnang.add(toolBar_9);
		
		JButton btnThuTien = new JButton("Thu Tiền");
		btnThuTien.setIcon(new ImageIcon("HinhAnh\\icon.png"));
		toolBar_9.add(btnThuTien);
		
		JButton btnTraTin = new JButton("Trả Tiền");
		btnTraTin.setIcon(new ImageIcon("HinhAnh\\icon.png"));
		toolBar_9.add(btnTraTin);
		
		JToolBar toolBar_10 = new JToolBar();
		panel_chucnang.add(toolBar_10);
		
		JButton btnBaoCaoKho = new JButton("Báo Cáo Kho Hàng");
		btnBaoCaoKho.setIcon(new ImageIcon("HinhAnh\\report.png"));
		toolBar_10.add(btnBaoCaoKho);
		
		JButton btnBaoCaoBan = new JButton("Báo Cáo Bán Hàng");
		btnBaoCaoBan.setIcon(new ImageIcon("HinhAnh\\report.png"));
		toolBar_10.add(btnBaoCaoBan);
		
		JButton btnChungTu = new JButton("Chứng Từ");
		btnChungTu.setIcon(new ImageIcon("HinhAnh\\Report2.png"));
		toolBar_10.add(btnChungTu);
		
		JPanel panel_trogiup = new JPanel();
		tabbedPane.addTab("Trợ Giúp", null, panel_trogiup, null);
		panel_trogiup.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JToolBar toolBar_11 = new JToolBar();
		panel_trogiup.add(toolBar_11);
		
		JButton btnHoTro = new JButton("Hổ Trợ Trực Tuyến");
		btnHoTro.setIcon(new ImageIcon("HinhAnh\\Yahoo-Messenger-icon.png"));
		toolBar_11.add(btnHoTro);
		
		JButton btnHuongDan = new JButton("Hướng Dẫn Sử Dụng");
		btnHuongDan.setIcon(new ImageIcon("HinhAnh\\sign-question-icon.png"));
		toolBar_11.add(btnHuongDan);
		
		JButton btnLienHe = new JButton("Liên Hệ");
		btnLienHe.setIcon(new ImageIcon("HinhAnh\\customers.png"));
		toolBar_11.add(btnLienHe);
		
		JToolBar toolBar_12 = new JToolBar();
		panel_trogiup.add(toolBar_12);
		
		JButton btnDK = new JButton("Đăng Ký");
		btnDK.setIcon(new ImageIcon("HinhAnh\\Key.png"));
		toolBar_12.add(btnDK);
		
		JButton btnThngTin = new JButton("Thông Tin");
		btnThngTin.setIcon(new ImageIcon("HinhAnh\\Information-icon.png"));
		toolBar_12.add(btnThngTin);
		
		JButton btnCapNhat = new JButton("Cập Nhật");
		btnCapNhat.setIcon(new ImageIcon("HinhAnh\\Apps-system-software-update-icon.png"));
		toolBar_12.add(btnCapNhat);
		
		
		
		
		GridBagConstraints gbc_desktopPane = new GridBagConstraints();
		gbc_desktopPane.fill = GridBagConstraints.BOTH;
		gbc_desktopPane.gridx = 0;
		gbc_desktopPane.gridy = 1;
		contentPane.add(desktopPane, gbc_desktopPane);
		
		
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
