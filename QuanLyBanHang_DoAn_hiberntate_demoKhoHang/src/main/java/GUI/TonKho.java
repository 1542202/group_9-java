package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JComboBox;
import java.awt.Insets;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class TonKho extends JInternalFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TonKho frame = new TonKho();
				
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TonKho() {
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setTitle("Tồn Kho\r\n");
		setMaximizable(true);
		setClosable(true);
		
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{424, 0};
		gbl_contentPane.rowHeights = new int[]{35, 30, 177, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.anchor = GridBagConstraints.WEST;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{53, 55, 43, 59, 0};
		gbl_panel.rowHeights = new int[]{23, 0};
		gbl_panel.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JButton btnXem = new JButton("Xem");
		btnXem.setIcon(new ImageIcon("HinhAnh\\view.png"));
		GridBagConstraints gbc_btnXem = new GridBagConstraints();
		gbc_btnXem.fill = GridBagConstraints.BOTH;
		gbc_btnXem.insets = new Insets(0, 0, 0, 5);
		gbc_btnXem.gridx = 0;
		gbc_btnXem.gridy = 0;
		panel.add(btnXem, gbc_btnXem);
		
		JButton btnXua = new JButton("Xuất");
		btnXua.setIcon(new ImageIcon("HinhAnh\\Export.JPG"));
		GridBagConstraints gbc_btnXua = new GridBagConstraints();
		gbc_btnXua.fill = GridBagConstraints.BOTH;
		gbc_btnXua.insets = new Insets(0, 0, 0, 5);
		gbc_btnXua.gridx = 1;
		gbc_btnXua.gridy = 0;
		panel.add(btnXua, gbc_btnXua);
		
		JButton btnIn = new JButton("In");
		btnIn.setIcon(new ImageIcon("HinhAnh\\print.png"));
		GridBagConstraints gbc_btnIn = new GridBagConstraints();
		gbc_btnIn.fill = GridBagConstraints.BOTH;
		gbc_btnIn.insets = new Insets(0, 0, 0, 5);
		gbc_btnIn.gridx = 2;
		gbc_btnIn.gridy = 0;
		panel.add(btnIn, gbc_btnIn);
		
		JButton btnng = new JButton("Đóng");
		btnng.setIcon(new ImageIcon("HinhAnh\\close.png"));
		GridBagConstraints gbc_btnng = new GridBagConstraints();
		gbc_btnng.fill = GridBagConstraints.BOTH;
		gbc_btnng.gridx = 3;
		gbc_btnng.gridy = 0;
		panel.add(btnng, gbc_btnng);
		
		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.anchor = GridBagConstraints.WEST;
		gbc_panel_1.fill = GridBagConstraints.VERTICAL;
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		contentPane.add(panel_1, gbc_panel_1);
		
		JComboBox comboBox = new JComboBox();
		panel_1.add(comboBox);
		comboBox.addItem("kho Hang");
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(null);
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 2;
		contentPane.add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{424, 0};
		gbl_panel_2.rowHeights = new int[]{27, 0};
		gbl_panel_2.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		table = new JTable();
		table.setBorder(null);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
			},
			new String[] {
				"Mã Hàng", "Tên Hàng", "Đơn Vị", "Số Lượng", "Còn Quản lý"
			}
		));
		JScrollPane scrollPane = new JScrollPane(table);
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		panel_2.add(scrollPane, gbc_scrollPane);
	}

}
