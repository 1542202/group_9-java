package controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import dao.khohangDAO;
import pojo.Khohang;
import GUI.KhoHang;
import GUI.danhmuckhohang_sua;
import GUI.danhmuckhohang_them;
public class dmKhoHang_controller {
	
	//private KhoHang kh = new KhoHang();
	
	public static void xuatfile(JFrame frame )
	{
		JFileChooser chooser  = new JFileChooser();
        int i = chooser.showSaveDialog(frame);
        if(i == JFileChooser.APPROVE_OPTION)
        {
       	 File file = chooser.getSelectedFile();
       	 try {
				FileWriter out = new FileWriter(file+".xls");
				BufferedWriter writer =new BufferedWriter(out);
				DefaultTableModel model = (DefaultTableModel) KhoHang.tableKH.getModel();
				for (int j = 0; j < model.getColumnCount(); j++) {
					writer.write(model.getColumnName(j)+"\t");
				}
				writer.write("\n");
				for (int k = 0; k < model.getRowCount(); k++) {
					for (int j = 0; j < model.getColumnCount(); j++) {
						writer.write(model.getValueAt(k, j) + "\t");
					}
					writer.write("\n");
				}
				writer.close();
				JOptionPane.showConfirmDialog(frame, "Save");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				JOptionPane.showConfirmDialog(frame, "Can not Save");
				e.printStackTrace();
			}
        }

		
		
		
	}
	
	public static void suaKhoHang()
	{
		danhmuckhohang_sua dmskh =null;
		khohangDAO nvd = new khohangDAO();
		Khohang kh  = nvd.find(dmskh.txtMa.getText());
		kh.setKhoId(dmskh.txtMa.getText());
		kh.setTenKho(dmskh.txtTen.getText());
		kh.setDiaChi(dmskh.txtDC.getText());
		kh.setDt(Integer.parseInt(dmskh.txtDT.getText()));
		kh.setFax(Integer.parseInt(dmskh.txtFax.getText()));
		kh.setGhiChu(dmskh.txtGhiChu.getText());
		kh.setNguoiLienHe(String.valueOf(dmskh.cbbNVQL.getSelectedItem()));
		
		if(nvd.update(kh))
		{
			LoadData();
			JOptionPane.showConfirmDialog(null, "Sữa Thành Công", "Xác Nhận", JOptionPane.CLOSED_OPTION);
			
			
		}
		else
			JOptionPane.showConfirmDialog(null, "Sữa Thất bại","Xác Nhận",JOptionPane.CLOSED_OPTION);
			
	}
		
	
	public static void themKhoHang()
	{
		danhmuckhohang_them dmhthem = null ;
		khohangDAO nvd = new khohangDAO();
		Khohang kh = new Khohang();
		kh.setKhoId(dmhthem.txtMa.getText());
		kh.setTenKho(dmhthem.txtTen.getText());
		kh.setDiaChi(dmhthem.txtDC.getText());
		kh.setDt(Integer.parseInt(dmhthem.txtDT.getText()));
		
		kh.setGhiChu(dmhthem.txtGhiChu.getText());
		kh.setNguoiLienHe(String.valueOf(dmhthem.cbbNVQL.getSelectedItem()));
		kh.setFax(Integer.parseInt(dmhthem.txtFax.getText()));
		if(nvd.saveorupdate(kh))
		{
			LoadData();
			JOptionPane.showConfirmDialog(null, "Thêm Thành Công","ok",JOptionPane.CLOSED_OPTION);
			
			
		}
		else
			JOptionPane.showConfirmDialog(null, "Thêm Thất bại","ok",JOptionPane.CLOSED_OPTION);
			
	}
	
	public static void xoaKhoHang()
	{
		khohangDAO khd = new khohangDAO();
		int cf = JOptionPane.showConfirmDialog(null, "Bạn có chắc xóa?", "Xác Nhận", JOptionPane.YES_NO_OPTION);
		if(cf == JOptionPane.YES_OPTION)
		{
			
			String un = KhoHang.tableKH.getValueAt(KhoHang.tableKH.getSelectedRow(), 0).toString();
			if(khd.delete(khd.find(un)))
				{
					JOptionPane.showConfirmDialog(null, "Xóa thành công","ok",JOptionPane.CLOSED_OPTION);
					LoadData();
				}
			else
			{
				JOptionPane.showConfirmDialog(null, "Xóa Thất bại","ok",JOptionPane.CLOSED_OPTION);
			}
		}
	}
	public static void LoadData()
	{
		
		khohangDAO khd = new khohangDAO();
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.addColumn("Mã");
		dtm.addColumn("Tên");
		dtm.addColumn("Fax");
		dtm.addColumn("Địa Chỉ");
		dtm.addColumn("Điên Thoại");
		dtm.addColumn("Ghi Chú");
		for(Khohang kh : khd.laydanhsachkhohang())
		{
			dtm.addRow(new Object[]{kh.getKhoId(), kh.getTenKho(), kh.getFax(), kh.getDiaChi(), kh.getDt(), kh.getGhiChu()});
		}
		
		KhoHang.tableKH.setModel(dtm);
		KhoHang.tableKH.repaint();
		KhoHang.tableKH.revalidate();
		
		
	}
}

