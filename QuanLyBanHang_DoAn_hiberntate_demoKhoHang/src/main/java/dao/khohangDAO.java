package dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pojo.Khohang;
import Util.HibernateUtil;

public class khohangDAO {
	private final SessionFactory sf = HibernateUtil.getSessionFactory();
	public static List<Khohang> laydanhsachkhohang(){
		List<Khohang> ds = null;
		Session session= HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Query query = session.createQuery("select kh from Khohang kh");
			ds = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
		}finally {
			session.close();
		}
		return ds;
	} 
	
	public List<Khohang> findAll()
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().createCriteria(Khohang.class).list();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	public Khohang find(String name)
	{
		try {
			sf.getCurrentSession().beginTransaction();
			return sf.getCurrentSession().get(Khohang.class, name);
		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}
	}
	
	public boolean delete(Khohang kh) {
		try {
			sf.getCurrentSession().beginTransaction();
			sf.getCurrentSession().delete(kh);
			sf.getCurrentSession().getTransaction().commit();
			return true;
		} catch (Exception e) {
			sf.getCurrentSession().getTransaction().rollback();
			// TODO: handle exception
			return false;
		}
		
	}
	public boolean saveorupdate(Khohang kh) {
		try {
			sf.getCurrentSession().beginTransaction();
			sf.getCurrentSession().saveOrUpdate(kh);
			sf.getCurrentSession().getTransaction().commit();
			return true;
		} catch (Exception e) {
			sf.getCurrentSession().getTransaction().rollback();
			// TODO: handle exception
			return false;
		}
		
	}
	public boolean update(Khohang kh) {
		try {
			sf.getCurrentSession().beginTransaction();
			sf.getCurrentSession().update(kh);
			sf.getCurrentSession().getTransaction().commit();
			return true;
		} catch (Exception e) {
			sf.getCurrentSession().getTransaction().rollback();
			// TODO: handle exception
			return false;
		}
		
	}
}
