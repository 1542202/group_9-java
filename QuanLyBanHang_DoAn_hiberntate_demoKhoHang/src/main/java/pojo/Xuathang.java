package pojo;
// Generated Sep 21, 2016 5:29:25 PM by Hibernate Tools 5.1.0.Alpha1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Xuathang generated by hbm2java
 */
public class Xuathang implements java.io.Serializable {

	private String xuatHangId;
	private Khachang khachang;
	private Nhanvien nhanvien;
	private Date ngayLap;
	private String khoId;
	private Integer dt;
	private String soHoDonVat;
	private String soPhieuVietTay;
	private Date ngayGiao;
	private String diaChi;
	private String ghiChu;
	private String dkthanhToan;
	private Date hanThanhToan;
	private Set<XuathangChitiet> xuathangChitiets = new HashSet<XuathangChitiet>(0);

	public Xuathang() {
	}

	public Xuathang(String xuatHangId) {
		this.xuatHangId = xuatHangId;
	}

	public Xuathang(String xuatHangId, Khachang khachang, Nhanvien nhanvien, Date ngayLap, String khoId, Integer dt,
			String soHoDonVat, String soPhieuVietTay, Date ngayGiao, String diaChi, String ghiChu, String dkthanhToan,
			Date hanThanhToan, Set<XuathangChitiet> xuathangChitiets) {
		this.xuatHangId = xuatHangId;
		this.khachang = khachang;
		this.nhanvien = nhanvien;
		this.ngayLap = ngayLap;
		this.khoId = khoId;
		this.dt = dt;
		this.soHoDonVat = soHoDonVat;
		this.soPhieuVietTay = soPhieuVietTay;
		this.ngayGiao = ngayGiao;
		this.diaChi = diaChi;
		this.ghiChu = ghiChu;
		this.dkthanhToan = dkthanhToan;
		this.hanThanhToan = hanThanhToan;
		this.xuathangChitiets = xuathangChitiets;
	}

	public String getXuatHangId() {
		return this.xuatHangId;
	}

	public void setXuatHangId(String xuatHangId) {
		this.xuatHangId = xuatHangId;
	}

	public Khachang getKhachang() {
		return this.khachang;
	}

	public void setKhachang(Khachang khachang) {
		this.khachang = khachang;
	}

	public Nhanvien getNhanvien() {
		return this.nhanvien;
	}

	public void setNhanvien(Nhanvien nhanvien) {
		this.nhanvien = nhanvien;
	}

	public Date getNgayLap() {
		return this.ngayLap;
	}

	public void setNgayLap(Date ngayLap) {
		this.ngayLap = ngayLap;
	}

	public String getKhoId() {
		return this.khoId;
	}

	public void setKhoId(String khoId) {
		this.khoId = khoId;
	}

	public Integer getDt() {
		return this.dt;
	}

	public void setDt(Integer dt) {
		this.dt = dt;
	}

	public String getSoHoDonVat() {
		return this.soHoDonVat;
	}

	public void setSoHoDonVat(String soHoDonVat) {
		this.soHoDonVat = soHoDonVat;
	}

	public String getSoPhieuVietTay() {
		return this.soPhieuVietTay;
	}

	public void setSoPhieuVietTay(String soPhieuVietTay) {
		this.soPhieuVietTay = soPhieuVietTay;
	}

	public Date getNgayGiao() {
		return this.ngayGiao;
	}

	public void setNgayGiao(Date ngayGiao) {
		this.ngayGiao = ngayGiao;
	}

	public String getDiaChi() {
		return this.diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public String getGhiChu() {
		return this.ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	public String getDkthanhToan() {
		return this.dkthanhToan;
	}

	public void setDkthanhToan(String dkthanhToan) {
		this.dkthanhToan = dkthanhToan;
	}

	public Date getHanThanhToan() {
		return this.hanThanhToan;
	}

	public void setHanThanhToan(Date hanThanhToan) {
		this.hanThanhToan = hanThanhToan;
	}

	public Set<XuathangChitiet> getXuathangChitiets() {
		return this.xuathangChitiets;
	}

	public void setXuathangChitiets(Set<XuathangChitiet> xuathangChitiets) {
		this.xuathangChitiets = xuathangChitiets;
	}

}
