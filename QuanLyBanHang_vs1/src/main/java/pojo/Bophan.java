// default package
// Generated Sep 19, 2016 2:07:21 PM by Hibernate Tools 5.1.0.Alpha1
package pojo;
import java.util.HashSet;
import java.util.Set;

/**
 * Bophan generated by hbm2java
 */
public class Bophan implements java.io.Serializable {

	private String boPhanId;
	private String tenBoPhan;
	private String moTa;
	private Set<Nhanvien> nhanviens = new HashSet<Nhanvien>(0);

	public Bophan() {
	}

	public Bophan(String boPhanId) {
		this.boPhanId = boPhanId;
	}

	public Bophan(String boPhanId, String tenBoPhan, String moTa, Set<Nhanvien> nhanviens) {
		this.boPhanId = boPhanId;
		this.tenBoPhan = tenBoPhan;
		this.moTa = moTa;
		this.nhanviens = nhanviens;
	}

	public String getBoPhanId() {
		return this.boPhanId;
	}

	public void setBoPhanId(String boPhanId) {
		this.boPhanId = boPhanId;
	}

	public String getTenBoPhan() {
		return this.tenBoPhan;
	}

	public void setTenBoPhan(String tenBoPhan) {
		this.tenBoPhan = tenBoPhan;
	}

	public String getMoTa() {
		return this.moTa;
	}

	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}

	public Set<Nhanvien> getNhanviens() {
		return this.nhanviens;
	}

	public void setNhanviens(Set<Nhanvien> nhanviens) {
		this.nhanviens = nhanviens;
	}

}
