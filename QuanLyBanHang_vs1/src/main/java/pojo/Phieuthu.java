// default package
// Generated Sep 19, 2016 2:07:21 PM by Hibernate Tools 5.1.0.Alpha1
package pojo;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Phieuthu generated by hbm2java
 */
public class Phieuthu implements java.io.Serializable {

	private String maPhieuThu;
	private Khachang khachang;
	private Date ngayLap;
	private BigDecimal soTienThu;
	private String nhanVienThu;
	private BigDecimal congNoHienTai;
	private String lyDo;

	public Phieuthu() {
	}

	public Phieuthu(String maPhieuThu) {
		this.maPhieuThu = maPhieuThu;
	}

	public Phieuthu(String maPhieuThu, Khachang khachang, Date ngayLap, BigDecimal soTienThu, String nhanVienThu,
			BigDecimal congNoHienTai, String lyDo) {
		this.maPhieuThu = maPhieuThu;
		this.khachang = khachang;
		this.ngayLap = ngayLap;
		this.soTienThu = soTienThu;
		this.nhanVienThu = nhanVienThu;
		this.congNoHienTai = congNoHienTai;
		this.lyDo = lyDo;
	}

	public String getMaPhieuThu() {
		return this.maPhieuThu;
	}

	public void setMaPhieuThu(String maPhieuThu) {
		this.maPhieuThu = maPhieuThu;
	}

	public Khachang getKhachang() {
		return this.khachang;
	}

	public void setKhachang(Khachang khachang) {
		this.khachang = khachang;
	}

	public Date getNgayLap() {
		return this.ngayLap;
	}

	public void setNgayLap(Date ngayLap) {
		this.ngayLap = ngayLap;
	}

	public BigDecimal getSoTienThu() {
		return this.soTienThu;
	}

	public void setSoTienThu(BigDecimal soTienThu) {
		this.soTienThu = soTienThu;
	}

	public String getNhanVienThu() {
		return this.nhanVienThu;
	}

	public void setNhanVienThu(String nhanVienThu) {
		this.nhanVienThu = nhanVienThu;
	}

	public BigDecimal getCongNoHienTai() {
		return this.congNoHienTai;
	}

	public void setCongNoHienTai(BigDecimal congNoHienTai) {
		this.congNoHienTai = congNoHienTai;
	}

	public String getLyDo() {
		return this.lyDo;
	}

	public void setLyDo(String lyDo) {
		this.lyDo = lyDo;
	}

}
