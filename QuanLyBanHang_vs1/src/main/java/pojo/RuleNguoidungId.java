// default package
// Generated Sep 19, 2016 2:07:21 PM by Hibernate Tools 5.1.0.Alpha1
package pojo;
/**
 * RuleNguoidungId generated by hbm2java
 */
public class RuleNguoidungId implements java.io.Serializable {

	private String idNhomNguoiDung;
	private String idDoiTuongChucNang;

	public RuleNguoidungId() {
	}

	public RuleNguoidungId(String idNhomNguoiDung, String idDoiTuongChucNang) {
		this.idNhomNguoiDung = idNhomNguoiDung;
		this.idDoiTuongChucNang = idDoiTuongChucNang;
	}

	public String getIdNhomNguoiDung() {
		return this.idNhomNguoiDung;
	}

	public void setIdNhomNguoiDung(String idNhomNguoiDung) {
		this.idNhomNguoiDung = idNhomNguoiDung;
	}

	public String getIdDoiTuongChucNang() {
		return this.idDoiTuongChucNang;
	}

	public void setIdDoiTuongChucNang(String idDoiTuongChucNang) {
		this.idDoiTuongChucNang = idDoiTuongChucNang;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof RuleNguoidungId))
			return false;
		RuleNguoidungId castOther = (RuleNguoidungId) other;

		return ((this.getIdNhomNguoiDung() == castOther.getIdNhomNguoiDung())
				|| (this.getIdNhomNguoiDung() != null && castOther.getIdNhomNguoiDung() != null
						&& this.getIdNhomNguoiDung().equals(castOther.getIdNhomNguoiDung())))
				&& ((this.getIdDoiTuongChucNang() == castOther.getIdDoiTuongChucNang())
						|| (this.getIdDoiTuongChucNang() != null && castOther.getIdDoiTuongChucNang() != null
								&& this.getIdDoiTuongChucNang().equals(castOther.getIdDoiTuongChucNang())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getIdNhomNguoiDung() == null ? 0 : this.getIdNhomNguoiDung().hashCode());
		result = 37 * result + (getIdDoiTuongChucNang() == null ? 0 : this.getIdDoiTuongChucNang().hashCode());
		return result;
	}

}
