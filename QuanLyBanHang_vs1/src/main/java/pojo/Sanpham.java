// default package
// Generated Sep 19, 2016 2:07:21 PM by Hibernate Tools 5.1.0.Alpha1
package pojo;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Sanpham generated by hbm2java
 */
public class Sanpham implements java.io.Serializable {

	private String sanPhamId;
	private Donvi donvi;
	private Nhacungcap nhacungcap;
	private Nhomhang nhomhang;
	private String tenSp;
	private String xuatXu;
	private Integer tonKhoMin;
	private Integer tonKhoHienTai;
	private BigDecimal giaMua;
	private BigDecimal giaSi;
	private BigDecimal giaBanLe;
	private String loaiSp;
	private Set<ChuyenkhoChitiet> chuyenkhoChitiets = new HashSet<ChuyenkhoChitiet>(0);
	private Set<Tonkho> tonkhos = new HashSet<Tonkho>(0);
	private Set<XuathangChitiet> xuathangChitiets = new HashSet<XuathangChitiet>(0);
	private Set<NhaphangChitiet> nhaphangChitiets = new HashSet<NhaphangChitiet>(0);

	public Sanpham() {
	}

	public Sanpham(String sanPhamId) {
		this.sanPhamId = sanPhamId;
	}

	public Sanpham(String sanPhamId, Donvi donvi, Nhacungcap nhacungcap, Nhomhang nhomhang, String tenSp, String xuatXu,
			Integer tonKhoMin, Integer tonKhoHienTai, BigDecimal giaMua, BigDecimal giaSi, BigDecimal giaBanLe,
			String loaiSp, Set<ChuyenkhoChitiet> chuyenkhoChitiets, Set<Tonkho> tonkhos,
			Set<XuathangChitiet> xuathangChitiets, Set<NhaphangChitiet> nhaphangChitiets) {
		this.sanPhamId = sanPhamId;
		this.donvi = donvi;
		this.nhacungcap = nhacungcap;
		this.nhomhang = nhomhang;
		this.tenSp = tenSp;
		this.xuatXu = xuatXu;
		this.tonKhoMin = tonKhoMin;
		this.tonKhoHienTai = tonKhoHienTai;
		this.giaMua = giaMua;
		this.giaSi = giaSi;
		this.giaBanLe = giaBanLe;
		this.loaiSp = loaiSp;
		this.chuyenkhoChitiets = chuyenkhoChitiets;
		this.tonkhos = tonkhos;
		this.xuathangChitiets = xuathangChitiets;
		this.nhaphangChitiets = nhaphangChitiets;
	}

	public String getSanPhamId() {
		return this.sanPhamId;
	}

	public void setSanPhamId(String sanPhamId) {
		this.sanPhamId = sanPhamId;
	}

	public Donvi getDonvi() {
		return this.donvi;
	}

	public void setDonvi(Donvi donvi) {
		this.donvi = donvi;
	}

	public Nhacungcap getNhacungcap() {
		return this.nhacungcap;
	}

	public void setNhacungcap(Nhacungcap nhacungcap) {
		this.nhacungcap = nhacungcap;
	}

	public Nhomhang getNhomhang() {
		return this.nhomhang;
	}

	public void setNhomhang(Nhomhang nhomhang) {
		this.nhomhang = nhomhang;
	}

	public String getTenSp() {
		return this.tenSp;
	}

	public void setTenSp(String tenSp) {
		this.tenSp = tenSp;
	}

	public String getXuatXu() {
		return this.xuatXu;
	}

	public void setXuatXu(String xuatXu) {
		this.xuatXu = xuatXu;
	}

	public Integer getTonKhoMin() {
		return this.tonKhoMin;
	}

	public void setTonKhoMin(Integer tonKhoMin) {
		this.tonKhoMin = tonKhoMin;
	}

	public Integer getTonKhoHienTai() {
		return this.tonKhoHienTai;
	}

	public void setTonKhoHienTai(Integer tonKhoHienTai) {
		this.tonKhoHienTai = tonKhoHienTai;
	}

	public BigDecimal getGiaMua() {
		return this.giaMua;
	}

	public void setGiaMua(BigDecimal giaMua) {
		this.giaMua = giaMua;
	}

	public BigDecimal getGiaSi() {
		return this.giaSi;
	}

	public void setGiaSi(BigDecimal giaSi) {
		this.giaSi = giaSi;
	}

	public BigDecimal getGiaBanLe() {
		return this.giaBanLe;
	}

	public void setGiaBanLe(BigDecimal giaBanLe) {
		this.giaBanLe = giaBanLe;
	}

	public String getLoaiSp() {
		return this.loaiSp;
	}

	public void setLoaiSp(String loaiSp) {
		this.loaiSp = loaiSp;
	}

	public Set<ChuyenkhoChitiet> getChuyenkhoChitiets() {
		return this.chuyenkhoChitiets;
	}

	public void setChuyenkhoChitiets(Set<ChuyenkhoChitiet> chuyenkhoChitiets) {
		this.chuyenkhoChitiets = chuyenkhoChitiets;
	}

	public Set<Tonkho> getTonkhos() {
		return this.tonkhos;
	}

	public void setTonkhos(Set<Tonkho> tonkhos) {
		this.tonkhos = tonkhos;
	}

	public Set<XuathangChitiet> getXuathangChitiets() {
		return this.xuathangChitiets;
	}

	public void setXuathangChitiets(Set<XuathangChitiet> xuathangChitiets) {
		this.xuathangChitiets = xuathangChitiets;
	}

	public Set<NhaphangChitiet> getNhaphangChitiets() {
		return this.nhaphangChitiets;
	}

	public void setNhaphangChitiets(Set<NhaphangChitiet> nhaphangChitiets) {
		this.nhaphangChitiets = nhaphangChitiets;
	}

}
