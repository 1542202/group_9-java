package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;

public class DonVi extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DonVi frame = new DonVi();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DonVi() {
		setTitle("Đơn Vị Tính - Phần Mềm Quản Lý Bán Hàng");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel.setBackground(Color.DARK_GRAY);
		contentPane.add(panel, BorderLayout.NORTH);
		
		JButton btnNewButton = new JButton("Thêm");
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(Color.DARK_GRAY);
		btnNewButton.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton.setVerticalAlignment(SwingConstants.TOP);
		btnNewButton.setIcon(new ImageIcon(DonVi.class.getResource("/GIAODIEN/Image/Add.png")));
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Sửa Chữa");
		btnNewButton_1.setIcon(new ImageIcon(DonVi.class.getResource("/GIAODIEN/Image/1472636753_Options.png")));
		btnNewButton_1.setFont(new Font("Arial", Font.PLAIN, 11));
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setBackground(Color.DARK_GRAY);
		panel.add(btnNewButton_1);
		
		JButton btnXa = new JButton("Xóa");
		btnXa.setIcon(new ImageIcon(DonVi.class.getResource("/GIAODIEN/Image/1472636899_DeleteRed.png")));
		btnXa.setFont(new Font("Arial", Font.PLAIN, 11));
		btnXa.setForeground(Color.WHITE);
		btnXa.setBackground(Color.DARK_GRAY);
		panel.add(btnXa);
		
		JButton btnNpLi = new JButton("Nạp Lại");
		btnNpLi.setIcon(new ImageIcon(DonVi.class.getResource("/GIAODIEN/Image/Refresh.png")));
		btnNpLi.setFont(new Font("Arial", Font.PLAIN, 11));
		btnNpLi.setForeground(Color.WHITE);
		btnNpLi.setBackground(Color.DARK_GRAY);
		panel.add(btnNpLi);
		
		JButton btnXut = new JButton("Xuất");
		btnXut.setIcon(new ImageIcon(DonVi.class.getResource("/GIAODIEN/Image/1472637417_export.png")));
		btnXut.setFont(new Font("Arial", Font.PLAIN, 11));
		btnXut.setForeground(Color.WHITE);
		btnXut.setBackground(Color.DARK_GRAY);
		panel.add(btnXut);
		
		JButton btnNhp = new JButton("Nhập");
		btnNhp.setIcon(new ImageIcon(DonVi.class.getResource("/GIAODIEN/Image/1472637557_excel.png")));
		btnNhp.setFont(new Font("Arial", Font.PLAIN, 11));
		btnNhp.setForeground(Color.WHITE);
		btnNhp.setBackground(Color.DARK_GRAY);
		panel.add(btnNhp);
		
		JButton btnng = new JButton("Đóng");
		btnng.setIcon(new ImageIcon(DonVi.class.getResource("/GIAODIEN/Image/1472637125_Close_Box_Red.png")));
		btnng.setFont(new Font("Arial", Font.PLAIN, 11));
		btnng.setForeground(Color.WHITE);
		btnng.setBackground(Color.DARK_GRAY);
		panel.add(btnng);
		
		
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
				new Object[][] {
					{"1", "DV01", "B\u00ECnh", "", ""},
					{"2", "DV02", "B\u1ED9", "", ""},
					{"3", "DV03", "Cái", "", ""},
					{"4", "DV04", "Cặp", "", ""},
					{"5", "DV05", "Cây", "", ""},
				},
				new String[] {
					"", "M\u00E3", "T\u00EAn", "Ghi Ch\u00FA", "C\u00F2n Qu\u1EA3n L\u00FD"
				}
		));
		table.getColumnModel().getColumn(0).setPreferredWidth(43);
                JScrollPane scrollPane = new JScrollPane(table);
                table.setFillsViewportHeight(true);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		
	}

}
