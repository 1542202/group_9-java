package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JList;
import java.awt.List;
import java.io.IOException;

import javax.swing.JComboBox;

public class HangHoa2 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HangHoa2 frame = new HangHoa2();
					frame.setVisible(true);
					frame.setTitle("Hang Hoa");
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/**
	 * 
	 */
	public HangHoa2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 554, 415);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{528, 0};
		gbl_contentPane.rowHeights = new int[]{33, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.anchor = GridBagConstraints.NORTHWEST;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		
		JButton btnNewButton = new JButton("Thêm");
		btnNewButton.setIcon(new ImageIcon("HinhAnh\\addIcon.png"));
		panel.add(btnNewButton);
		
		JButton button = new JButton("Sửa chữa");
		button.setIcon(new ImageIcon("HinhAnh\\update.png"));
		panel.add(button);
		
		JButton btnNewButton_1 = new JButton("Xóa");
		btnNewButton_1.setIcon(new ImageIcon("HinhAnh\\removeIcon.png"));
		panel.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Nạp Lại");
		btnNewButton_2.setIcon(new ImageIcon("HinhAnh\\forward.png"));
		panel.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Xuất");
		btnNewButton_3.setIcon(new ImageIcon("HinhAnh\\Export.JPG"));
		panel.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Nhập");
		btnNewButton_4.setIcon(new ImageIcon("HinhAnh\\import.png"));
		panel.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("Đóng");
		btnNewButton_5.setIcon(new ImageIcon("HinhAnh\\close.png"));
		panel.add(btnNewButton_5);
		
		JComboBox comboBox = new JComboBox();
		comboBox.addItem("Nhom hang");
		comboBox.addItem(1);
		comboBox.addItem(2);
		comboBox.addItem(3);
		comboBox.addItem(4);
		comboBox.setSelectedIndex(0);
		//comboBox.setPreferredSize(new Dimension(200, 50));		
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.anchor = GridBagConstraints.WEST;
		gbc_comboBox.insets = new Insets(0, 0, 5, 0);
		
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 1;
		contentPane.add(comboBox, gbc_comboBox);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"Ma001", "Tem t\u1EEB gi\u1EA5y  4x4", "cai", "90000", "95000", "100000", "1", "Hang Hoa", "Kho Cong ty"},
				{"Ma002", "Tem t\u1EEB gi\u1EA5y 3x3", "cai", "90000", "95000", "100000", "1", "Hang Hoa", "Kho Cong ty"},
				{"Ma003", "D\u00E2y \u0111eo kim lo\u1EA1i BB L002", "cai", "90000", "95000", "100000", "1", "Hang Hoa", "Kho Cong ty"},
				{"Ma004", "D\u00E2y \u0111eo nh\u1EF1a BB L003/G", "cai", "90000", "95000", "100000", "1", "Hang Hoa", "Kho Cong ty"},
				{"Ma005", "M\u1EDF kh\u00F3a DO-0004", "cai", "90000", "95000", "100000", "1", "Hang Hoa", "Kho Cong ty"},
			},
			new String[] {
				"M\u00E3 H\u00E0ng", "T\u00EAn H\u00E0ng", "\u0110\u01A1n v\u1ECB", "Gi\u00E1 Mua", "Gi\u00E1 B\u00E1n S\u1EC9", "Gi\u00E1 B\u00E1n L\u1EBB", "T\u1ED1i Thi\u1EC3u", "T\u00EDnh Ch\u1EA5t", "Kho M\u1EB7c \u0110\u1ECBnh"
			}
		));
		table.getColumnModel().getColumn(1).setPreferredWidth(120);
		
		table.setBounds(30,40,200,300);
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.gridheight = 2;
		gbc_table.insets = new Insets(0, 0, 5, 0);
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridx = 0;
		gbc_table.gridy = 2;
		contentPane.add(new JScrollPane(table), gbc_table);
		
	}

}
