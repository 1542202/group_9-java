package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Insets;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

public class InMaVach extends JFrame {

	private JPanel contentPane;
	private JTable table;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InMaVach frame = new InMaVach();
					frame.setVisible(true);
					frame.setTitle("In Ma Vach");
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InMaVach() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{424, 0};
		gbl_contentPane.rowHeights = new int[]{0, 10, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.anchor = GridBagConstraints.NORTHWEST;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		
		JButton btnXem = new JButton("Xem");
		btnXem.setIcon(new ImageIcon("HinhAnh\\view.png"));
		panel.add(btnXem);
		
		JButton btnXuat = new JButton("Xuất");
		btnXuat.setIcon(new ImageIcon("HinhAnh\\forward.png"));
		panel.add(btnXuat);
		
		JButton btnIn = new JButton("In");
		btnIn.setIcon(new ImageIcon("HinhAnh\\print.png"));
		panel.add(btnIn);
		
		JButton btnDong = new JButton("Đóng");
		btnDong.setIcon(new ImageIcon("HinhAnh\\close.png"));
		panel.add(btnDong);
		
		table = new JTable();
		table.setEnabled(false);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"1", "Ma001", "Tem t\u1EEB gi\u1EA5y  4x4", "100000", "ANS0001", "cai"},
				{"1", "Ma002", "Tem t\u1EEB gi\u1EA5y 3x3", "100000", "ANS0002", "cai"},
				{"1", "Ma003", "D\u00E2y \u0111eo kim lo\u1EA1i BB L002", "100000", "ANS0003", "cai"},
				{"1", "Ma004", "D\u00E2y \u0111eo nh\u1EF1a BB L003/G", "100000", "ANS0004", "cai"},
				{"1", "Ma005", "M\u1EDF kh\u00F3a DO-0004", "100000", "ANS0005", null},
			},
			new String[] {
				"S\u1ED1 L\u1EA7n In", "M\u00E3 H\u00E0ng", "T\u00EAn Ti\u1EBFng Vi\u1EC7t", "Gi\u00E1 B\u00E1n", "M\u00E3 V\u1EA1ch", "\u0110\u01A1n V\u1ECB"
			}
		));
		table.getColumnModel().getColumn(2).setPreferredWidth(95);
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridwidth = 2;
		gbc_table.gridheight = 2;
		gbc_table.gridx = 0;
		gbc_table.gridy = 1;
		contentPane.add(new JScrollPane(table), gbc_table);
	}
	

}
