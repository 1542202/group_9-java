package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class KhoHang {

	private JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KhoHang window = new KhoHang();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public KhoHang() {
		initialize();
                this.frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 581, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		
		JButton btnThm = new JButton("Thêm");
		btnThm.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/them.png")));
		panel.add(btnThm);
		
		JButton btnXa = new JButton("Xóa");
		btnXa.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/delete.png")));
		panel.add(btnXa);
		
		JButton btnSa = new JButton("Sửa");
		btnSa.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/edit.png")));
		panel.add(btnSa);
		
		JButton btnNp = new JButton("Nạp");
		btnNp.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/napvao.png")));
		panel.add(btnNp);
		
		JButton btnXut = new JButton("Xuất");
		btnXut.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/xuat.png")));
		panel.add(btnXut);
		
		JButton btnNhp = new JButton("Nhập");
		btnNhp.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/nhap.png")));
		panel.add(btnNhp);
		
		JButton btnng = new JButton("Đóng");
		btnng.setIcon(new ImageIcon(KhoHang.class.getResource("/com/javabasic/QLBanHang/images/dong.png")));
		panel.add(btnng);
		
		JScrollPane scrollPane = new JScrollPane();
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
			},
			new String[] {
				"Id", "M\u00E3", "T\u00EAn", "Li\u00EAn H\u1EC7", "\u0110\u1ECBa ch\u1EC9", "\u0110i\u1EC7n tho\u1EA1i", "K\u00ED hi\u1EC7u", "Ghi ch\u00FA", "C\u00F2n qu\u1EA3n l\u00FD"
			}
		));
		scrollPane.setViewportView(table);
	}

}
