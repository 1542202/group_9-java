package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NhaCungCap{

	private JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NhaCungCap window = new NhaCungCap();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NhaCungCap() {
		initialize();
                this.frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 525, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		
		JButton btnThm = new JButton("Thêm");
		btnThm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnThm.setIcon(new ImageIcon(NhaCungCap.class.getResource("/com/javabasic/QLBanHang/images/them.png")));
		panel.add(btnThm);
		
		JButton btnXa = new JButton("Xóa");
		btnXa.setIcon(new ImageIcon(NhaCungCap.class.getResource("/com/javabasic/QLBanHang/images/delete.png")));
		panel.add(btnXa);
		
		JButton btnSa = new JButton("Sửa");
		btnSa.setIcon(new ImageIcon(NhaCungCap.class.getResource("/com/javabasic/QLBanHang/images/edit.png")));
		panel.add(btnSa);
		
		JButton btnNp = new JButton("Nạp");
		btnNp.setIcon(new ImageIcon(NhaCungCap.class.getResource("/com/javabasic/QLBanHang/images/napvao.png")));
		panel.add(btnNp);
		
		JButton btnXut = new JButton("Xuất");
		btnXut.setIcon(new ImageIcon(NhaCungCap.class.getResource("/com/javabasic/QLBanHang/images/xuat.png")));
		panel.add(btnXut);
		
		JButton btnNhp = new JButton("Nhập");
		btnNhp.setIcon(new ImageIcon(NhaCungCap.class.getResource("/com/javabasic/QLBanHang/images/nhap.png")));
		btnNhp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		panel.add(btnNhp);
		
		JButton btnng = new JButton("Đóng");
		panel.add(btnng);
		
		JScrollPane scrollPane = new JScrollPane();
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null},
			},
			new String[] {
				"Id", "Nh\u00E0 cung c\u1EA5p", "Ng\u01B0\u1EDDi li\u00EAn h\u1EC7", "Ch\u1EE9c v\u1EE5", "\u0110\u1ECBa ch\u1EC9", "\u0110i\u1EC7n Tho\u1EA1i", "Di \u0111\u1ED9ng", "Fax", "C\u00F2n qu\u1EA3n l\u00FD"
			}
		));
		scrollPane.setViewportView(table);
		
	}

}
